module BESO_SUPER
  implicit none
  ! ステップ数
  integer :: kstep = 1
  ! ステップ名文字列
  character :: kstepStr*4
  ! ファイル名
  character(:), allocatable :: modelName
end module BESO_SUPER

module BESO_DATA
  implicit none

  type pt_val
    integer :: Num_vec(3), list_len
    integer, allocatable :: ID_vec(:,:)
    real(8), allocatable :: Val_vec(:,:)
  end type pt_val

  !*** 詳細情報関係 ***
  integer :: step_end                !最大Step数
  integer :: step_start = 1            !繰り返し計算のdoループの引数
  real(8), parameter ::  pi = 4.0d0 * datan(1.0d0)

  !*** 解析条件関係 ***
  integer :: symType = 3              !敏感数の対称性タイプ(1:対称性なし, 2:x-z面対称, 3:z軸対称)
  integer :: sym_num                  ! 対称数
  integer :: sym_nodeNum              ! 対称成分数
  integer, allocatable :: sym_map(:) ! 対称部分のオリジナルをマッピング
  integer, allocatable :: Cre_Map_local(:) ! 対称領域内の有効設計変数のマップ
  real(8), allocatable :: sense_hist_ope(:)
  integer, allocatable :: Be_ope(:)

  !*** 形態操作 ***
  integer :: target_Nd               !目標要素数
  integer :: history=1               !位相履歴の考慮(1:有り,  0:無し)
  integer :: convergence_num = 5     !収束判定数
  real(8) :: target_rate=0.5d0        !目標体積
  real(8) :: e_rate=0.025d0          !構造体の進化率
  real(8) :: topo_rate=0.01d0        !構造体の位相進化率
  real(8) :: filter_degree = 1.d0    ! フィルター関数の次数
  real(8) :: r_min=2.d0              !均質化の際の、近傍節点探索距離(パラメータ座標)
  real(8) :: r_min1                  !均質化の際の、近傍節点探索距離(実座標)

  !*** 基準値決定法 ***
  integer :: opt_target=3              !敏感数の選択(1:Von Mises, 2:変位敏感数, 3:座屈敏感数)
  real(8) :: coeff_von
  real(8) :: coeff_disp
  real(8) :: coeff_buck

  !*** 設計領域 ***
  real(8) :: X0=0, Y0=0, Z0=0              !初期座標(原点)
  real(8) :: Xn,Yn,Zn              !設計領域(単位:m)
  integer :: Nx,Ny,Nz              !設計領域分割
  ! 設計領域情報
  integer :: Nd,Ln,El              !節点総数、線総数、要素総数(全体領域)
  integer :: Num_ope               ! 有効設計変数(Cre_Map=1)の数
  integer :: Be_Num_ope            ! 有効設計変数(Cre_Map=1)のうち、構造体内部
  real(8) :: Hx,Hy,Hz              !要素の大きさ
  real(8) :: step_volume
  integer :: Nd1,Ln1,El1              !節点総数、線総数、要素総数(解析領域)
  integer :: Nd_ope                ! 形態創生対象要素(Cre_Map=1)のモデル要素数
  integer :: Nd1_ope                ! 現ステップの形態創生対象要素(Cre_Map=1)のモデル要素数

  !*** 設計領域/ジオメトリ ***
  integer, allocatable :: grid_node_map(:)
  !*-設計領域
  real(8),allocatable :: Nd_in(:,:)         !節点座標(1,2,3)を格納する行列
  !*-要素-節点関係
  integer,allocatable :: El_in(:,:)          !要素番号・要素を構成する節点番号を格納する行列(全体領域)
  !*-設計グリッド内の設計領域をMapping
  integer, allocatable :: Cre_Map(:)       ! 有効設計変数のマップ(1:形態創生対象,0:形態創生対象外)
  !*-設計変数(1or0)
  integer,allocatable :: Be(:)              !全体領域の中での要素の有無を表す行列(1:有り、2:無し)
  !*-要素材料Map
  integer,allocatable :: material_map(:)    !それぞれの要素の材料IDを格納
  real(8),allocatable :: EL_g(:,:)          !要素の重心座標を格納する行列(要素番号，1⇒x 2⇒y 3⇒z)
  ! integer,allocatable :: sym_el(:,:)        !対称軸上に存在する要素の要素番号
  integer,allocatable :: sym_node(:,:)        !対称軸上に存在する節点の節点番号

  integer,allocatable :: Be_pre(:)        !全体領域の中での要素の有無を表す行列(1:有り、2:無し)(前回のStep)

  !*** モデル-設計領域関係 ***
  !*-設計領域-モデル節点関係
  integer,allocatable :: model_nodeID(:)  !設計領域節点番号-モデル節点番号
  !*-設計領域-モデル要素関係
  integer,allocatable :: model_elID(:)    !設計領域要素番号-モデル要素番号

  !*** 解析ケース ***
  integer :: totalAnalyCase = 1                            !解析ケース数(荷重・支持組み合わせ数)
  integer, allocatable :: analyCase_list(:,:)             !(支持ケース,荷重ケース)*解析ケース

  !*** 支持条件 ***
  integer :: support_caseNum = 1                               !支持ケース総数(default=1)
  integer :: support_listLen
  integer, allocatable :: supportEl_list(:)   ! 設計領域における支持属性を持つ要素のリスト
  integer :: NFIX_1(3)                        !設計領域の拘束数(X方向:1,Y方向:2,Z方向:3)
  integer,allocatable :: NFIXP_1(:,:)      !設計領域各方向の拘束節点番号

  !*** 荷重条件 ***
  integer :: load_caseNum = 1                               !荷重ケース総数(default=1)
  integer :: load_listLen
  integer, allocatable :: loadEl_list(:)          ! 設計領域における荷重属性を持つ要素のリスト
  type(pt_val), allocatable :: load_data(:)        ! 設計領域における荷重データをまとめた構造体

  !== 座屈解析 ==
  integer :: buckAnalyNum = 6                ! 座屈モード算出数
  !== Von Mises ==
  real(8), allocatable :: vonMises(:,:)    !解析領域で要素毎のVon Mises応力を格納するベクトル,全体領域で要素毎のVon Mises応力を格納するベクトル
  !== Disp Sense ==
  real(8), allocatable :: dispSense(:,:)  !解析領域で要素毎の変位敏感数を格納するベクトル
  !== Buck Sense ==
  real(8) :: Bsen_mixPenalty = 1.d0          ! 座屈敏感数合成時のペナルティ
  integer :: BsenJoinNum = 6                 !座屈敏感数合成数
  real(8), allocatable :: buckLoad(:,:)
  real(8), allocatable :: buckSense_list(:,:,:)
  real(8), allocatable :: buckSense(:,:) !モデル領域で,各荷重ケースごとの要素毎の擬似座屈敏感数を格納する配列
  !*** 形態創生感度 ***
  real(8),allocatable :: Ndsen_hist(:)    ! hitory有効時の要素感度配列
  real(8),allocatable :: El_sen(:)        !均質化後の要素の敏感数の値
  real(8),allocatable :: Nd_sen(:)        !節点値化した格子節点の敏感数の値
  real(8),allocatable :: Sensitivity(:)   !均質化後の要素の敏感数の値

  real(8) :: Vp0 !次の構造体を形成する基準値

end module
