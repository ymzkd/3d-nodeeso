!■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
!
!                            BESO Program
!                                 ver:2016_9_5, date:160905, @yamazaki
!
!■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
program BESO
  use BESO_SUPER
  use BESO_DATA
  use FILE_IO
  use BESO_ROUTINE
  use ANALYSIS_CTRL
  use STD_CAL
  use INIT_BUILD_SETTING
  use PHYSICAL_DATA
  use MESH_HANDLE
  implicit none
  integer :: i

  call Time_Stamp('Program Start')

  !**** BESOパラメータの設定 ****
  call Set_BESO_Param

!%%%%%%%%% 解析準備 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!**** 設計領域の設定 ****
  call Init_Build_Area

  !**** 要素の大きさを求める(X,Y,Z各方向) ****
  Hx=(Xn-X0)/DFLOAT(Nx)
  Hy=(Yn-Y0)/DFLOAT(Ny)
  Hz=(Zn-Z0)/DFLOAT(Nz)
  unitVolume=Hx*Hy*Hz
  write(*,*)'Element Size(X,Y,Z)'
  write(*,*)'Hx=',Hx
  write(*,*)'Hy=',Hy
  write(*,*)'Hz=',Hz
  Nd=(Nx+1)*(Ny+1)*(Nz+1)                                 !節点総数:Nd(Node)
  Ln=Nx*(Ny+1)*(Nz+1)+Ny*(Nx+1)*(Nz+1)+(Nx+1)*Nz*(Ny+1)   !線総数:Ln(Line)
  El=Nx*Ny*Nz                                             !設計領域の要素総数:El(Element)
  r_min1=r_min*max(Hx,Hy,Hz)                              !均質化の際の、近傍節点探索距離(実座標)

  call Init_Physical_Param

!%%%%%%%%% 初期形状の作成 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  allocate(Nd_in(Nd,3))
  allocate(El_in(El,8))
  allocate(Be(Nd))
  allocate(grid_node_map(Nd))
  allocate(material_map(El))
  allocate(Cre_Map(Nd))
  allocate(El_g(El,3))
  allocate(sym_map(Nd))
  if (symType == 2) then
    sym_num = 2
    sym_nodeNum = (Nx+1) * ((Ny+1)/2 + mod((Nx+1),2)) * (Nz+1)
    allocate(sym_node(sym_nodeNum,sym_num))
  elseif (symType == 3) then
    sym_num = 4
    sym_nodeNum = ((Nx+1)/2 + mod((Nx+1),2)) * ((Ny+1)/2 + mod((Ny+1),2)) * (Nz+1)
    allocate(sym_node(sym_nodeNum, 4))
  else
    sym_num = 1
    sym_nodeNum = Nd
    allocate(sym_node(sym_nodeNum,sym_num))
  end if

  ! 位相履歴用感度
  allocate(Ndsen_hist(Nd)); Ndsen_hist(:) = 0.d0
  ! デフォルト値
  Cre_Map(:) = 1
  material_map(:) = 1

  allocate(model_nodeID(Nd))
  allocate(model_elID(El))

  allocate(Be_pre(Nd))

  !**** 六面体要素分割 ****
  call Time_Stamp('設計領域分割')
  call MeshG(Nd,Nx,Ny,Nz)

  !**** 初期形状作成 ****
  ! Beの作成
  call Time_Stamp('初期形態の生成')
  call InitModel

  !%%%%%%%%% 支持・荷重条件 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !**** 支持条件情報 ****
  call Time_Stamp('支持条件の生成')
  call InitSupport

  !**** 荷重条件情報 ****
  call Time_Stamp('荷重条件の生成')
  call InitLoad

  !**** 荷重条件情報 ****
  ! analycase_list(i,1) : 支持条件
  ! analycase_list(i,2) : 荷重条件
  totalAnalyCase = load_caseNum
  allocate(analyCase_list(totalAnalyCase,2))
  analyCase_list(:,:) = 1
  do i = 1, totalAnalyCase
    analyCase_list(i,2) = i
  end do

  call Diside_Variable

  
  ! モデルデータを格納する配列を準備
  call DataArray

  !目標要素数
  target_Nd=(Nd - count(Cre_Map(:)==0))*target_rate
  El1=count(Be(:)==1)
  Nd_ope = count(pack(Nd_sen(:)>=Vp0, Cre_Map(:)==1))
  Nd1_ope = Nd_ope

  ! 解析設定のアウトプット
  call OutputBESO_Parm

  call Time_Stamp('進化スタート')
![][][][][][][][][][][][][][][][][][][][][][][][][][][]
!              繰り返し演算開始
![][][][][][][][][][][][][][][][][][][][][][][][][][][]
do kstep = step_start, step_end

  write(*,'(a,i4,a)')'============<Step=',kstep,'>============'
  write(*,*) '要素数      =', El1
  write(*,*) '現在の体積比=', dfloat(El1)/dfloat(El)*100
  write(*,*) '目標体積比  =', target_rate

  write(kstepStr,fmt='(I4.4)') kstep
  call OutputOverView
  call OutputModel_info
  call OutputAnalysis_Summary

  call Time_Stamp('step' // kstepStr// 'スタート')

  !**** 孤立要素のマッピング ****
  ! call Remove_Isolate

  !%%%%%%%%% モデル生成 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ! -* Beから、
  call Time_Stamp('step' // kstepStr // 'モデル生成')

  ! モデルジオメトリ配列
  ! call Input_Geometry

  call Make_Tetra_Model

  ! モデルジオメトリアウトプット
  call OutputGeometory_inp('current_model')

  call Time_Stamp('step' // kstepStr // '構造解析開始')


  ! 解析
  call Analysis_All

  ! ジオメトリー配列など開放
  call FreeModel_Geometry

! %%%%%%%%% 形態操作 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
52 format(I5,10e26.15)
  if (not(BsenJoinNum==0 .and. buckAnalyNum==0)) then
    call Time_Stamp('step' // kstepStr // '座屈敏感数合成')
    call BuckSense_Mix
    ! call BuckSense_Brending
  end if

  !**** 敏感数の重ねあわせ *****
  call Time_Stamp('step' // kstepStr // '敏感数重ね合わせ')
  call SenVal_Compose

  ! 対称性を反映
  if (symType /= 1) then
    call SymCopy
  end if

  !**** 双方向進化 + 均質化 ****
  call Time_Stamp('step' // kstepStr // '敏感数フィルタリング')
  call filter_scheme

  ! Vp0 = sum(Nd_sen(:)) / size(Nd_sen(:)) - std(Nd_sen(:))/2
  ! print *, 'std', std(Nd_sen(:))
  ! print *, 'mean', sum(Nd_sen(:)) / size(Nd_sen(:))
  !**** 設計領域情報の出力 ****
  call OutputBuild_inp

  call Time_Stamp('step' // kstepStr // 'モデルの更新')
  call Generate_NextModel

end do
![][][][][][][][][][][][][][][][][][][][][][][][][][][]
!              繰り返し演算終了
![][][][][][][][][][][][][][][][][][][][][][][][][][][]

  close(1) !応答値出力ファイルを閉じる
  close(2) !解析時間出力ファイルを閉じる
  stop "============<End BESO Program>============"
end program
