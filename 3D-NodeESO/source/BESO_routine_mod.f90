module BESO_ROUTINE
  use BESO_DATA
  implicit none
  contains

!-------------------------------------------------------------------------------
! subroutine BuckSense_Mix
! 座屈感度係数の合成
!-------------------------------------------------------------------------------
subroutine BuckSense_Mix
  implicit none
  real(8) :: weight_bottom

  integer :: i, j
  do i = 1, totalAnalyCase
    weight_bottom = 0.d0
    do  j = 1, BsenJoinNum
      weight_bottom = weight_bottom + 1.d0 / (buckLoad(j,i)**Bsen_mixPenalty)
    end do

    buckSense(:,i) = 0.d0
    do j = 1, BsenJoinNum
      buckSense(:,i) = buckSense(:,i) + buckSense_list(:,j,i) * buckLoad(1,i) / &
        & (buckLoad(j,i)**(Bsen_mixPenalty+1.d0) * weight_bottom)
    end do
  end do

end subroutine BuckSense_Mix

!-------------------------------------------------------------------------------
! subroutine BuckSense_Brending
! 感度係数の上限下限を基準化する。
!-------------------------------------------------------------------------------
subroutine BuckSense_Brending
  real(8) :: base_width, weight_bottom
  real(8) :: buckSense_temp(El,BsenJoinNum)
  integer :: i, j
  do i = 1, totalAnalyCase
    base_width = maxval(buckSense_list(:,1,i)) - minval(buckSense_list(:,1,i))
    buckSense_temp(:,1) = buckSense_list(:,1,i)
    do j = 2, BsenJoinNum
      buckSense_temp(:,j) = base_width/(maxval(buckSense_list(:,j,i)) - minval(buckSense_list(:,j,i))) &
        & * buckSense_list(:,j,1)
    end do

    weight_bottom = 0.d0
    do  j = 1, BsenJoinNum
      weight_bottom = weight_bottom + 1.d0 / (buckLoad(j,i)**Bsen_mixPenalty)
    end do

    buckSense(:,i) = 0.d0
    do j = 1, BsenJoinNum
      buckSense(:,i) = buckSense(:,i) + buckSense_temp(:,j) * buckLoad(1,i) / &
        & (buckLoad(j,i)**(Bsen_mixPenalty+1.d0) * weight_bottom)
    end do
  end do
end subroutine BuckSense_Brending

!-------------------------------------------------------------------------------
! subroutine SenVal_Comp
! 敏感数の合成と全体領域への移動
! 敏感数の対応番号(1:Von Mises,  2:変位敏感数, 3:座屈敏感数)
!-------------------------------------------------------------------------------
subroutine SenVal_Compose
  ! use MODEL_MOD
  implicit none

  real(8) :: maxVon, maxDisp, maxBuck
  integer :: i, j

  Sensitivity(:) = 0.d0
  do i = 1, totalAnalyCase
    maxVon = maxval(VonMises(:,i))
    maxDisp = maxval(dispSense(:,i))
    maxBuck = maxval(buckSense(:,i))
    ! Mises応力の足し合わせ
    ! TODO: VonMises節点値をまだ計算実装してない
    ! do j = 1, Nd
    !   Sensitivity(j) = Sensitivity(j) + coeff_von * VonMises(j,i) / maxVon
    ! end do

    ! 変位敏感数の足し合わせ
    do j = 1, Nd
      Sensitivity(j) = Sensitivity(j) + coeff_disp * dispSense(j,i) / maxDisp
    end do

    ! 座屈敏感数の足し合わせ
    do j = 1, Nd
      Sensitivity(j) = Sensitivity(j) + coeff_buck * buckSense(j,i) / maxBuck
    end do
  end do
end subroutine SenVal_Compose

!-------------------------------------------------------------------------------
! subroutine SymCopy
! 感度係数の対称性を反映
!-------------------------------------------------------------------------------
subroutine SymCopy
  implicit none
  integer :: symNode_Num, symNum
  real(8) :: tmp_val

  integer :: i, j

  symNode_Num = size(sym_node(:,:), dim=1)
  symNum = size(sym_node(:,:), dim=2)

  do i = 1, symNode_Num
    ! 対称な要素の平均感度係数を算出
    tmp_val = 0.d0
    do j = 1, symNum
      tmp_val = tmp_val + Sensitivity(sym_node(i,j))
    end do
    tmp_val = tmp_val / symNum

    ! 平均を代入
    do j = 1, symNum
      Sensitivity(sym_node(i,j)) = tmp_val
    end do
  end do

end subroutine SymCopy

!-------------------------------------------------------------------------------
! subroutine Remove_Isolate
! 要素の連続性をマッピング
!-------------------------------------------------------------------------------
! subroutine Remove_Isolate
!   implicit none
!   integer :: stack_num(2), elm_stack(El,2)
!
!   integer :: neigh_type = 1 ! 1:ノイマン境界, 2:ディリクレ境界
!
!   integer :: target_elm, target_ix, target_iy, target_iz
!   integer :: srch_elm, srch_ix, srch_iy, srch_iz
!   integer :: count_zero
!   integer, allocatable :: continuous_map(:,:)
!
!   integer :: i
!
!   allocate(continuous_map(El,2))
!   continuous_map(:,1) = -Be(:)
!   continuous_map(:,2) = continuous_map(:,1)
!   stack_num(:) = 0
!   elm_stack(:,:) = 0
!
!   ! 支持要素でかつ、位相内部(Be=1)に1を入力
!   do i = 1, size(supportEl_list)
!     if (Be(supportEl_list(i))==1) then
!       continuous_map(supportEl_list(i),1) = 1
!       stack_num(1) = stack_num(1) + 1
!       elm_stack(stack_num(1),1) = supportEl_list(i)
!     end if
!   end do
!
!   ! 荷重要素でかつ、位相内部(Be=1)に1を入力
!   do i = 1, size(loadEl_list)
!     if (Be(loadEl_list(i))==1) then
!       continuous_map(loadEl_list(i),2) = 1
!       stack_num(2) = stack_num(2) + 1
!       elm_stack(stack_num(2),2) = loadEl_list(i)
!     end if
!   end do
!
!   ! スタックを消費し切るまで回る
!   do i = 1, 2
!     srch_roop: do
!       target_elm = elm_stack(stack_num(i),i)
!       stack_num(i) = stack_num(i) - 1
!
!       target_iz = target_elm / (nx*ny)
!       if (mod(target_elm, (nx*ny)) > 0) target_iz = target_iz + 1
!
!       target_iy = mod(target_elm, (nx*ny)) / nx
!       if (mod(mod(target_elm, (nx*ny)), nx) > 0) target_iy = target_iy + 1
!
!       target_ix = mod(target_elm, nx)
!       if (target_ix==0) target_ix = nx
!
!       do srch_ix = target_ix - 1, target_ix + 1
!         ! インデックスが領域外部
!         if (srch_ix <= 0) cycle
!         if (srch_ix > nx) cycle
!
!         do srch_iy = target_iy - 1, target_iy + 1
!           ! インデックスが領域外部
!           if (srch_iy <= 0) cycle
!           if (srch_iy > ny) cycle
!
!           do srch_iz = target_iz - 1, target_iz + 1
!             ! インデックスが領域外部
!             if (srch_iz <= 0) cycle
!             if (srch_iz > nz) cycle
!
!             srch_elm = Total_ID(srch_ix, srch_iy, srch_iz)
!             ! 探索先要素が位相外部
!             if (continuous_map(srch_elm, i)==0) cycle
!
!             ! 探索先要素がすでに探索済み
!             if (continuous_map(srch_elm, i)==1) cycle
!
!             ! 境界条件によって選別
!             count_zero = count((/target_ix==srch_ix, target_iy==srch_iy, target_iz==srch_iz/))
!             ! 真ん中は自分自身なので回避
!             if (count_zero >= 3) cycle
!
!             ! ノイマン境界タイプでは、面が接しないときは回避
!             if (neigh_type == 1) then
!               if (count_zero < 2) cycle
!             end if
!
!             ! ここに到達すると
!             ! target要素とsrch要素が隣接
!             ! スタックに新しく見つかった要素を追加
!             stack_num(i) = stack_num(i) + 1
!             elm_stack(stack_num(i),i) = srch_elm
!
!             ! 連続性を更新
!             continuous_map(srch_elm, i) = 1
!           end do
!         end do
!       end do
!
!       ! ディリクレ境界チェック
!
!       ! 直交インデックスが領域外を示している。→next
!       ! 位相外部(continuous_map=0)→next
!       ! すでに連続性を確認済み(continuous_map=1)→next
!       ! 連続性を発見(continuous_map=-1)
!       !   →スタックに新しく見つかった要素を追加・連続性を更新→next
!
!       if (stack_num(i) <= 0) exit srch_roop
!
!     end do srch_roop
!
!   end do
!
!   do i = 1, El
!     if (all(continuous_map(i,:)==1)) then
!       continuous_model(i) = 1
!     else
!       continuous_model(i) = 0
!     end if
!   end do
!
!   ! print *, 'continue:', count(continuous_map(:,1)==1), count(continuous_map(:,2)==1)
!   ! print *, 'Be :', count(Be(:)==1)
!
! end subroutine Remove_Isolate

!-------------------------------------------------------------------------------
! function Total_ID
! 直交座標インデックスを通しのインデックスに変換
!-------------------------------------------------------------------------------
integer function Total_ID(ix, iy, iz)
  integer, intent(in) :: ix, iy, iz
  Total_ID = (iz-1) * (nx*ny) + (iy-1) * nx + ix
end function Total_ID

!-------------------------------------------------------------------------------
! subroutine filter_scheme
! 感度フィルタリング
! 要素値のフィルタリング
!-------------------------------------------------------------------------------
subroutine filter_scheme
  use BESO_SUPER
  implicit none
  integer :: ix_head, ix_tail, iy_head, iy_tail, iz_head, iz_tail
  integer :: ix_range, iy_range, iz_range
  integer :: self_index, target_index, count_list_ind
  integer :: x_width, y_width, z_width
  real(8) :: target_dist

  real(8), allocatable :: chk_list(:)
  integer :: i, ix, iy, iz, jx, jy, jz

  ix_range = int(r_min1 / hx)
  iy_range = int(r_min1 / hy)
  iz_range = int(r_min1 / hz)
  allocate(chk_list((ix_range*2+1)*(iy_range*2+1)*(iz_range*2+1)))

  x_width = Nx + 1
  y_width = Ny + 1
  z_width = Nz + 1

  Nd_sen(:) = 0.d0
  do iz = 1, z_width
    iz_head = max(iz - iz_range, 1)
    iz_tail = min(z_width, iz + iz_range)
    do iy = 1, y_width
      iy_head = max(iy - iy_range, 1)
      iy_tail = min(y_width, iy + iy_range)
      do ix = 1, x_width
        ix_head = max(ix - ix_range, 1)
        ix_tail = min(x_width, ix + ix_range)
        ! フィルター適用要素の通しのインデックス
        self_index = ((iz-1)*y_width + (iy-1))*x_width + ix

        !------調査範囲内を調査------
        count_list_ind = 0
        chk_list(:) = 0.d0
        do jz = iz_head, iz_tail
          do jy = iy_head, iy_tail
            do jx = ix_head, ix_tail
              ! 調査点の通しのインデックス
              target_index = ((jz-1)*y_width + (jy-1))*x_width + jx
              ! 調査点との距離
              target_dist = sqrt(sum((Nd_in(target_index,:) - Nd_in(self_index,:))**2))

              if (target_dist > r_min1) cycle
              ! 重み係数の保存
              count_list_ind = count_list_ind + 1
              chk_list(count_list_ind) = ((r_min1 - target_dist)/r_min1)**filter_degree
              ! 重みを掛けて足し合わせ
              Nd_sen(self_index) = Nd_sen(self_index) + chk_list(count_list_ind) * Sensitivity(target_index)
            end do
          end do
        end do
        ! 重み係数の総和で割る
        Nd_sen(self_index) = Nd_sen(self_index) / sum(chk_list(1:count_list_ind))
        !------調査範囲内を調査------

      end do
    end do
  end do

  if (history==1 .and. kstep>1) then
    !位相履歴を考慮
    do i=1,Nd
      Nd_sen(i)=(Nd_sen(i)+Ndsen_hist(i))/2.0d0
    end do
  end if
  Ndsen_hist(:) = Nd_sen(:)

end subroutine filter_scheme

!-------------------------------------------------------------------------------
! subroutine Generate_NextModel
! 次ステップのモデルを作成
!-------------------------------------------------------------------------------
subroutine Generate_NextModel
  use BESO_DATA
  use BESO_SUPER
  use STD_CAL
  implicit none

  real(8) :: sense_ope(Num_ope)
  real(8) :: tmp_pack(Num_ope)

  real(8), allocatable :: Sen_arange(:)
  integer, allocatable :: Be_local(:)
  integer :: next_volume
  integer, save :: count_convergence = 0
  real(8) :: current_rate
  real(8) :: volume_rate, max_sen

  real(8), allocatable :: update_dom_sense(:)
  integer :: i, j
  integer :: i_count
  real(8) :: find_rate

  sense_ope(1:Num_ope) = pack(pack(Nd_sen(:), sym_map(:)==1), Cre_Map_local(:)==1)
  allocate(Be_local(sym_nodeNum))
  Be_local(1:sym_nodeNum) = pack(Be(:), sym_map(:)==1)

  ! 次ステップの要素数の決定
  current_rate = dble(Be_Num_ope)/dble(Num_ope)

  ! 必要な削除率が進化率以下
  if (abs(target_rate - current_rate) < e_rate) then
    next_volume = Num_ope * target_rate
    ! 必要な削除率が進化率より大きい
  else
    next_volume = Num_ope * (sign(1.d0,target_rate - current_rate) * e_rate + current_rate)
  end if

  ! print *, 'next_volume', next_volume, 'Be_Num_ope', Be_Num_ope
  print *, 'Num_ope', Num_ope

  ! topomove_num
  if (topo_rate >= 0.d0) then
    call Find_new_sense(sense_hist_ope(:), sense_ope(:), Num_ope, next_volume, int(topo_rate*Num_ope), find_rate)
  else
    sense_hist_ope(:) = sense_ope(:)
  end if

  print *, 'find_rate', find_rate

  ! 基準値の決定
  tmp_pack(:) = dqsort(sense_hist_ope(:))
  Vp0 = (tmp_pack(Num_ope - next_volume) + tmp_pack(Num_ope - next_volume - 1)) / 2.d0

  do i = 1, Num_ope
    if (sense_hist_ope(i) >= Vp0) then
      Be_ope(i) = 1
    else
      Be_ope(i) = 0
    end if
  end do

  Be_Num_ope = count(Be_ope(:)==1)
  Be_local(:) = unpack(Be_ope(:), Cre_Map_local(:)==1, Be_local(:))

  print *, 'Be_Num_ope_after', Be_Num_ope

  ! 対称部分にミラー
  do i = 1, sym_nodeNum
    do j = 1, sym_num
      Be(sym_node(i,j)) = Be_local(i)
    end do
  end do

  ! 面対称
  ! if (symType==2) then
  !   next_volume = next_volume / 2
  !   ! 軸対称
  ! elseif (symType==3) then
  !   next_volume = next_volume / 4
  !   ! 非対称
  ! else
  !   ! next_volume = next_volume
  !   continue
  ! end if

  ! 対称部分を抽出
  ! sym_gridNum = size(sym_node(:,:), dim=1)
  ! allocate(Cre_Map_local(sym_gridNum))
  ! allocate(Be_local(sym_gridNum))
  ! allocate(Sen_arange(sym_gridNum))
  ! do i = 1, sym_gridNum
  !   Cre_Map_local(i) = Cre_Map(sym_node(i,1))
  !   Sen_arange(i) = Nd_sen(sym_node(i,1))
  ! end do

  ! 更新領域を参照して、基準値の決定
  ! count update domain nodes
  ! i_count = count(Cre_Map_local(:)==1)
  ! allocate(update_dom_sense(i_count))
  ! update_dom_sense(1:i_count) = pack(Sen_arange(:), Cre_Map_local(:)==1)
  ! update_dom_sense(:) = dqsort(update_dom_sense(:))
  ! ! Sen_arange(:) = unpack(update_dom_sense(:), Cre_Map_local(:)==1, Sen_arange(:))
  ! ! comment: unpackする必要ない気がする
  !
  ! Vp0 = (update_dom_sense(i_count-next_volume) + update_dom_sense(i_count-next_volume-1))/2.d0

  ! 荷重面など特定の部分は、感度係数の最大値を与えて保護
  max_sen = maxval(Nd_sen(:))
  do i = 1, Nd
    if (Cre_Map(i) == 0) then
      Nd_sen(i) = max_sen
    end if
  end do

  ! do i = 1, sym_gridNum
  !   if (Cre_Map_local(i) == 1) then
  !     if (Nd_sen(sym_node(i,1)) >= Vp0) then
  !       Be_local(i) = 1
  !     else
  !       Be_local(i) = 0
  !     end if
  !   else
  !     Be_local(i) = Be(sym_node(i,1))
  !   end if
  ! end do

  ! do i = 1, sym_gridNum
  !   do j = 1, size(sym_node(:,:), dim=2)
  !     Be(sym_node(i,j)) = Be_local(i)
  !   end do
  ! end do

  ! 収束判定
  if (kstep > 1) then
    ! 位相変化量が移動幅の内部
    if (e_rate/2.d0*Nd1_ope <= count(Be_pre(:) /= Be(:))) then
      if ( target_Nd*(1.d0 - e_rate/2.d0) <= Nd1_ope &
        & .and. Nd1_ope <= target_Nd*(1.d0 + e_rate/2.d0) ) then
        count_convergence = count_convergence + 1
        if (count_convergence >= convergence_num) then
          stop "count_convergence is reached to convergence_num"
        end if
      else
        count_convergence = 0
      end if
    else
      count_convergence = 0
    end if
  end if

  Be_pre(:) = Be(:)
  Nd1 = count(Be(:)==1)
  Nd1_ope = count(pack(Nd_sen(:)>=Vp0, Cre_Map(:)==1))

end subroutine Generate_NextModel

!-------------------------------------------------------------------------------
! subroutine Find_new_sense
! トポロジー変化量を満たす感度場を算出
!-------------------------------------------------------------------------------
subroutine Find_new_sense(sense_pre, sense_new, variable_num, inside_num, topomove_num, div_rate)
  use STD_CAL
  real(8), intent(inout) :: sense_pre(:)
  real(8), intent(in) :: sense_new(:)
  integer, intent(in) :: variable_num
  integer, intent(in) :: inside_num
  integer, intent(in) :: topomove_num
  real(8), intent(out) :: div_rate

  integer :: roop_max = 100
  real(8) :: div_head, div_tail
  real(8) :: bound_val ! しきい値
  integer :: move_cell
  integer :: topo_map_pre(variable_num), topo_map(variable_num)
  real(8) :: sense_mid(variable_num)
  real(8) :: tmp_listsort(variable_num)

  integer :: roop_count
  integer :: i

  div_rate = 1.d0

  ! 初期マップの作成
  tmp_listsort(:) = dqsort(sense_pre(:))
  bound_val = tmp_listsort(variable_num - inside_num + 1)
  do i = 1, variable_num
    if(sense_pre(i) >= bound_val) then
      topo_map_pre(i) = 1
    else
      topo_map_pre(i) = 0
    end if
  end do

  ! print *, 'topo_map_pre inside', count(topo_map_pre(:)==1), 'bound_val', bound_val

  ! 次ステップの感度による位相変化量のチェック
  tmp_listsort(:) = dqsort(sense_new(:))
  bound_val = tmp_listsort(variable_num - inside_num + 1)
  do i = 1, variable_num
    if(sense_new(i) >= bound_val) then
      topo_map(i) = 1
    else
      topo_map(i) = 0
    end if
  end do

  move_cell = count(topo_map_pre(:)/=topo_map(:))
  ! print *, 'topo_map_pre inside', count(topo_map_pre(:)==1), 'bound_val', bound_val

  print *, 'movecell origin :', move_cell
  print *, 'state', move_cell, topomove_num

  ! 位相変化量が十分小さい
  if (move_cell <= topomove_num) then
    sense_pre(:) = sense_new(:)
    print *, 'normal', move_cell, topomove_num
    return
  end if

  ! 位相変化量に基づく次ステップマップの決定
  div_rate = 0.5d0
  div_head = 1.d0
  div_tail = 0.d0
  roop_count = 0
  do
    sense_mid(:) = sense_pre(:)*(1.d0 - div_rate) + sense_new(:)*div_rate

    tmp_listsort(:) = dqsort(sense_mid(:))
    bound_val = tmp_listsort(variable_num - inside_num + 1)
    do i = 1, variable_num
      if(sense_mid(i) >= bound_val) then
        topo_map(i) = 1
      else
        topo_map(i) = 0
      end if
    end do

    move_cell = count(topo_map_pre(:)/=topo_map(:))

    ! 2分探索の更新
    if (move_cell < topomove_num) then
      div_tail = div_rate
      div_rate = (div_tail + div_head) / 2.d0
    elseif (move_cell > topomove_num) then
      div_head = div_rate
      div_rate = (div_tail + div_head) / 2.d0
    else
      ! 終了
      sense_pre(:) = sense_mid(:)
      print *, 'converge'
      print *, 'movecell fixed: ', move_cell
      return
    end if

    ! 終了条件
    if (roop_count >= roop_max) then
      sense_pre(:) = sense_mid(:)
      print *, 'max loop'
      print *, 'movecell fixed: ', move_cell
      return
    end if

    roop_count = roop_count + 1

  end do

end subroutine Find_new_sense

!-------------------------------------------------------------------------------
! subroutine Dicide_RefVal
! 体積一定削除手法における基準値の決定
!-------------------------------------------------------------------------------
subroutine Dicide_RefVal(sen_val, next_volume, hold_volume, ref_val, err_val)
  implicit none
  real(8), dimension(:) :: sen_val
  integer, intent(in) :: next_volume, hold_volume
  real(8), intent(out) :: ref_val
  real(8), intent(in) :: err_val

  real(8) :: max, sen_x
  integer :: range

  integer :: i, j, k

  range = size(sen_val(:))

  do i=1,range-1
    max=sen_val(i)
    k=i
    do j=i+1,range
      if(sen_val(j) > max)then
        max=sen_val(j)
        k=j
      end if
    end do
    sen_x=sen_val(i)
    sen_val(i)=max
    sen_val(k)=sen_x
  end do
  print *, 'next_volume, hold_volume : ', next_volume, hold_volume
  ref_val = sen_val(next_volume - hold_volume) + err_val

end subroutine Dicide_RefVal

!-------------------------------------------------------------------------------
! subroutine make_Be
! 設計変数配列Beを要素の敏感数と基準値をもとに生成
!-------------------------------------------------------------------------------
subroutine make_Be(ref_val, el_senVal)
  use BESO_DATA
  implicit none
  real(8), intent(in) :: ref_val
  real(8), dimension(:), intent(in) :: el_senVal

  integer :: i

  ! 形態創生の対象となる要素のみBeを更新
  do i = 1, El
    if (Cre_Map(i) == 1) then
      if (ref_val < el_senVal(i)) then
        Be(i) = 1
      else
        Be(i) = 0
      end if
    end if
  end do
end subroutine make_Be

!-------------------------------------------------------------------------------
! subroutine DataArray
! 解析によって得られるデータを格納する配列を用意
!-------------------------------------------------------------------------------
subroutine DataArray
  use BESO_DATA
  implicit none

  allocate(vonMises(El,totalAnalyCase));  vonMises(:,:) = 0.d0
  allocate(dispSense(Nd,totalAnalyCase)); dispSense(:,:) = 0.d0
  if (not(BsenJoinNum==0 .and. buckAnalyNum==0)) then
    allocate(buckSense(Nd,totalAnalyCase)); buckSense(:,:) = 0.d0
    allocate(buckSense_list(Nd, BsenJoinNum, totalAnalyCase)); buckSense_list(:,:,:) = 0.d0
    allocate(buckLoad(BsenJoinNum, totalAnalyCase)); buckLoad(:,:) = 0.d0
  end if

  ! 最適化対象の感度
  allocate(Sensitivity(Nd))

  ! 節点値化した感度
  allocate(Nd_sen(Nd))
  Nd_sen(:) = 1.d0
  Vp0 = 0.d0

  ! フィルター後の要素感度
  allocate(El_sen(El))

end subroutine DataArray

end module BESO_ROUTINE
