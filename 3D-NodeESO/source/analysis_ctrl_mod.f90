module ANALYSIS_CTRL
  use BESO_DATA
  use MODEL_SUPER
  use MODEL_MOD
  use FILE_IO
  implicit none
  character(len=8) dbleForm
contains
!-------------------------------------------------------------------------------
! subroutine Analysis_All
! 各解析ケースについて解析
!-------------------------------------------------------------------------------
subroutine Analysis_All
  use model_routines
  use MATRIX_FORM
  use MATRIX_SOLVER
  use StiffMat_GenMOD
  use GeoMat_GenMOD
  implicit none
  !*-外力*-
  real(8), allocatable :: F_vec(:)
  !*-変位*-
  real(8), allocatable :: U_vec(:)
  real(8), allocatable :: eigen_vector(:,:)

  ! マトリクス
  real(8), allocatable :: gk_CSR(:), gkt_CSR(:)

  real(8), parameter :: eps = 1.d-15

  integer :: unchange_sup
  integer :: i

  unchange_sup = 0
  call MDL_DataArray

  call Time_Stamp('モデル荷重・支持情報の作成')
  call Mk_LoadSupport

  do case_i = 1, totalAnalyCase
    write(analyCaseStr,'(I2.2)') case_i

    if (unchange_sup==0) then
      ! 支持条件が変わらなければマトリクス構造は同じ
      call Mk_Dof_ID(1)
      if (allocated(CSR_rowIndex)) deallocate(CSR_rowIndex)
      if (allocated(CSR_columns))  deallocate(CSR_columns)
      if (allocated(F_vec))   deallocate(F_vec)
      if (allocated(U_vec))   deallocate(U_vec)

      allocate(CSR_rowIndex(lc+1))
      allocate(F_vec(lc))
      allocate(U_vec(lc))

      call Time_Stamp('case' // analyCaseStr // 'CSRマトリクス構造の作成')
      ! call MakeCSR_RowSearch_Array
      call MkCSR_Index

      call Time_Stamp('case' // analyCaseStr // '各マトリクス配列の割付')
      if (allocated(gk_CSR)) deallocate(gk_CSR)
      if (allocated(gkt_CSR))deallocate(gkt_CSR)

      allocate(gk_CSR(CSRMat_len))
      if (eigenSolveNum > 0) then
        allocate(gkt_CSR(CSRMat_len))
      end if
    end if

    ! 外力ベクトルの作成
    call ExternalForce(case_i)
    F_vec(:) = pack(force(:), icrs(:)/=0)

    ! CSR剛性マトリクスの作成
    call Time_Stamp('case' // analyCaseStr // 'CSR剛性マトリクスの作成')
    call mkCSRStiff_Mat(gk_CSR(:))

    !%%%%%%% 線形解析 %%%%%%%
    U_vec(:) = F_vec(:)
    call Time_Stamp('case' // analyCaseStr // 'pardiso solverの実行')
    call pardiso_solver(lc, CSRMat_len, CSR_rowIndex(:), CSR_columns(:), &
    & gk_CSR(:), F_vec(:), U_vec(:))

    !%%%%%%% 線形解析結果による物理量計算 %%%%%%%
    ! 歪・応力・ミーゼス応力・変位敏感数・ひずみエネルギー
    call Time_Stamp('case' // analyCaseStr // '線形解析結果の算出・出力')
    call mirror(icrs, U_vec(:), disp(:))

    call cal_strain
    call cal_stress

    ! call Principal_Stress

    ! call Cal_VonMises

    call Cal_dispSense

    call strain_energy(F_vec(:), U_vec(:))

    !%%%%%%% 座屈解析 %%%%%%%
    if (eigenSolveNum > 0) then
      ! call eigen_solve(eigenSolveNum, gkt(:), gk0(:))
      if (allocated(eigen_vector)) deallocate(eigen_vector)
      if (allocated(eigen_value))  deallocate(eigen_value)
      allocate(eigen_vector(lc, eigenSolveNum)); eigen_vector(:,:)=0.d0
      allocate(eigen_value(eigenSolveNum));      eigen_value(:)=0.d0

      call Time_Stamp('case' // analyCaseStr // 'CSR幾何剛性マトリクスの作成')
      call mkCSRGeo_Mat(8, 3, gkt_CSR(:))

      eigen_value(:) = 0.d0

      call Time_Stamp('case' // analyCaseStr // 'ARPACK_CSR線形座屈解析の実行')
      call dsdrv5_CSR(lc, CSRMat_len, CSR_rowIndex(:), CSR_columns(:), gk_CSR(:), &
        & -gkt_CSR(:), eigenSolveNum, eigen_value, eigen_vector)

      call Time_Stamp('case' // analyCaseStr // '線形座屈解析結果の算出・出力')
      !%%%%%%% 座屈解析結果による感度係数等計算 %%%%%%%
      ! 総自由度への転写
      do i = 1, eigenSolveNum
        call mirror(icrs, eigen_vector(:,i), mode_vectors(:,i))
      end do

      ! 敏感数の算出
      call Time_Stamp('case' // analyCaseStr // '座屈敏感数の算出')
      call sus_Buck_sense(MDL_BsenJoinNum, eigen_vector(:,:), gkt_CSR(:))

    end if

    call cal_nodedispSense

    call Time_Stamp('case' // analyCaseStr // 'ケース解析結果の出力')
    !%%%%%%% 各種物理量等,計算結果の設計領域への移動 %%%%%%%
    call DataToBLDArea(case_i)

    !%%%%%%% 解析結果・計算結果のファイル出力 %%%%%%%
    !---ファイル出力---
    call output_analyInfo

    if (eigenSolveNum > 0) then
      call output_eigenInfo
      call OutputMode_inp2
    end if

    call data_inp(disp(:))
    call output_inp

    if (case_i < totalAnalyCase) then
      if (analyCase_list(case_i,1)==analyCase_list(case_i+1,1)) then
        unchange_sup = 1
      else
        unchange_sup = 0
      end if
    end if
  end do

  call Time_Stamp('モデル情報の開放')

  if (allocated(CSR_rowIndex)) deallocate(CSR_rowIndex)
  if (allocated(CSR_columns))  deallocate(CSR_columns)

  if (allocated(F_vec))   deallocate(F_vec)
  if (allocated(U_vec))   deallocate(U_vec)

  if (allocated(eigen_vector)) deallocate(eigen_vector)
  if (allocated(eigen_value))  deallocate(eigen_value)

  if (allocated(gk_CSR)) deallocate(gk_CSR)
  if (allocated(gkt_CSR))deallocate(gkt_CSR)

  call MDL_FreeDataArray

end subroutine Analysis_All

!-------------------------------------------------------------------------------
! subroutine Mk_LoadSupport
! モデルにおける荷重・支持節点IDの登録
!-------------------------------------------------------------------------------
subroutine Mk_LoadSupport
  integer :: counter
  integer :: i, j, k
  allocate(MDL_support(support_caseNum), MDL_load(load_caseNum))
  ! モデルに含まれる支持節点を探索
  MDL_support(1)%num(:) = 0
  do i = 1, 3
    do j = 1, NFIX_1(i)
      if (grid_node_map(NFIXP_1(j,i)) /= 0) then
        MDL_support(1)%num(i) = MDL_support(1)%num(i) + 1
      end if
    end do
  end do

  ! 探索結果に基づいてモデルの支持節点を設定
  MDL_support(1)%len = maxval(MDL_support(1)%num(:))
  allocate(MDL_support(1)%nodes(MDL_support(1)%len,3))
  MDL_support(1)%nodes(:,:) = 0
  do i = 1, 3
    counter = 0
    do j = 1, NFIX_1(i)
      if (grid_node_map(NFIXP_1(j,i)) /= 0) then
        counter = counter + 1
        MDL_support(1)%nodes(counter,i) = grid_node_map(NFIXP_1(j,i))
      end if
    end do
  end do

  do i = 1, load_caseNum
    ! モデルに含まれる荷重節点を探索
    MDL_load(i)%num(:) = 0
    do j = 1, 3
      do k = 1, load_data(i)%Num_vec(j)
        if (grid_node_map(load_data(i)%ID_vec(k,j)) /= 0) then
          MDL_load(i)%num(j) = MDL_load(i)%num(j) + 1
        end if
      end do
    end do

    ! 探索結果に基づいてモデルの荷重条件を設定
    MDL_load(i)%len = maxval(MDL_load(i)%num(:))
    allocate(MDL_load(i)%nodes(MDL_load(i)%len,3))
    allocate(MDL_load(i)%vals(MDL_load(i)%len,3))
    MDL_load(i)%nodes(:,:) = 0
    MDL_load(i)%vals(:,:) = 0.d0
    do j = 1, 3
      counter = 0
      do k = 1, load_data(i)%Num_vec(j)
        if (grid_node_map(load_data(i)%ID_vec(k,j)) /= 0) then
          counter = counter + 1
          MDL_load(i)%nodes(counter,j) = grid_node_map(load_data(i)%ID_vec(k,j))
          MDL_load(i)%vals(counter,j) = load_data(i)%Val_vec(k,j)
        end if
      end do
    end do
  end do

end subroutine Mk_LoadSupport

!-------------------------------------------------------------------------------
! subroutine ExternalForce_tet
! 外力ベクトルを設定(自重がある場合設定)
!-------------------------------------------------------------------------------
subroutine ExternalForce(load_id)
  use PHYSICAL_DATA
  integer, intent(in) :: load_id
  integer :: i, j, k
  integer :: node_i, dof_k

  force(:) = 0.d0

  do i = 1, 3
    do j = 1, MDL_load(load_id)%num(i)
      force((MDL_load(load_id)%nodes(j,i)-1)*3+i) = MDL_load(load_id)%vals(j,i)
    end do
  end do

  !TODO:四面体の体積の扱いが微妙なので一次停止
  ! 自重の設定
  ! if(grav_weight==0) return
  ! do i = 1, MDL_elmNum
  !   do j = 1, 8
  !     node_i = MDL_elm_node(i,j)
  !     do k = 1, 3
  !       dof_k = (node_i-1)*3 + k
  !       if (k==abs(gravityDir)) then
  !         force(dof_k) = force(dof_k) + unitVolume*density_list(mte(i))*sign(1,gravityDir)/8
  !       end if
  !     end do
  !   end do
  ! end do
end subroutine ExternalForce

!===============================================================================
! subroutine strain_tet
!  要素中心のひずみの計算
!===============================================================================
subroutine cal_strain
  use StiffMat_GenMOD
  integer :: i,j,k,l,jj
  integer,dimension(12) :: list
  real(8) :: dndxi(4), dndeta(4), dndzeta(4)

  do i=1, MDL_elmNum
    !	形状関数の微分
    call dshape_function(dndxi(:), dndeta(:), dndzeta(:))
    !	ヤコビ行列
    call make_Jmatrix(i, dndxi(:), dndeta(:), dndzeta(:), detJ, J_invMat(:,:))
    !	ひずみ-変位マトリクス
    call make_Bmatrix(dndxi(:), dndeta(:), dndzeta(:), J_invMat(:,:))

    !  変位の対応表を作成
    do j=1,4
      do k=1,3
        list(3*(j-1)+k) = 3*(MDL_elm_node(i,j)-1) + k
      end do
    end do

    strain(i,:) = 0.d0
    do k=1,6
      do l=1,12
        jj = list(l)
        strain(i,k) = strain(i,k) + Bmatrix(k,l) * disp(jj)
      end do
    end do

  end do

end subroutine cal_strain

!===============================================================================
! subroutine cal_stress_tet
! 要素中心の応力の計算
!===============================================================================
subroutine cal_stress
  use StiffMat_GenMOD
  integer :: i, j, k

  stress(:,:) = 0.0d0
  do i=1,MDL_elmNum
    !  応力-ひずみマトリクス
    call make_Dmatrix
    do j=1,6
      do k=1,6
        stress(i,j) = stress(i,j) + Dmatrix(j,k) * strain(i,k)
      end do
    end do
  end do

end subroutine cal_stress

!-------------------------------------------------------------------------------
! subroutine cal_dispSense_tet
! 要素中心の変位敏感数の計算
!-------------------------------------------------------------------------------
subroutine cal_dispSense
  use STD_CAL
  use StiffMat_GenMOD

  integer :: n1,ie,i,j,k
  real(8) :: dl_sns
  real(8),dimension(12) :: disp_sum,delta

  MDL_dispSense(:)=0.0d0

  call make_Dmatrix

  do ie=1,MDL_elmNum
    k=0
    dl_sns=0.d0
    disp_sum=0.d0
    delta=0.d0

    call mkStiff_elMat(ie) !要素剛性マトリクスの作成

    !要素を構成する節点番号の変位を求める
    do i=1,4
      do j=1,3
        k=k+1
        n1=3*(MDL_elm_node(ie,i)-1)+j
        delta(k)=disp(n1)
      end do
    end do

    do i=1,12
      do j=1,12
        disp_sum(j)=disp_sum(j)+delta(i)*elm_Mat(i,j)
      end do
    end do

    do i=1,12
      dl_sns=dl_sns+delta(i)*disp_sum(i)
    end do

    MDL_dispSense(ie) = dl_sns / detJ
  end do

  ! 統計量の算出
  DsenMax  = maxval(MDL_dispSense(:))
  DsenMin  = minval(MDL_dispSense(:))
  DsenMean = sum(MDL_dispSense(:)) / size(MDL_dispSense(:))
  DsenStd  = std(MDL_dispSense(:))

end subroutine cal_dispSense

!-------------------------------------------------------------------------------
! subroutine cal_nodedispSense
! 敏感数節点値化
!-------------------------------------------------------------------------------
subroutine cal_nodedispSense
  implicit none
  integer :: node_j
  integer, allocatable :: sense_sumcount(:)

  integer :: i, j

  allocate(sense_sumcount(MDL_nodeNum)); sense_sumcount(:)=0
  MDL_nodedispSense(:) = 0.d0

  do i = 1, MDL_elmNum
    do j = 1, 4
      node_j = MDL_elm_node(i,j)
      sense_sumcount(node_j) = sense_sumcount(node_j) + 1
      MDL_nodedispSense(node_j) = MDL_nodedispSense(node_j) + MDL_dispSense(i)
      MDL_nodebuckSense(node_j,:) = MDL_nodebuckSense(node_j,:) + MDL_buckSense_list(i,:)
    end do
  end do

  do i = 1, MDL_nodeNum
    MDL_nodedispSense(i) = MDL_nodedispSense(i) / sense_sumcount(i)
    MDL_nodebuckSense(node_j,:) = MDL_nodebuckSense(node_j,:) / sense_sumcount(i)
  end do

end subroutine cal_nodedispSense

!-------------------------------------------------------------------------------
! subroutine MDL_DataArray
! モデルの解析データ等を格納する配列を用意
! MDL_nodeNum,MDL_elmNumの長さのデータ配列
!-------------------------------------------------------------------------------
subroutine MDL_DataArray
  use BESO_DATA, only: buckAnalyNum, BsenJoinNum
  use MODEL_MOD
  implicit none

  ! 座屈解析の求めるモード数を決定
  MDL_BsenJoinNum = BsenJoinNum
  ! 解析モード数と合成数多いほう
  eigenSolveNum = max(BsenJoinNum,buckAnalyNum)

  ! 節点データ群
  allocate(icrs (MDL_nodeNum*3)); icrs(:)=0
  allocate(force(MDL_nodeNum*3)); force(:)=0.d0
  allocate(disp (MDL_nodeNum*3)); disp(:)=0.d0
  allocate(MDL_nodedispSense(MDL_nodeNum)); MDL_nodedispSense(:)=0.d0
  if (eigenSolveNum/=0) then
    allocate(mode_vectors(MDL_nodeNum*3,eigenSolveNum)); mode_vectors(:,:)=0.d0
    allocate(eigen_value(eigenSolveNum)); eigen_value(:)=0.d0
  end if

  ! 要素データ群
  allocate(MDL_elm_volume(MDL_elmNum)); MDL_elm_volume(:)=0.d0
  allocate(strain(MDL_elmNum,6)); strain(:,:)=0.d0
  allocate(stress(MDL_elmNum,6)); stress(:,:)=0.d0
  allocate(pStress(MDL_elmNum,3)); pStress(:,:) = 0.d0
  allocate(pStressVec(MDL_elmNum,3,3)); pStressVec(:,:,:) = 0.d0

  ! 敏感数データ群
  allocate(MDL_vonMises (MDL_elmNum)); MDL_vonMises(:)=0.d0
  allocate(MDL_dispSense(MDL_elmNum)); MDL_dispSense(:)=0.d0
  if (eigenSolveNum > 0) then
    allocate(MDL_nodebuckSense(MDL_nodeNum,BsenJoinNum)); MDL_nodebuckSense(:,:)=0.d0
    allocate(MDL_buckSense_list(MDL_elmNum,BsenJoinNum)); MDL_buckSense_list(:,:)=0.d0
    allocate(BsenMax(BsenJoinNum), BsenMin(BsenJoinNum), BsenMean(BsenJoinNum), BsenStd(BsenJoinNum))
  end if

end subroutine MDL_DataArray

!-------------------------------------------------------------------------------
! subroutine MDL_FreeDataArray
! モデルの解析データ等を格納する配列をメモリーから開放
! MDL_nodeNum,MDL_elmNumの長さのデータ配列
!-------------------------------------------------------------------------------
subroutine MDL_FreeDataArray
  use MODEL_MOD
  implicit none

  ! 節点データ群
  if (allocated(icrs))  deallocate(icrs )
  if (allocated(force)) deallocate(force)
  if (allocated(disp))  deallocate(disp )
  if (allocated(mode_vectors)) deallocate(mode_vectors)
  if (allocated(eigen_value))  deallocate(eigen_value)
  if (allocated(MDL_nodedispSense)) deallocate(MDL_nodedispSense)

  ! 要素データ群
  if (allocated(MDL_elm_volume)) deallocate(MDL_elm_volume)
  if (allocated(strain)) deallocate(strain)
  if (allocated(stress)) deallocate(stress)
  if (allocated(pStress)) deallocate(pStress)
  if (allocated(pStressVec)) deallocate(pStressVec)

  ! 敏感数データ群
  if (allocated(MDL_vonMises))  deallocate(MDL_vonMises )
  if (allocated(MDL_dispSense)) deallocate(MDL_dispSense)

  if (allocated(MDL_nodebuckSense)) deallocate(MDL_nodebuckSense)

  if (allocated(MDL_buckSense_list)) deallocate(MDL_buckSense_list)
  if (allocated(BsenMax)) deallocate(BsenMax)
  if (allocated(BsenMin)) deallocate(BsenMin)
  if (allocated(BsenMean)) deallocate(BsenMean)
  if (allocated(BsenStd)) deallocate(BsenStd)

  if (allocated(MDL_support)) deallocate(MDL_support)
  if (allocated(MDL_load)) deallocate(MDL_load)

end subroutine MDL_FreeDataArray

!-------------------------------------------------------------------------------
! subroutine Input_Geometry
! モデルの節点座標配列,要素-節点関係配列,材料配列の生成
!-------------------------------------------------------------------------------
subroutine Input_Geometry
  use BESO_DATA
  use MODEL_MOD

  integer :: i, j

  ! 1グリッド分の体積
  !unitVolume = Hx*Hy*Hz

  ! 節点座標をモデル座標配列に入力
  allocate(MDL_nodePos(MDL_nodeNum,3))
  do i = 1, Nd
    if(model_nodeID(i)==0) cycle
    MDL_nodePos(model_nodeID(i),:) = Nd_in(i,:)
  end do

  ! 要素節点関係をモデル節点IDに書き換えて生成
  allocate(MDL_elm_node(MDL_elmNum,8))
  allocate(mte(MDL_elmNum))
  do i = 1, El
    if(model_elID(i)==0) cycle
    do j = 1, 8
      ! モデル節点IDに書き換え
      MDL_elm_node(model_elID(i),j) = model_nodeID(El_in(i,j))
    end do
    mte(model_elID(i)) = material_map(i)
  end do

end subroutine Input_Geometry

!-------------------------------------------------------------------------------
! subroutine DataToBLDArea
! モデルの解析によって得られたデータを設計領域に移動
!-------------------------------------------------------------------------------
subroutine DataToBLDArea(analyCase)
  use BESO_DATA
  use MODEL_MOD
  implicit none
  integer, intent(in) :: analyCase
  integer :: i


  vonMises (:,analyCase) = 0.d0
  dispSense(:,analyCase) = 0.d0

  if (eigenSolveNum>0) then
    buckLoad(1:MDL_BsenJoinNum,analyCase) = eigen_value(1:MDL_BsenJoinNum)
    buckSense(:,analyCase) = 0.d0
    buckSense_list(:,:,analyCase) = 0.d0
  end if

  dispSense(1:Nd, analyCase) = unpack(MDL_nodedispSense(1:grid_node_model_num), &
    & grid_node_map(1:Nd)/=0, field=Nd_sen(1:Nd))

  step_volume = sum(MDL_elm_volume(:))

  ! do i = 1, El
  !   if(model_elID(i)==0) cycle
  !   vonMises (i,analyCase) = MDL_vonMises (model_elID(i))
  !   dispSense(i,analyCase) = MDL_dispSense(model_elID(i))
  if (eigenSolveNum > 0) then
    do i = 1, MDL_BsenJoinNum
      buckSense_list(1:Nd, i, analyCase) = unpack(MDL_nodebuckSense(1:grid_node_model_num, i), &
      & grid_node_map(1:Nd)/=0, field=buckSense_list(1:Nd, i, analyCase))
      ! buckSense_list(i,:,analyCase) = MDL_buckSense_list(model_elID(i),:)
    end do
  end if
  ! end do

end subroutine DataToBLDArea

!-------------------------------------------------------------------------------
! subroutine FreeModel_Geometry
! モデルの節点座標配列,要素-節点関係配列,材料配列のメモリー開放
!-------------------------------------------------------------------------------
subroutine FreeModel_Geometry
  use MODEL_MOD
  if (allocated(MDL_nodePos)) deallocate(MDL_nodePos)
  if (allocated(MDL_elm_node)) deallocate(MDL_elm_node)
  if (allocated(mte))  deallocate(mte)

end subroutine FreeModel_Geometry

!===================================================================
subroutine data_inp(disp)
!===================================================================
  implicit none
  real(8), intent(in) :: disp(:)
  integer :: i

!  --
  open(10,file='./dataout/testout/'//'dataout_analy.inp', action='write')
  write(10,'("1")')
  write(10,'("data_geom")')

  write(10,'("step1")')

  write(10,'(2(I8,1X))') MDL_nodeNum, MDL_elmNum

  do i = 1, MDL_nodeNum
    write(10,fmt='(I8,3(X,E15.7))') i, MDL_nodePos(i,:)
  end do
  do i = 1, MDL_elmNum
    write(10,fmt='(I8, I3, A, 4(2X,I8))') i, 1, ' tet', MDL_elm_node(i,:)
  end do
  write(10,*) '4  7'
  write(10,*) '4 1 1 1 1'
  write(10,*) 'u,'
  write(10,*) 'v,'
  write(10,*) 'w,'
  write(10,*) 'disp_node,'
  do i = 1, MDL_nodeNum
    write(10,'(I8,12E15.7)')  i, disp(3*(i-1)+1), disp(3*(i-1)+2), disp(3*(i-1)+3), MDL_nodedispSense(i)
  end do

  write(10,*) '7 1 1 1 1 1 1 1'
  write(10,*) 'sig-xx,'
  write(10,*) 'sig-yy,'
  write(10,*) 'sig-zz,'
  write(10,*) 'sig-xy,'
  write(10,*) 'sig-yz,'
  write(10,*) 'sig-zx,'
  write(10,*) 'dispsense,'
  do i = 1, MDL_elmNum
    write(10, '(I8,7(1X,E15.7))') i, stress(i,:), MDL_dispSense(i)
  end do
  close(10)

end subroutine data_inp

!=====================================================================
subroutine output_inp
  !                    解析形状のデータ出力(.inp)
  ! 出力データ内容
  ! 節点データ:節点変位
  ! 要素データ:応力、mises応力/変異?感数、座屈?感数(今は、荷重係数そのもの)
!=====================================================================
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
   implicit none

  integer :: i, k

  open(4,file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // '.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2(I8,X))') MDL_nodeNum, MDL_elmNum

  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,fmt='(I8,3(1X,E15.7))') i, MDL_nodePos(i,1:3)
  end do

  do i=1,MDL_elmNum  !要素数 要素の節点番号
    write(4,'(I8, I3,"    tet", 4(X,I8))') i, 1, MDL_elm_node(i,:)
  end do

  write(4,'(2(I2,1X))') 3, MDL_BsenJoinNum+2

  ! -----節点データ-----
  !データ構造
  write(4,'("3 1 1 1")')

  !データラベル
  write(4,'("u,"/,"v,"/,"w,")')

  !データ
  do i=1,MDL_nodeNum
    write(4,'(I8,12E15.7)') i,disp(3*(i-1)+1), disp(3*(i-1)+2), disp(3*(i-1)+3)
  end do

  !-----要素データ-----
  ! データ構造
  write(4,'(I2,1X)', advance='no') 2 + MDL_BsenJoinNum
  do i = 1, MDL_BsenJoinNum+2
    write(4,'(I2,1X)', advance='no') 1
  end do
  write(4,*)

  ! データラベル
  write(4,'("VonMises,"/," Disp_sens,")')
  do i = 1, MDL_BsenJoinNum
    write(4,'("Buck_sens", I2.2, ",")') i
  end do

  ! データ
  do i=1,MDL_elmNum
    if (not(MDL_BsenJoinNum==0)) then
      write(4,'(I8,1X,<MDL_BsenJoinNum+2>(E15.7,1X))') i, MDL_vonMises(i), MDL_dispSense(i), MDL_buckSense_list(i,:)
    else
      write(4,'(I8,1X,2(E15.7,1X))') i, MDL_vonMises(i), MDL_dispSense(i)
    end if
  end do

  close(4)

end subroutine output_inp

!=====================================================================
! subroutine OutputGeometory_inp
!                    解析形状のデータ出力(.inp)
! 出力データ内容
! 節点データ:節点変位
! 要素データ:応力、mises応力/変異?感数、座屈?感数(今は、荷重係数そのもの)
!=====================================================================
subroutine OutputGeometory_inp(file_name)
  use BESO_SUPER
  use MODEL_MOD
   implicit none
  character(*), intent(in) :: file_name
  integer :: i, k

  open(4,file='dataout/analysis/'// file_name // '.inp')
  write(4,'("#",a,".inp")') file_name
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2(I8,X))') MDL_nodeNum, MDL_elmNum

  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,fmt='(I8,3(1X,E15.7))') i,MDL_nodePos(i,1),MDL_nodePos(i,2),MDL_nodePos(i,3)
  end do

  do i=1,MDL_elmNum  !要素数 要素の節点番号
    write(4,'(I8, I3,"    tet", 4(X,I8))') i, 1 ,MDL_elm_node(i,:)
  end do

  write(4,'("1  1")')

  write(4,'("1 1"/,"dummy,")')
  do i=1,MDL_nodeNum  !節点数 節点変位
    write(4,'(I8,e15.7)') i, 1.d0
  end do

  ! それぞれのデータラベル
  write(4,'("1 1")')
  write(4,'("dummy,")')
  do i=1,MDL_elmNum
    write(4,'(I8,e15.7)') i, 1.d0
  end do

  close(4)
end subroutine OutputGeometory_inp

!=====================================================================
subroutine OutputMode_inp2
!解析形状のデータ出力(.inp)
!=====================================================================
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
  implicit none

  integer :: i, j, k
  real(8), allocatable :: node_mode(:)

  allocate(node_mode(eigenSolveNum*3))

  open(4,file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // 'mode2.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i8)') MDL_nodeNum, MDL_elmNum
  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,'(1x,i8,3E15.7)') i,MDL_nodePos(i,1),MDL_nodePos(i,2),MDL_nodePos(i,3)
  end do

  do i=1,MDL_elmNum  !要素数 要素の節点番号
    write(4,'(i8,i5 ,"    tet ",8i10)') i, 1 ,MDL_elm_node(i,:)
    ! write(4,'(i7,i5 ,"    hex ",8i10)') i, mte(i) ,MDL_elm_node(i,:)
  end do

  write(4,'(2(I4,2X))') 3*eigenSolveNum, 18

  ! -----節点データ-----
  !データ構造
  write(4,'(i4)', advance='no') 3*eigenSolveNum
  do i = 1, 3*eigenSolveNum
    write(4,'(i4)', advance='no') 1
  end do
  write(4,*)

  !データラベル
  do i = 1, eigenSolveNum
    write(4,'("u",i2.2,","/,"v",i2.2,","/,"w",i2.2,",")') i, i, i
  end do

  !データ
  do i=1,MDL_nodeNum
    do j = 1, eigenSolveNum
      do k = 1, 3
        node_mode((j-1)*3+k) = mode_vectors((i-1)*3+k,j)
      end do
    end do
    write(4,'(i8,<eigenSolveNum*3>E15.7)') i, node_mode(:)
  end do

  !-----要素データ-----
  ! データ構造
  write(4,'("18 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1")')

  ! データラベル
  write(4,'(" sigma-x,"/," sigma-y,"/," sigma-z,"/," sigma-xy,"/," sigma-xz,"/," sigma-yz,"/," p1,"/," p2,"/," p3,"/,"p1u,"/,"p1v,"/,"p1w,"/,"p2u,"/,"p2v,"/,"p2w,"/,"p3u,"/,"p3v,"/,"p3w,")')

  ! データ
  do i=1,MDL_elmNum
    write(4,'(i8,21E15.7)') i,stress(i,:), pStress(i,:), pStressVec(i,1,1:3), pStressVec(i,2,1:3), pStressVec(i,3,1:3)
  end do

  close(4)

end subroutine OutputMode_inp2

!=====================================================================
subroutine OutputBuild_inp
!                    設計領域上のデータ出力(.inp)
! 出力データ内容
! 節点データ:なし
! 要素データ:敏感数(今は、荷重係数そのもの)
!=====================================================================
use BESO_SUPER
use BESO_DATA
 implicit none

  integer :: i, j

  open(4,file='dataout/build_domain/' // modelName // '_' // kstepStr // 'build.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i8)') Nd,El

  do i=1,Nd  !節点数 節点の座標
    write(4,'(i8,3E15.7)') i,Nd_in(i,:)
  end do

  do i=1,El  !要素数 要素の節点番号
    write(4,'(i8,i5,"    hex ",8i10)') i,Cre_Map(i),El_in(i,:)
  end do

  ! 節点のデータ数、要素のデータ数
  write(4,'(2(I10,1X))') 1+BsenJoinNum*totalAnalyCase, 3

  ! 節点データラベル
  write(4,'(i7, 1x, <BsenJoinNum*totalAnalyCase+1>("1",1X))') BsenJoinNum*totalAnalyCase+1
  write(4,'("Sense Node,")')
  do i = 1, totalAnalyCase
    do j = 1, BsenJoinNum
      write(4,'("BuckSense", I2.2, "C", I2.2, ",")') j, i
    end do
  end do

  ! 節点データ
  do i=1,Nd  !節点数 節点変位
    write(4,'(i8,E15.7,<BsenJoinNum*totalAnalyCase>(1X,E15.7))') i, Nd_sen(i), (buckSense_list(i,1:BsenJoinNum,j),j=1,totalAnalyCase)
  end do

  ! 要素データ構造
  write(4,'(I2,1X,3("1",1X))',advance='no') 3
  write(4,*)

  ! 要素データラベル
  write(4,'(" Sense Origin,"/,"Sense Filtered,"/,"Be,")')

  ! 要素データ値
  do i=1,El
    write(4,'(i8,2(1X,E15.7), i7)') i, Sensitivity(i), El_sen(i), Be(i)
  end do

  close(4)

end subroutine OutputBuild_inp

!-------------------------------------------------------------------------------
! subroutine output_analyInfo
! 解析ケースごとの解析・敏感数算出結果を出力
! 160605 : @yamazaki 荷重ケースごとにファイルを分けた。ステップ経過が追えるように。
!-------------------------------------------------------------------------------
subroutine output_analyInfo
  use BESO_SUPER, only: kstep
  use MODEL_MOD
  implicit none
  integer :: i

  if (kstep == 1) then
    open(300, file='./dataout/analysis/analyInfo-Case' // analyCaseStr // '.txt')
    write(300,'(12(A,1X))', advance='no') 'step', '内力E', '外力E', '1次固有値', &
                 & 'Mises平均', 'Mises標準偏差', 'Mises最大', 'Mises最小', &
                 & '変位平均', '変位標準偏差', '変位最大', '変位最小'
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') '座屈平均', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') '座屈標準偏差', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') '座屈最大', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') '座屈最小', i
    end do
    write(300,*)
  else
    open(300, file='./dataout/analysis/analyInfo-Case' // analyCaseStr // '.txt', position='append')
  end if

  if (not(MDL_BsenJoinNum==0)) then
    write(300, '(I4, 1X, <MDL_BsenJoinNum*4 + 11>(E15.7,1X))') kstep, V_energyCase_i, U_energyCase_i, eigen_value(1), &
    & vonMean, vonStd, vonMax, vonMin, &
    & DsenMean, DsenStd, DsenMax, DsenMin, &
    & BsenMean(1:MDL_BsenJoinNum), BsenStd(1:MDL_BsenJoinNum), BsenMax(1:MDL_BsenJoinNum), BsenMin(1:MDL_BsenJoinNum)
  else
    write(300, '(I4, 1X, 10(E15.7,1X))') kstep, V_energyCase_i, U_energyCase_i, &
    & vonMean, vonStd, vonMax, vonMin, &
    & DsenMean, DsenStd, DsenMax, DsenMin
  end if

  close(300)

end subroutine output_analyInfo

!-------------------------------------------------------------------------------
! subroutine Output_eigenInfo
! 固有値の出力
! 160610 : @yamazaki 固有値だけ分離して出力
!-------------------------------------------------------------------------------
subroutine output_eigenInfo
  use BESO_DATA
  use BESO_SUPER
  use MODEL_MOD
  implicit none

  integer :: i
  character mode*2

  if (kstep == 1) then
    open(300, file='./dataout/analysis/eigenInfo-Case' // analyCaseStr // '.txt')
    do i = 1, eigenSolveNum
      write(mode,'(I2.2)') i
      write(300,353, advance='no')  'mode' // mode
    end do
    write(300, *)
  else
    open(300, file='./dataout/analysis/eigenInfo-Case' // analyCaseStr // '.txt', position='append')
  end if

  do i = 1, eigenSolveNum
    write(300, 354, advance='no') eigen_value(i)
  end do

  close(300)
353 format(A26)
354 format(E26.15)
end subroutine output_eigenInfo

end module ANALYSIS_CTRL
