module FILE_IO
  implicit none
contains

!-------------------------------------------------------------------------------
! subroutine Time_Stamp
! 解析時間・CPU時間などの記録
!-------------------------------------------------------------------------------
subroutine Time_Stamp(message)
  use BESO_DATA
  implicit none
  character(*), intent(in) :: message

  character :: dateStr*8, timeStr*10
  real(8) :: cpuTime
  integer :: txtLen, diff_tf, t_max
  integer :: tf = 0, tr = 0

  integer, save :: call_first = 1, pre_tf = 0
  character, save :: pre_dateStr, pre_timeStr
  real(8), save :: pre_cpuTime=0.d0, pre_realTime

  txtLen = len(message)
  call date_and_time(dateStr, timeStr)

  if (call_first == 1) then
    call_first = 0
    open(200, file='./dataout/overview/time_stamp.txt', action='write')
    ! Header
    write(200,fmt='("Date",8x,"Time",10x,"CPU_Time",7x,"Diff_Time",6x,"Diff_CPU_Time",5x,"Event")')

    ! 時間計測
    call cpu_time(cpuTime)
    call system_clock(tf)
    diff_tf = tf - pre_tf

  else
    open(200, file='./dataout/overview/time_stamp.txt', action='write', access='append')

    ! 時間計測
    call cpu_time(cpuTime)
    call system_clock(tf, tr, t_max)
    if ( tf < pre_tf ) then
      diff_tf = (t_max - pre_tf) + tf + 1
    else
      diff_tf = tf - pre_tf
    endif

  end if

  ! Date
  write(200,fmt='(a4,"/",a2,"/",a2,2x)', advance='no') dateStr(1:4), dateStr(5:6), dateStr(7:)
  ! Time
  write(200,fmt='(a2,":",a2,":",a6,2x)', advance='no') timeStr(1:2), timeStr(3:4), timeStr(5:)
  ! CPU Time
  write(200,fmt='(e15.7,2x)', advance='no') cpuTime
  ! Real diff
  write(200,fmt='(f10.4,2x)', advance='no') diff_tf/dble(tr)
  ! CPU diff
  write(200,fmt='(f10.4,2x)', advance='no') cpuTime - pre_cpuTime
  ! Event
  write(200,fmt='(a1,a<txtLen>,a1)') '"', message, '"'

  pre_tf      = tf
  pre_cpuTime = cpuTime
  pre_timeStr = timeStr
  pre_dateStr = pre_dateStr
  close(200)
end subroutine Time_Stamp

!-------------------------------------------------------------------------------
! subroutine Set_BESO_Param
! BESOのパラメータを設定
!-------------------------------------------------------------------------------
subroutine Set_BESO_Param
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: access, file_status
  character :: modelName_in*100

  NAMELIST/ BESO_PARAM/ modelName_in, coeff_von, coeff_disp, coeff_buck, &
    & step_start, step_end, target_rate, e_rate, topo_rate, symType, history, &
    & r_min, filter_degree, Bsen_mixPenalty, BsenJoinNum, buckAnalyNum

  open(200,file='./setting/beso_param.set',action='read')
  read(200,NML=BESO_PARAM)
  close(200)

  file_status = access('./datain/beso_param.dat','r')
  if (file_status==0) then
    print *, 'use file input'
    open(200,file='./datain/beso_param.dat',action='read')
    read(200,NML=BESO_PARAM)
    close(200)
  end if

  modelName = trim(modelName_in)
end subroutine Set_BESO_Param

!-------------------------------------------------------------------------------
! subroutine Init_Build_Area
!   設計領域の設定
!-------------------------------------------------------------------------------
subroutine Init_Build_Area
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: access, file_status

  NAMELIST/ Build_PARAM/ Xn, Yn, Zn, Nx, Ny, Nz

  open(200,file='./setting/beso_param.set',action='read')
  read(200,NML=Build_PARAM)
  close(200)

  file_status = access('./datain/build.dat','r')
  if (file_status==0) then
    print *, 'use file input'
    open(200,file='./datain/build.dat',action='read')
    read(200,NML=Build_PARAM)
    close(200)
  end if

end subroutine Init_Build_Area

!-------------------------------------------------------------------------------
! subroutine Init_Physical_Param
!   initial setting of physical parameter.
!-------------------------------------------------------------------------------
subroutine Init_Physical_Param
  use PHYSICAL_DATA
  implicit none

  NAMELIST/ PhysicNum_PARAM/ mtNum
  NAMELIST/ Physic_PARAM/ young_list, poisson_list, density_list, gravityDir, grav_weight

  open(200,file='./setting/beso_param.set',action='read')
  read(200,NML=PhysicNum_PARAM)
  rewind(200)
  allocate(young_list(mtNum), poisson_list(mtNum), density_list(mtNum))
  read(200,NML=Physic_PARAM)
  close(200)

end subroutine Init_Physical_Param

!-------------------------------------------------------------------------------
! subroutine OutputOverView
!   output calculation overview.
!-------------------------------------------------------------------------------
subroutine OutputOverView
  use BESO_SUPER
  use BESO_DATA
	implicit none
  integer :: str_len
  str_len = len(modelName)
	open(200,file='./dataout/overview/overview.txt', action='write')
	write(200,fmt='("モデル名:",2x,a<str_len>)') modelName
	write(200,fmt='("現ステップ:",2x,i3)')       kstep
	write(200,fmt='("最終ステップ:",2x,i3)')     step_end
  ! 解析終了の確認
  if (kstep<step_end) then
  	write(200,fmt='("計算状態:",2x,a10)')     'incomplete'
  else
  	write(200,fmt='("計算状態:",2x,a10)')     'complete'
  end if
  ! 最適化対象
  if (opt_target==1) then
  	write(200,fmt='("最適化:",2x,a10)')      'Mises'
  elseif (opt_target==2) then
    write(200,fmt='("最適化:",2x,a10)')      'dispSense'
  else
    write(200,fmt='("最適化:",2x,a10)')      'buckring'
  end if
	write(200,fmt='("位相履歴:",2x,i3)')        history
	close(200)
end subroutine OutputOverView

!-------------------------------------------------------------------------------
! subroutine InputOverView
!   input calculation overview.
!-------------------------------------------------------------------------------
subroutine InputOverView
	implicit none
	integer :: access
	character :: tmp_str, buff_str*100
  character(:), allocatable :: in_modelName

	integer :: in_kstep, in_endStep, in_topoHistory
  integer :: in_calc_status, in_opt_target

	! 0が返ってきたら存在している
	if (access('./overview.txt','r')/=0) return

	open(200,file='./overview.txt',action='read')
	read(200,*) tmp_str, buff_str
  in_modelName = trim(buff_str)
	read(200,*) tmp_str, in_kstep
	read(200,*) tmp_str, in_endStep

  ! read calculation status.
	read(200,*) tmp_str, buff_str
  if (buff_str(1:1)=='i') then
    in_calc_status = 0
  elseif (buff_str(1:1)=='c') then
    in_calc_status = 1
  end if

  ! read optimization target.
	read(200,*) tmp_str, buff_str
  if (buff_str(1:1)=='M') then
    in_opt_target = 1
  elseif (buff_str(1:1)=='d') then
    in_opt_target = 2
  elseif (buff_str(1:1)=='b') then
    in_opt_target = 3
  end if

	read(200,*) tmp_str, in_topoHistory

end subroutine InputOverView

!-------------------------------------------------------------------------------
! function AllocInputter
! 入力を受け取り、入力された文字長さの文字を返す。
!-------------------------------------------------------------------------------
function AllocInputter()
  character(:), allocatable :: AllocInputter
  character(100) :: input_char

  read(*,*) input_char
  AllocInputter = trim(input_char)

end function

!-------------------------------------------------------------------------------
! subroutine OutputAnalysis_Summary
! ステップごとの体積などを出力
!-------------------------------------------------------------------------------
subroutine OutputAnalysis_Summary
  use BESO_SUPER
  use BESO_DATA
	implicit none

	if (kstep == 1) then
    open(300, file='./dataout/overview/' // modelName // '_' // 'BESO_summary.txt', action='write')
		write(300,fmt='(5(A15,2X))') 'step', '体積', '要素数', '体積率', '削除基準値'
  else
		open(300, file='./dataout/overview/' // modelName // '_' // 'BESO_summary.txt', position='append', action='write')
	end if

	write(300, fmt='(I15, 2X, E15.7, 2X, I15, 2X, F15.7, 2X, E15.7)') kstep, step_volume, El1, dble(El1)/dble(El), Vp0
	close(300)

end subroutine OutputAnalysis_Summary

!-------------------------------------------------------------------------------
! subroutine OutputModel_info
! モデル情報のアウトプット
!-------------------------------------------------------------------------------
subroutine OutputModel_info
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: i

  open(200, file='./dataout/topology_info/' // modelName // '_' // kstepStr // '_model.dat', action='write')
  write(200,*) '# element num'
  write(200,fmt='(I10)') El
  write(200,fmt='(3(A12,2X),A15)') 'Be', 'Cremap', 'material_ID', 'SenseHistry'
  do i = 1, El
    write(200,fmt='(3(I12,2X),E15.7)') Be(i), Cre_Map(i), material_map(i), Ndsen_hist(i)
  end do
  close(200)

end subroutine OutputModel_info


!-------------------------------------------------------------------------------
! subroutine OutputBESO_Parm
! BESO法のパラメーターのアウトプット
!-------------------------------------------------------------------------------
subroutine OutputBESO_Parm
  use BESO_SUPER, modelName_in => modelName
  use BESO_DATA
  use MODEL_MOD
  use PHYSICAL_DATA
  implicit none
  integer :: int_vec(3)
  real(8) :: dble_vec(3)
  character :: strID*2
  integer :: i, j

  NAMELIST/ BESO_PARAM/ modelName_in, coeff_von, coeff_disp, coeff_buck, &
    & step_start, step_end, target_rate, e_rate, topo_rate, symType, history, &
    & r_min, filter_degree, Bsen_mixPenalty, BsenJoinNum, buckAnalyNum
  NAMELIST/ Build_PARAM/ Xn, Yn, Zn, Nx, Ny, Nz
  NAMELIST/ PhysicNum_PARAM/ mtNum
  NAMELIST/ Physic_PARAM/ young_list, poisson_list, density_list, gravityDir, grav_weight

  ! BESO parameter
  open(200, file='./dataout/topology_info/' // modelName_in // '_beso_param.dat', action='write')
  write(200, NML=BESO_PARAM)
  close(200)

  ! physical setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_physic.dat', action='write')
  write(200,NML=PhysicNum_PARAM)
  write(200,NML=Physic_PARAM)
  close(200)

  ! build area setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_build.dat', action='write')
  write(200, NML=Build_PARAM)
  close(200)

  ! load setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_load.dat', action='write')
  write(200,*) '# case num'
  write(200,fmt='(I5)') load_caseNum
  do i = 1, load_caseNum
    write(strID,fmt='(I2.2)') i
    ! 各ケースデータ長
    write(200,*) '# case' // strID // ' load listLen'
    write(200,fmt='(I5)') load_data(i)%list_len
    ! 各方向荷重データ数
    write(200,*) '# case' // strID // ' load num'
    write(200,fmt='(3(I15,2X))') load_data(i)%Num_vec(:)
    ! 各方向荷重節点ID
    write(200,*) '# case' // strID // ' load node'
    do j = 1, load_data(i)%list_len
      write(200,fmt='(3(I15,2X))') load_data(i)%ID_vec(j,:)
    end do
    ! 各方向節点荷重値
    write(200,*) '# case' // strID // ' load value'
    do j = 1, load_data(i)%list_len
      write(200,fmt='(3(E15.7,2X))') load_data(i)%Val_vec(j,:)
    end do
  end do
  close(200)

  ! support setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_support.dat', action='write')
  write(200,*) '# case num'
  write(200,fmt='(I5)') support_caseNum
  write(200,*) '# support_listLen'
  write(200,fmt='(I5)') support_listLen
  do i = 1, support_caseNum
    write(strID,fmt='(I2.2)') i
    write(200,*) '# case' // strID // ' support num'
    write(200,fmt='(3(I15,2X))') NFIX_1(:)
    write(200,*) '# case' // strID // ' support node'
    do j = 1, maxval(NFIX_1(:))
      write(200,fmt='(3(I15,2X))') NFIXP_1(j,:)
    end do
  end do
  close(200)

  ! analysis setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_analy.dat', action='write')
  write(200,*) '# analysis case num'
  write(200,fmt='(I5)') totalAnalyCase
  write(200,fmt='(3(A15,2X))') 'caseID', 'loadID', 'supportID'
  do i = 1, totalAnalyCase
    write(200,fmt='(3(I15,2X))') i, analyCase_list(i,:)
  end do
  close(200)

end subroutine OutputBESO_Parm

end module FILE_IO
