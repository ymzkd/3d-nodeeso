module INIT_BUILD_SETTING
  use BESO_DATA
  implicit none
  contains
!===============================================================================
! subroutine MeshG
! 全体領域の作成
!===============================================================================
subroutine MeshG(Node,Nx1,Ny1,Nz1)
  use BESO_DATA
  implicit none
  integer,intent(in) :: Node,Nx1,Ny1,Nz1

  integer :: npt_x, npt_y, npt_z

  integer :: i,j,k,ir,jr,kr
  integer :: Nd_N1,Nd_N2,Nd_N3,Nd_N4,Nd_N5,Nd_N6,Nd_N7,Nd_N8
  integer :: Enr,Enr1,Enr2,Enr3,Enr4
  integer :: Nx1Half, Ny1Half
  real(8) :: x1, y1, z1, x2, y4, z5

  !節点の座標を求める
  Nd_in(:,:)=0
  do i=1,Node
    ! 座標値を求め格納
    Nd_in(i,1)=((Xn-X0)/dble(Nx1))*dble((i-1)-(Nx1+1)*(Ny1+1)*((i-1)/((Nx1+1)*(Ny1+1)))-(Nx1+1)*((i-1)/(Nx1+1)-(Ny1+1)*((i-1)/((Nx1+1)*(Ny1+1)))))+X0
    Nd_in(i,2)=((Yn-Y0)/dble(Ny1))*dble((i-1)/(Nx1+1)-(Ny1+1)*((i-1)/((Nx1+1)*(Ny1+1))))+Y0
    Nd_in(i,3)=((Zn-Z0)/dble(Nz1))*dble((i-1)/((Nx1+1)*(Ny1+1)))+Z0
  end do

  !六面体要素番号と節点番号の関係付け
  !六面体要素の重心座標を求める
  Enr=0  !要素番号のカウント
  El_in(:,:)=0
  El_g(:,:) =0.0d0
  do k=1,Nz1
    do j=1,Ny1
      do i=1,Nx1
        Enr=Enr+1

        kr=(Enr-1)/(Nx1*Ny1)+1
        jr=(Enr-1)/Nx1-Ny1*(kr-1)+1
        ir=Enr-(Nx1*Ny1*(kr-1)+Nx1*(jr-1))

        Nd_N1=(Nx1+1)*(Ny1+1)*(kr-1)+(Nx1+1)*(jr-1)+ir
        Nd_N2=(Nx1+1)*(Ny1+1)*(kr-1)+(Nx1+1)*(jr-1)+ir+1
        Nd_N3=(Nx1+1)*(Ny1+1)*(kr-1)+(Nx1+1)*(jr)+ir+1
        Nd_N4=(Nx1+1)*(Ny1+1)*(kr-1)+(Nx1+1)*(jr)+ir
        Nd_N5=(Nx1+1)*(Ny1+1)*(kr)+(Nx1+1)*(jr-1)+ir
        Nd_N6=(Nx1+1)*(Ny1+1)*(kr)+(Nx1+1)*(jr-1)+ir+1
        Nd_N7=(Nx1+1)*(Ny1+1)*(kr)+(Nx1+1)*(jr)+ir+1
        Nd_N8=(Nx1+1)*(Ny1+1)*(kr)+(Nx1+1)*(jr)+ir

        El_in(Enr,1)=Nd_N1
        El_in(Enr,2)=Nd_N2
        El_in(Enr,3)=Nd_N3
        El_in(Enr,4)=Nd_N4
        El_in(Enr,5)=Nd_N5
        El_in(Enr,6)=Nd_N6
        El_in(Enr,7)=Nd_N7
        El_in(Enr,8)=Nd_N8

        x1=Nd_in(Nd_N1,1)
        y1=Nd_in(Nd_N1,2)
        z1=Nd_in(Nd_N1,3)
        x2=Nd_in(Nd_N2,1)
        y4=Nd_in(Nd_N4,2)
        z5=Nd_in(Nd_N5,3)

        El_g(Enr,1)=(x1+x2)/2.d0
        El_g(Enr,2)=(y1+y4)/2.d0
        El_g(Enr,3)=(z1+z5)/2.d0
      end do
    end do
  end do

  !対称情報の作成
  ! x-z面対称
  Enr = 1
  if (symType == 2) then
    do k = 1, Nz1
      Ny1Half = Ny1 / 2 + mod(Ny1, 2)
      do j = 1, Ny1Half
        do i = 1, Nx1
          Enr1 = Nx1 * Ny1 * (k-1) + Nx1*(j-1) + i
          ! x軸対称
          Enr2 = Nx1 * Ny1 * k - (Nx1*j - i)
          sym_node(Enr,1:2) = (/Enr1, Enr2/)
          Enr = Enr + 1
        end do
      end do
    end do
  ! z軸対称
  elseif (symType == 3) then
    do k = 1, Nz1
      Ny1Half = Ny1 / 2 + mod(Ny1, 2)
      do j = 1, Ny1Half
        Nx1Half = Nx1 / 2 + mod(Nx1, 2)
        do i = 1, Nx1Half
          Enr1 = Nx1 * Ny1 * (k-1) + Nx1*(j-1) + i
          ! 点対称
          Enr2 = Nx1 * Ny1 * k + 1 - (Nx1*(j-1) + i)
          ! x軸対称
          Enr3 = Nx1 * Ny1 * k - (Nx1*j - i)
          ! y軸対称
          Enr4 = Nx1 * Ny1 * (k-1) + Nx1*j - i + 1
          sym_node(Enr,1:4) = [Enr1, Enr2, Enr3, Enr4]
          Enr = Enr + 1
        end do
      end do
    end do
  end if

  !対称情報の作成
  npt_x = Nx1 + 1
  npt_y = Ny1 + 1
  npt_z = Nz1 + 1
  ! x-z面対称
  Enr = 1
  if (symType == 1) then
    do i = 1, Nd
      sym_node(i,1) = i
    end do
  end if

  if (symType == 2) then
    do k = 1, npt_z
      Ny1Half = npt_y / 2 + mod(npt_y, 2)
      do j = 1, Ny1Half
        do i = 1, npt_x
          Enr1 = npt_x * npt_y * (k-1) + npt_x*(j-1) + i
          ! x軸対称
          Enr2 = npt_x * npt_y * k - (npt_x*j - i)
          sym_node(Enr,1:2) = (/Enr1, Enr2/)
          Enr = Enr + 1
        end do
      end do
    end do
  ! z軸対称
  elseif (symType == 3) then
    do k = 1, npt_z
      Ny1Half = npt_y / 2 + mod(npt_y, 2)
      do j = 1, Ny1Half
        Nx1Half = npt_x / 2 + mod(npt_x, 2)
        do i = 1, Nx1Half
          Enr1 = npt_x * npt_y * (k-1) + npt_x*(j-1) + i
          ! 点対称
          Enr2 = npt_x * npt_y * k + 1 - (npt_x*(j-1) + i)
          ! x軸対称
          Enr3 = npt_x * npt_y * k - (npt_x*j - i)
          ! y軸対称
          Enr4 = npt_x * npt_y * (k-1) + npt_x*j - i + 1
          sym_node(Enr,1:4) = (/Enr1, Enr2, Enr3, Enr4/)
          Enr = Enr + 1
        end do
      end do
    end do
  end if

  sym_map(:) = 0
  do i = 1, sym_nodeNum
    sym_map(sym_node(i,1)) = 1
  end do

end subroutine MeshG

!-------------------------------------------------------------------------------
! subroutine Model_FileInput
!   モデルデータの読み込み
!-------------------------------------------------------------------------------
subroutine InitModel
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: access, file_status
  integer :: decide_form, X1s,Y1s, X1e, Y1e

  NAMELIST/ MODEL_PARAM/ decide_form, X1s,Y1s, X1e, Y1e

  file_status = access('./datain/' // modelName // '_model.dat','r')
  if (file_status==0) then
    print *, 'use file input'
    call Model_FileInput
  else
    open(200,file='./setting/beso_param.set',action='read')
    read(200,NML=MODEL_PARAM)
    close(200)
    print *, 'use default setting, decide_form:' , decide_form
    call ModelEasySetting(decide_form, X1s,Y1s, X1e, Y1e)
  end if
  Be_pre(:) = Be(:)

end subroutine InitModel

!-------------------------------------------------------------------------------
! subroutine Model_FileInput
!   モデルデータの読み込み
!-------------------------------------------------------------------------------
subroutine Model_FileInput
  use BESO_SUPER
  use BESO_DATA
  implicit none

  integer :: i

  open(200,file='./datain/' // modelName // '_model.dat', action='read')
  read(200,*)
  read(200,*)
  read(200,*)
  do i = 1, El
    read(200,*) Be(i), Cre_Map(i), material_map(i), Ndsen_hist(i)
  end do
  close(200)
end subroutine Model_FileInput

!===============================================================================
! subroutine ModelEasySetting
!   初期形状の入力(選択領域を残す)
!===============================================================================
subroutine ModelEasySetting(decide_form, X1s,Y1s, X1e, Y1e)
  use BESO_DATA
  implicit none
  integer, intent(in) :: decide_form  ! 1: 部分指定, 2: 全体をモデルに
  integer, intent(inout) :: X1s,Y1s,X1e,Y1e  !初期形状を決める始点と終点の位置

  integer :: Z1s,Z1e  !初期形状を決める始点と終点の位置
  integer :: i,j,k
  integer :: No

  Z1s = 1
  Z1e = Nz + 1

  if(decide_form == 2) then
    X1s = 1
    Y1s = 1
    X1e = Nx + 1
    Y1e = Ny + 1
  end if

  Be(:)=0

  do k = Z1s, Z1e
    do j = Y1s, Y1e
      do i = X1s, X1e
        No = (Nx+1)*(Ny+1)*(k-1)+(Nx+1)*(j-1)+i  !全体領域での要素番号。
        Be(No)=1                  !要素の有無の情報を格納。
      end do
    end do
  end do

end subroutine ModelEasySetting

!-------------------------------------------------------------------------------
! subroutine InitLoad
! 荷重情報の生成
!-------------------------------------------------------------------------------
subroutine InitLoad
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: access, file_status, hold_load_node, rigid_insrt
  integer, allocatable :: loadsrf(:), loaddir(:)
  real(8), allocatable :: loadval(:)

  integer :: i, j

  NAMELIST/  AnalyLoadBase_PARAM/ load_caseNum, hold_load_node, rigid_insrt
  NAMELIST/ AnalyLoadEasy_PARAM/ loadsrf, loaddir, loadval

  file_status = access('./datain/' // modelName // '_load.dat','r')

  ! 基本設定の読み込み
  open(13,file='./setting/beso_param.set',action='read')
  read(13,NML=AnalyLoadBase_PARAM)
  close(13)

  ! 詳細荷重設定
  if (file_status==0) then
    print *, 'use file input'
    call Load_FileInput

  ! デフォルト荷重設定
  else
    print *, 'use default setting'
    allocate(loadsrf(load_caseNum), loaddir(load_caseNum), loadval(load_caseNum))

    open(13,file='./setting/beso_param.set',action='read')
    read(13,NML=AnalyLoadEasy_PARAM)
    close(13)
    ! 分布荷重設定面(laodsrf)の選択
    ! 1 : x-z面・正方向
    ! 2 : x-z面・負方向
    ! 3 : x-y面・正方向
    ! 4 : x-y面・負方向
    ! 5 : y-z面・正方向
    ! 6 : y-z面・負方向

    ! 荷重入力方向(loaddir)の指定
    ! 1 : x方向
    ! 2 : y方向
    ! 3 : z方向
    call Load_easySetting(loadsrf(:), loaddir(:), loadval(:))

  end if

  ! 荷重要素(という属性を持つ要素のリストを生成)
  call Load_Attribution

  ! 荷重節点を含む要素を位相操作対象外に
  if (hold_load_node==1) then
    do i = 1, 3
      do j = 1, load_data(1)%Num_vec(i)
        Cre_Map(load_data(1)%ID_vec(j,i)) = 0
      end do
    end do
    ! call Lock_Load_Element
  end if

  if (rigid_insrt==1) then
    do i = 1, size(loadEl_list)
      material_map(loadEl_list(i)) = 2
    end do
    ! call Change_Material_LoadElm
  end if

end subroutine InitLoad

!-------------------------------------------------------------------------------
! subroutine Load_FileInput
! 荷重情報の入力
!-------------------------------------------------------------------------------
subroutine Load_FileInput
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: j_Num_vec(3)
  real(8) :: j_Val_vec(3)

  integer :: i, j, k

  open(13,file='./datain/' // modelName // '_load.dat', action='read')
  read(13,*)
  read(13,*) load_caseNum
  allocate(load_data(load_caseNum))
  do i = 1, load_caseNum
    ! 配列サイズの
    read(13,*)
    read(13,*) load_data(i)%list_len
    allocate(load_data(i)%ID_vec(load_data(i)%list_len,3), &
      & load_data(i)%Val_vec(load_data(i)%list_len,3))
    load_data(i)%ID_vec(:,:) = 0
    load_data(i)%Val_vec(:,:) = 0.d0

    ! 荷重節点IDの入力
    read(13,*)
    read(13,*) load_data(i)%Num_vec(1:3)
    read(13,*)
    do j = 1, load_data(i)%list_len
      read(13,*) j_Num_vec(1:3)
      do k = 1, 3
        if (j > load_data(i)%Num_vec(k)) cycle
        load_data(i)%ID_vec(j,k) = j_Num_vec(k)
      end do
    end do

    ! 荷重値の入力
    read(13,*)
    do j = 1, load_data(i)%list_len
      read(13,*) j_Val_vec(1:3)
      do k = 1, 3
        if (j > load_data(i)%Num_vec(k)) cycle
        load_data(i)%Val_vec(j,k) = j_Val_vec(k)
      end do
    end do
  end do
  close(13)
end subroutine Load_FileInput

!===============================================================================
! subroutine Load_easySetting
! 荷重情報のシンプルな設定。
!===============================================================================
subroutine Load_easySetting(loadsrf, loaddir, loadval)
  use BESO_DATA
  implicit none

  integer, intent(in) :: loadsrf(:), loaddir(:)
  real(8), intent(in) :: loadval(:)

  integer :: ikds

  integer :: i, j, k, ii, ij, ik
  real(8) :: surface

  integer :: i_eleID, i_nodeID
  integer, allocatable :: node_map(:,:,:)
  real(8), allocatable :: node_value(:,:,:)
  integer :: elsrf_ID(6,4)

  elsrf_ID(1,:) = (/4,3,7,8/)
  elsrf_ID(2,:) = (/1,5,6,2/)
  elsrf_ID(3,:) = (/6,5,8,7/)
  elsrf_ID(4,:) = (/1,2,3,4/)
  elsrf_ID(5,:) = (/2,6,7,3/)
  elsrf_ID(6,:) = (/1,4,8,5/)

  ! 荷重配列の割り付け
  allocate(node_map(load_caseNum,Nd,3));  node_map(:,:,:) = 0
  allocate(node_value(load_caseNum,Nd,3));node_value(:,:,:) = 0.d0

  allocate(load_data(load_caseNum))

  ! マッピング
  do ikds=1,load_caseNum

  do i = 1, Nx
    do j = 1, Ny
      do k = 1, Nz
        i_eleID = Nx*Ny*(k-1) + Nx*(j-1) + i
        if (loadsrf(ikds)==1) then
          surface = Hx*Hz
          ! loadsrf(ikds)=1の条件
          if (j==Ny) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        elseif (loadsrf(ikds)==2) then
          surface = Hx*Hz
          ! loadsrf(ikds)=2の条件
          if (j==1) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        elseif (loadsrf(ikds)==3) then
          surface = Hx*Hy
          ! loadsrf(ikds)=3の条件
          if (k==Nz) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        elseif (loadsrf(ikds)==4) then
          surface = Hx*Hy
          ! loadsrf(ikds)=4の条件
          if (k==1) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        elseif (loadsrf(ikds)==5) then
          surface = Hy*Hz
          ! loadsrf(ikds)=5の条件
          if (i==Nx) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        elseif (loadsrf(ikds)==6) then
          surface = Hy*Hz
          ! loadsrf(ikds)=6の条件
          if (i==1) then
            do ii = 1, 4
              i_nodeID = El_in(i_eleID,elsrf_ID(loadsrf(ikds),ii))
              node_map(ikds,i_nodeID,loaddir(ikds)) = i_nodeID
              node_value(ikds,i_nodeID,loaddir(ikds)) = node_value(ikds,i_nodeID,loaddir(ikds)) &
                & + surface / 4.d0 * loadval(ikds)
            end do
          end if
        end if
      end do
    end do
  end do
  end do

  ! マッピング結果を集計・割付
  do i = 1, load_caseNum
    load_data(i)%Num_vec(1:3) = count(node_map(i,:,1:3)/=0, dim=1)
    load_data(i)%list_len = maxval(load_data(i)%Num_vec(1:3))

    allocate(load_data(i)%ID_vec(load_data(i)%list_len,3))
    load_data(i)%ID_vec(:,:) = 0
    allocate(load_data(i)%Val_vec(load_data(i)%list_len,3))
    load_data(i)%Val_vec(:,:) = 0.d0
  end do

  ! 荷重値と荷重節点ベクトルの代入
  do i = 1, load_caseNum
    do j = 1, 3
      load_data(i)%ID_vec(1:load_data(i)%Num_vec(j),j) = pack(node_map(i,:,j), mask=(node_map(i,:,j)/=0))
      load_data(i)%Val_vec(1:load_data(i)%Num_vec(j),j) = pack(node_value(i,:,j), mask=(node_map(i,:,j)/=0))
    end do
  end do

end subroutine Load_easySetting

!-------------------------------------------------------------------------------
! subroutine Load_Attribution
! 荷重要素という属性を持つリスト、loadEl_listを作成
!-------------------------------------------------------------------------------
subroutine Load_Attribution
  use STD_CAL
  implicit none
  integer :: element_map(El)
  integer :: i, j
  element_map(:) = 0

  do i = 1, load_caseNum
    do j = 1, El
      if (m_any(El_in(j,:), &
        & pack(load_data(i)%ID_vec(:,:), load_data(i)%ID_vec(:,:)/=0))) then
        element_map(j) = j
      end if
    end do
  end do

  allocate(loadEl_list(count(element_map/=0)))
  loadEl_list(:) = pack(element_map(:), element_map(:) /= 0)

end subroutine Load_Attribution

!-------------------------------------------------------------------------------
! subroutine Lock_Load_Element
! 荷重節点を持つ要素を位相操作の対象としない。
!-------------------------------------------------------------------------------
subroutine Lock_Load_Element
  use BESO_DATA
  use STD_CAL
  implicit none
  integer :: i, j
  do i = 1, load_caseNum
    do j = 1, El
      if (m_any(El_in(j,:), &
        & reshape(load_data(i)%ID_vec(:,:), &
          & [size(load_data(i)%ID_vec(:,:))]))) then
        Cre_Map(j) = 0
      end if
    end do
  end do
end subroutine Lock_Load_Element

!-------------------------------------------------------------------------------
! subroutine Change_Material_LoadElm
! 荷重節点を持つ要素のマテリアルIDを通常の要素と変更(ID=2)
!-------------------------------------------------------------------------------
subroutine Change_Material_LoadElm
  use BESO_DATA
  use STD_CAL
  integer :: i, j
  do i = 1, load_caseNum
    do j = 1, El
      if (m_any(El_in(j,:), &
        & reshape(load_data(i)%ID_vec(:,:), &
          & [size(load_data(i)%ID_vec(:,:))]))) then
        material_map(j) = 2
      end if
    end do
  end do
end subroutine Change_Material_LoadElm

!-------------------------------------------------------------------------------
! subroutine InitSupport
! 支持情報の生成
!-------------------------------------------------------------------------------
subroutine InitSupport
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: access, file_status
  integer :: support_type

  NAMELIST/ AnalySupport_PARAM/ support_type

  file_status = access('./datain/' // modelName // '_support.dat','r')
  if (file_status==0) then
    print *, 'use file input'
    call Support_FileInput

  else
    print *, 'use default setting'
    open(13,file='./setting/beso_param.set',action='read')
    read(13,NML=AnalySupport_PARAM)
    close(13)

    ! 支持条件の情報(support_type)＝？
    ! 1＝四隅ピン支持
    ! 2＝底面ピン支持
    ! 3＝周辺ピン支持
    call Support_easySetting(support_type)

  end if

  ! 支持要素のリストを生成
  call Support_Attribution

end subroutine InitSupport

!-------------------------------------------------------------------------------
! subroutine Support_FileInput
! 支持条件のファイル入力
!-------------------------------------------------------------------------------
subroutine Support_FileInput
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: i_Num_vec(3)

  integer :: i, j

  open(13,file='./datain/' // modelName // '_support.dat', action='read')
  read(13,*)
  read(13,*)
  read(13,*)
  ! 支持条件配列の生成
  read(13,*) support_listLen
  allocate(NFIXP_1(support_listLen,3))
  NFIXP_1(:,:) = 0

  ! 各方向の支持節点数
  read(13,*)
  read(13,*) NFIX_1(1:3)
  read(13,*)

  ! 荷重節点の入力
  do i = 1, support_listLen
    read(13,*) i_Num_vec(1:3)
    do j = 1, 3
      if (i > NFIX_1(j)) cycle
      NFIXP_1(i,j) = i_Num_vec(j)
    end do
  end do

end subroutine Support_FileInput

!===============================================================================
! subroutine  Support_easySetting
! 支持情報の形成。
!===============================================================================
subroutine  Support_easySetting(support_type)
  use BESO_DATA
  implicit none

  ! 1＝四隅ピン支持
  ! 2＝底面ピン支持
  ! 3＝周辺ピン支持
  integer, intent(in) :: support_type
  integer :: i_nodeID
  integer :: i,j
  integer, allocatable :: node_map(:,:)

  support_caseNum = 1

  ! 支持節点のマッピング
  allocate(node_map(Nd,3)); node_map(:,:) = 0
  do i = 1, Nx+1
    do j = 1, Ny+1
      i_nodeID = (j-1) * (Nx+1) + i
      if (support_type==1) then
        ! support_type = 1の条件
        if (i==1    .and. j==1)    node_map(i_nodeID,:) = i_nodeID
        if (i==1    .and. j==Ny+1) node_map(i_nodeID,:) = i_nodeID
        if (i==Nx+1 .and. j==1)    node_map(i_nodeID,:) = i_nodeID
        if (i==Nx+1 .and. j==Ny+1) node_map(i_nodeID,:) = i_nodeID
      elseif (support_type==2) then
        ! support_type = 2の条件
        node_map(i_nodeID,:) = i_nodeID
      elseif (support_type==3) then
        ! support_type = 3の条件
        if (j==1)    node_map(i_nodeID,:) = i_nodeID
        if (j==Ny+1) node_map(i_nodeID,:) = i_nodeID
        if (i==1)    node_map(i_nodeID,:) = i_nodeID
        if (i==Nx+1) node_map(i_nodeID,:) = i_nodeID
      end if
    end do
  end do

  ! マッピング情報のまとめ・割付
  NFIX_1(1:3) = count(node_map(:,1:3)/=0, dim=1)
  support_listLen = maxval(NFIX_1(:))
  allocate(NFIXP_1(support_listLen,3)); NFIXP_1(:,:) = 0

  ! 支持自由度の入力
  do i = 1, 3
    NFIXP_1(1:NFIX_1(i),i) = pack(node_map(:,i), mask=(node_map(:,i)/=0))
  end do

  deallocate(node_map)

end subroutine Support_easySetting

!-------------------------------------------------------------------------------
! subroutine Support_Attribution
! 支持属性を持つ要素のリストの作成
!-------------------------------------------------------------------------------
subroutine Support_Attribution
  use STD_CAL
  implicit none
  integer :: element_map(El)
  integer :: i, j
  element_map(:) = 0

  do i = 1, load_caseNum
    do j = 1, El
      if (m_any(El_in(j,:), &
        & reshape(NFIXP_1(:,:), &
          & [size(NFIXP_1(:,:))]))) then
        element_map(j) = j
      end if
    end do
  end do

  allocate(supportEl_list(count(element_map/=0)))
  supportEl_list(:) = pack(element_map(:), element_map(:) /= 0)

end subroutine Support_Attribution

subroutine Diside_Variable
  integer :: i

  allocate(Cre_Map_local(sym_nodeNum))
  Cre_Map_local(:) = 0
  BE_Num_ope = 0
  do i = 1, sym_nodeNum
    Cre_Map_local(i) = Cre_Map(sym_node(i,1))
  end do
  Num_ope = count(Cre_Map_local(:)==1)

  allocate(Be_ope(Num_ope))
  allocate(sense_hist_ope(Num_ope))
  Be_ope(1:Num_ope) = pack(pack(Be(:), sym_map(:)==1), Cre_Map_local(:)==1)
  sense_hist_ope(:) = Be_ope(:)
  Be_Num_ope = count(Be_ope(:)==1)

  !目標要素数
  target_Nd = (sym_num*Num_ope)*target_rate
  Nd1 = count(Be(:)==1)

end subroutine Diside_Variable

end module INIT_BUILD_SETTING
