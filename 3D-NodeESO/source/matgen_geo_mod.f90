!===============================================================================
! module GeoMat_GenMOD
! 線形座屈解析向けの幾何剛性の作成
!--------------------------
! gkt                : 幾何剛性マトリクス
! sigma0_matrix(9,9) : 初期応力マトリクス
! B0matrix(9,12)     : 名前忘れた。要素幾何剛性マトリクスに必要
! geo_elmMat(12,12)         : 要素幾何剛性マトリクス
!===============================================================================
module GeoMat_GenMOD
  use MODEL_MOD
  implicit none
  real(8), private :: detJ
  real(8), private :: J_invMat(3,3)
  real(8),dimension(12,12) :: geo_elmMat

  real(8),dimension(9,12) :: B0matrix
  real(8),dimension(9,9) :: sigma0_matrix

contains

!-------------------------------------------------------------------------------
! subroutine mkCSRStiff_Mat
!	要素剛性マトリクスのCSR形式マトリクスへの重ね合わせ
!-------------------------------------------------------------------------------
subroutine mkCSRGeo_Mat(ns, nf, gk_CSR)
  use MATRIX_FORM
  implicit none
  integer, intent(in) :: ns, nf
  real(8), intent(inout) :: gk_CSR(:)
  integer :: nsf, row_head, row_tail, row_num
	integer :: ie,ip,i,nt,it,jp,j,jt,k,kk,iel,jel,m,ic
	real(8) :: s

	gk_CSR(:) = 0.0d0
  nsf = ns * nf

	do ie=1,MDL_elmNum

    ! 要素幾何剛性マトリクスの作成
    call mkGeo_elMat(ie)

    ! CSRマトリクスへの合成
    call assemCSRMat(ie, gk_CSR(:), geo_elmMat(:,:))

  end do

end subroutine mkCSRGeo_Mat

!====================================================================================================
!	subroutine mkGeo_elMat(ie)
!	要素剛性マトリクスの作成
!====================================================================================================
subroutine mkGeo_elMat(ie)
  integer,intent(in) :: ie

  real(8) :: dndxi(4), dndeta(4), dndzeta(4)

  integer :: i,j

  geo_elmMat(:,:) = 0.d0

  !	応力-ひずみマトリクス
  call make_Sigma0_matrix(ie)

  !	形状関数の微分
  call dshape_function(dndxi(:), dndeta(:), dndzeta(:))

  !	ヤコビ行列
  call make_Jmatrix(ie, dndxi(:), dndeta(:), dndzeta(:), detJ, J_invMat(:,:))

  !	ひずみ-変位マトリクス
  call make_B0matrix(dndxi(:), dndeta(:), dndzeta(:), J_invMat(:,:))

  !	ガウス積分の計算
  call gauss(1.d0/6.d0, detJ)

end subroutine mkGeo_elMat

!====================================================================================================
! subroutine make_Sigma0_matrix
!	応力-ひずみマトリクスの作成
!====================================================================================================
subroutine make_Sigma0_matrix(ie)
  integer,intent(in) :: ie
  integer :: i, j

  sigma0_matrix = 0.0d0
  do j = 1,3
    i = (j-1) * 3 + 1
    sigma0_matrix(i,i) = stress(ie, 1)
    sigma0_matrix(i+1,i+1) = stress(ie, 2)
    sigma0_matrix(i+2,i+2) = stress(ie, 3)
  end do

  sigma0_matrix(1,2) = stress(ie, 4)
  sigma0_matrix(2,1) = stress(ie, 4)
  sigma0_matrix(4,5) = stress(ie, 4)
  sigma0_matrix(5,4) = stress(ie, 4)
  sigma0_matrix(7,8) = stress(ie, 4)
  sigma0_matrix(8,7) = stress(ie, 4)

  sigma0_matrix(1,3) = stress(ie, 5)
  sigma0_matrix(3,1) = stress(ie, 5)
  sigma0_matrix(4,6) = stress(ie, 5)
  sigma0_matrix(6,4) = stress(ie, 5)
  sigma0_matrix(7,9) = stress(ie, 5)
  sigma0_matrix(9,7) = stress(ie, 5)

  sigma0_matrix(2,3) = stress(ie, 6)
  sigma0_matrix(3,2) = stress(ie, 6)
  sigma0_matrix(5,6) = stress(ie, 6)
  sigma0_matrix(6,5) = stress(ie, 6)
  sigma0_matrix(8,9) = stress(ie, 6)
  sigma0_matrix(9,8) = stress(ie, 6)
end subroutine make_Sigma0_matrix

!-------------------------------------------------------------------------------
! subroutine dshape_function
! 形状関数の微分
!-------------------------------------------------------------------------------
subroutine dshape_function(dndxi, dndeta, dndzeta)
  real(8), intent(out) :: dndxi(4), dndeta(4), dndzeta(4)

  dndxi(1) = -1.d0
  dndxi(2) =  1.d0
  dndxi(3) =  0.d0
  dndxi(4) =  0.d0

  dndeta(1) = -1.d0
  dndeta(2) =  0.d0
  dndeta(3) =  1.d0
  dndeta(4) =  0.d0

  dndzeta(1) = -1.d0
  dndzeta(2) =  0.d0
  dndzeta(3) =  0.d0
  dndzeta(4) =  1.d0

end subroutine dshape_function

!====================================================================================================
! subroutine make_B0matrix
!	ひずみ-変位マトリクスの作成
!====================================================================================================
subroutine make_B0matrix(dndxi, dndeta, dndzeta, J_invMat)
  real(8), intent(in) :: dndxi(:), dndeta(:), dndzeta(:)
  real(8), intent(in) :: J_invMat(:,:)

  integer :: i,i1,i2,i3
  real(8) :: dndx, dndy, dndz

  B0matrix(:,:) = 0.0d0
  do i = 1, 4
    i1 = 3*(i-1)+1
    i2 = 3*(i-1)+2
    i3 = 3*(i-1)+3
    dndx = J_invMat(1,1)*dndxi(i)+J_invMat(1,2)*dndeta(i)+J_invMat(1,3)*dndzeta(i)
    dndy = J_invMat(2,1)*dndxi(i)+J_invMat(2,2)*dndeta(i)+J_invMat(2,3)*dndzeta(i)
    dndz = J_invMat(3,1)*dndxi(i)+J_invMat(3,2)*dndeta(i)+J_invMat(3,3)*dndzeta(i)

    B0matrix(1,i1) = dndx
    B0matrix(2,i1) = dndy
    B0matrix(3,i1) = dndz
    B0matrix(4,i2) = dndx
    B0matrix(5,i2) = dndy
    B0matrix(6,i2) = dndz
    B0matrix(7,i3) = dndx
    B0matrix(8,i3) = dndy
    B0matrix(9,i3) = dndz

  end do

end subroutine make_B0matrix

!-------------------------------------------------------------------------------
! subroutine make_Jmatrix
! ヤコビ行列の作成
!-------------------------------------------------------------------------------
subroutine make_Jmatrix(ie, dndxi, dndeta, dndzeta, detJ, J_invMat)
  integer, intent(in) :: ie
  real(8), intent(in) :: dndxi(4), dndeta(4), dndzeta(4)

  real(8), intent(out) :: detJ
  real(8), intent(out) :: J_invMat(3,3)

  integer :: i_node
  real(8) :: J_mat(3,3)

  J_mat(:,:) = 0.d0

  J_mat(1,1) = MDL_nodePos(MDL_elm_node(ie,2),1) - MDL_nodePos(MDL_elm_node(ie,1),1)
  J_mat(2,1) = MDL_nodePos(MDL_elm_node(ie,3),1) - MDL_nodePos(MDL_elm_node(ie,1),1)
  J_mat(3,1) = MDL_nodePos(MDL_elm_node(ie,4),1) - MDL_nodePos(MDL_elm_node(ie,1),1)

  J_mat(1,2) = MDL_nodePos(MDL_elm_node(ie,2),2) - MDL_nodePos(MDL_elm_node(ie,1),2)
  J_mat(2,2) = MDL_nodePos(MDL_elm_node(ie,3),2) - MDL_nodePos(MDL_elm_node(ie,1),2)
  J_mat(3,2) = MDL_nodePos(MDL_elm_node(ie,4),2) - MDL_nodePos(MDL_elm_node(ie,1),2)

  J_mat(1,3) = MDL_nodePos(MDL_elm_node(ie,2),3) - MDL_nodePos(MDL_elm_node(ie,1),3)
  J_mat(2,3) = MDL_nodePos(MDL_elm_node(ie,3),3) - MDL_nodePos(MDL_elm_node(ie,1),3)
  J_mat(3,3) = MDL_nodePos(MDL_elm_node(ie,4),3) - MDL_nodePos(MDL_elm_node(ie,1),3)

  detJ = J_mat(1,1)*(J_mat(2,2)*J_mat(3,3)-J_mat(3,2)*J_mat(2,3))&
    & + J_mat(1,2)*(J_mat(3,1)*J_mat(2,3)-J_mat(2,1)*J_mat(3,3)) &
    & + J_mat(1,3)*(J_mat(2,1)*J_mat(3,2)-J_mat(3,1)*J_mat(2,2))

  J_invMat(1,1) = (J_mat(2,2)*J_mat(3,3)-J_mat(3,2)*J_mat(2,3))/detJ
  J_invMat(2,2) = (J_mat(1,1)*J_mat(3,3)-J_mat(1,3)*J_mat(3,1))/detJ
  J_invMat(3,3) = (J_mat(1,1)*J_mat(2,2)-J_mat(1,2)*J_mat(2,1))/detJ

  J_invMat(2,1) = (J_mat(3,1)*J_mat(2,3)-J_mat(2,1)*J_mat(3,3))/detJ
  J_invMat(1,2) = (J_mat(1,3)*J_mat(3,2)-J_mat(1,2)*J_mat(3,3))/detJ

  J_invMat(3,1) = (J_mat(2,1)*J_mat(3,2)-J_mat(3,1)*J_mat(2,2))/detJ
  J_invMat(1,3) = (J_mat(1,2)*J_mat(2,3)-J_mat(1,3)*J_mat(2,2))/detJ

  J_invMat(3,2) = (J_mat(1,2)*J_mat(3,1)-J_mat(3,2)*J_mat(1,1))/detJ
  J_invMat(2,3) = (J_mat(2,1)*J_mat(1,3)-J_mat(2,3)*J_mat(1,1))/detJ

end subroutine make_Jmatrix

!====================================================================================================
! subroutine gauss
!	ガウス積分
!====================================================================================================
subroutine gauss(w,detJ)
  real(8),intent(in) :: w,detJ
  real(8), dimension(12,9) :: btd

  !	[B]T[D]の計算
  btd = 0.0d0
  call dgemm('T','N', 12, 9, 9, 1.d0, B0matrix(1:9,1:12), 9, &
    & sigma0_matrix(1:9,1:9), 9, 0.d0, btd(1:12,1:9), 12)

  !	[B]T[D][B]の計算
  call dgemm('N', 'N', 12, 12, 9, w * detJ, btd(1:12,1:9), 12, &
    & B0matrix(1:9,1:12), 9, 1.d0, geo_elmMat(1:12,1:12), 12)

end subroutine gauss

end module GeoMat_GenMOD
