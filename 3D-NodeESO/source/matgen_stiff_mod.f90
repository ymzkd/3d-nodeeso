!===============================================================================
! module StiffMat_GenMOD
!	有限要素法マトリクスの生成
!===============================================================================
module StiffMat_GenMOD
  use MODEL_MOD
  implicit none

  real(8) :: detJ
  real(8) :: J_invMat(3,3)
  real(8) :: elm_Mat(12,12)
  real(8) :: Bmatrix(6,12)
  real(8) :: Dmatrix(6,6)
contains

!-------------------------------------------------------------------------------
! subroutine mkCSRStiff_Mat
!	要素剛性マトリクスのCSR形式マトリクスへの重ね合わせ
!-------------------------------------------------------------------------------
subroutine mkCSRStiff_Mat(gk_CSR)
  use MATRIX_FORM
  real(8), intent(inout) :: gk_CSR(:)

  integer :: ie

  integer :: i

  gk_CSR(:) = 0.0d0

  !	要素の応力-ひずみマトリクス作成
  call make_Dmatrix

  do ie=1, MDL_elmNum
    ! 要素剛性マトリクスの作成
    call mkStiff_elMat(ie)

    call assemCSRMat(ie, gk_CSR(:), elm_Mat(:,:))

  end do

  do i = 1, lc
    if (not(gk_CSR(CSR_rowIndex(i)) > 0.d0)) print *, 'minus element Gstiff', i
    if (isnan(gk_CSR(CSR_rowIndex(i)))) print *, 'NaN element Gstiff', i
    if (gk_CSR(CSR_rowIndex(i)) > huge(1.d0)) print *, 'inf element Gstiff', i
    if (gk_CSR(CSR_rowIndex(i)) < -huge(1.d0)) print *, '-inf element Gstiff', i
  end do

end subroutine mkCSRStiff_Mat

!-------------------------------------------------------------------------------
! subroutine mkStiff_elMat
!	要素剛性マトリクスの作成
!-------------------------------------------------------------------------------
subroutine mkStiff_elMat(ie)
  integer,intent(in) :: ie
  real(8) :: dndxi(4), dndeta(4), dndzeta(4)

  integer :: i, j

  elm_Mat(:,:) = 0.0d0
  !	形状関数の微分
  call dshape_function(dndxi(:), dndeta(:), dndzeta(:))
  !	ヤコビ行列
  call make_Jmatrix(ie, dndxi(:), dndeta(:), dndzeta(:), detJ, J_invMat(:,:))

  !	ひずみ-変位マトリクス
  call make_Bmatrix(dndxi(:), dndeta(:), dndzeta(:), J_invMat(:,:))

  MDL_elm_volume(ie) = detJ / 6.d0

  !	ガウス積分の計算
  call gaussStiff_el(1.d0/6.d0, detJ)

  ! do i = 1, 12
  !   if (not(elm_Mat(i,i) > 0.d0)) print *, 'minus element elmstiff', ie, i
  !   if (isnan(elm_Mat(i,i))) print *, 'NaN element elmstiff', ie, i, elm_Mat(i,i)
  !   if (elm_Mat(i,i) > huge(1.d0)) print *, 'inf element elmstiff', ie, i, elm_Mat(i,i)
  ! end do

end subroutine mkStiff_elMat

!-------------------------------------------------------------------------------
! subroutine dshape_function
! 形状関数の微分
!-------------------------------------------------------------------------------
subroutine dshape_function(dndxi, dndeta, dndzeta)
  real(8), intent(out) :: dndxi(4), dndeta(4), dndzeta(4)

  dndxi(1) = -1.d0
  dndxi(2) =  1.d0
  dndxi(3) =  0.d0
  dndxi(4) =  0.d0

  dndeta(1) = -1.d0
  dndeta(2) =  0.d0
  dndeta(3) =  1.d0
  dndeta(4) =  0.d0

  dndzeta(1) = -1.d0
  dndzeta(2) =  0.d0
  dndzeta(3) =  0.d0
  dndzeta(4) =  1.d0

end subroutine dshape_function

!-------------------------------------------------------------------------------
! subroutine make_Jmatrix
! ヤコビ行列の作成
!-------------------------------------------------------------------------------
subroutine make_Jmatrix(ie, dndxi, dndeta, dndzeta, detJ, J_invMat)
  integer, intent(in) :: ie
  real(8), intent(in) :: dndxi(4), dndeta(4), dndzeta(4)

  real(8), intent(out) :: detJ
  real(8), intent(out) :: J_invMat(3,3)

  integer :: i_node
  real(8) :: J_mat(3,3)

  J_mat(:,:) = 0.d0

  J_mat(1,1) = MDL_nodePos(MDL_elm_node(ie,2),1) - MDL_nodePos(MDL_elm_node(ie,1),1)
  J_mat(2,1) = MDL_nodePos(MDL_elm_node(ie,3),1) - MDL_nodePos(MDL_elm_node(ie,1),1)
  J_mat(3,1) = MDL_nodePos(MDL_elm_node(ie,4),1) - MDL_nodePos(MDL_elm_node(ie,1),1)

  J_mat(1,2) = MDL_nodePos(MDL_elm_node(ie,2),2) - MDL_nodePos(MDL_elm_node(ie,1),2)
  J_mat(2,2) = MDL_nodePos(MDL_elm_node(ie,3),2) - MDL_nodePos(MDL_elm_node(ie,1),2)
  J_mat(3,2) = MDL_nodePos(MDL_elm_node(ie,4),2) - MDL_nodePos(MDL_elm_node(ie,1),2)

  J_mat(1,3) = MDL_nodePos(MDL_elm_node(ie,2),3) - MDL_nodePos(MDL_elm_node(ie,1),3)
  J_mat(2,3) = MDL_nodePos(MDL_elm_node(ie,3),3) - MDL_nodePos(MDL_elm_node(ie,1),3)
  J_mat(3,3) = MDL_nodePos(MDL_elm_node(ie,4),3) - MDL_nodePos(MDL_elm_node(ie,1),3)

  detJ = J_mat(1,1)*(J_mat(2,2)*J_mat(3,3)-J_mat(3,2)*J_mat(2,3))&
    & + J_mat(1,2)*(J_mat(3,1)*J_mat(2,3)-J_mat(2,1)*J_mat(3,3)) &
    & + J_mat(1,3)*(J_mat(2,1)*J_mat(3,2)-J_mat(3,1)*J_mat(2,2))

  J_invMat(1,1) = (J_mat(2,2)*J_mat(3,3)-J_mat(3,2)*J_mat(2,3))/detJ
  J_invMat(2,2) = (J_mat(1,1)*J_mat(3,3)-J_mat(1,3)*J_mat(3,1))/detJ
  J_invMat(3,3) = (J_mat(1,1)*J_mat(2,2)-J_mat(1,2)*J_mat(2,1))/detJ

  J_invMat(2,1) = (J_mat(3,1)*J_mat(2,3)-J_mat(2,1)*J_mat(3,3))/detJ
  J_invMat(1,2) = (J_mat(1,3)*J_mat(3,2)-J_mat(1,2)*J_mat(3,3))/detJ

  J_invMat(3,1) = (J_mat(2,1)*J_mat(3,2)-J_mat(3,1)*J_mat(2,2))/detJ
  J_invMat(1,3) = (J_mat(1,2)*J_mat(2,3)-J_mat(1,3)*J_mat(2,2))/detJ

  J_invMat(3,2) = (J_mat(1,2)*J_mat(3,1)-J_mat(3,2)*J_mat(1,1))/detJ
  J_invMat(2,3) = (J_mat(2,1)*J_mat(1,3)-J_mat(2,3)*J_mat(1,1))/detJ

end subroutine make_Jmatrix

!-------------------------------------------------------------------------------
! subroutine make_Dmatrix
!	応力-ひずみマトリクスの作成
!-------------------------------------------------------------------------------
subroutine make_Dmatrix
  use PHYSICAL_DATA
  integer :: i,j,material_ID
  real(8) :: young,poisson,temp

  young   = young_list(1)
  poisson = poisson_list(1)

  temp = young / ((1.0d0+poisson)*(1.0d0-2.0d0*poisson))

  Dmatrix = 0.0d0
  do i=1,3
    Dmatrix(i,i)     = temp * (1.0d0 - poisson)
    Dmatrix(i+3,i+3) = temp * (1.0d0 - 2.0d0*poisson)/2.0d0
    do j=i+1,3
      Dmatrix(i,j) = temp * poisson
      Dmatrix(j,i) = temp * poisson
    end do
  end do

end subroutine make_Dmatrix

!-------------------------------------------------------------------------------
! subroutine make_Bmatrix
!	Bマトリクスの作成
!-------------------------------------------------------------------------------
subroutine make_Bmatrix(dndxi, dndeta, dndzeta, J_invMat)
  real(8), intent(in) :: dndxi(4), dndeta(4), dndzeta(4)
  real(8), intent(in) :: J_invMat(3,3)

  real(8) :: dndx, dndy, dndz

  integer :: i1, i2, i3
  integer :: i

  Bmatrix(:,:) = 0.d0
  do i = 1, 4
    i1 = 3*(i-1)+1
    i2 = 3*(i-1)+2
    i3 = 3*(i-1)+3
    dndx = J_invMat(1,1)*dndxi(i)+J_invMat(1,2)*dndeta(i)+J_invMat(1,3)*dndzeta(i)
    dndy = J_invMat(2,1)*dndxi(i)+J_invMat(2,2)*dndeta(i)+J_invMat(2,3)*dndzeta(i)
    dndz = J_invMat(3,1)*dndxi(i)+J_invMat(3,2)*dndeta(i)+J_invMat(3,3)*dndzeta(i)

    Bmatrix(1,i1) = dndx
    ! Bmatrix(1,i2)
    ! Bmatrix(1,i3)
    ! Bmatrix(2,i1)
    Bmatrix(2,i2) = dndy
    ! Bmatrix(2,i3)
    ! Bmatrix(3,i1)
    ! Bmatrix(3,i2)
    Bmatrix(3,i3) = dndz

    Bmatrix(4,i1) = dndy
    Bmatrix(4,i2) = dndx
    ! Bmatrix(4,i3)
    ! Bmatrix(5,i1)
    Bmatrix(5,i2) = dndz
    Bmatrix(5,i3) = dndy
    Bmatrix(6,i1) = dndz
    ! Bmatrix(6,i2)
    Bmatrix(6,i3) = dndx
  end do

end subroutine make_Bmatrix

!-------------------------------------------------------------------------------
! subroutine gaussStiff_el
!	ガウス積分
!-------------------------------------------------------------------------------
subroutine gaussStiff_el(weight, detJ)
  real(8), intent(in) :: weight, detJ
  real(8), dimension(12,6) :: btd

  !	[B]T[D]の計算
  btd = 0.d0
  call dgemm('T', 'N', 12, 6, 6, 1.d0, Bmatrix(1:6,1:12), 6, &
    & Dmatrix(1:6,1:6), 6, 0.d0, btd(1:12,1:6), 12)

  !	[B]T[D][B]の計算
  call dgemm('N', 'N', 12, 12, 6, weight * detJ, btd(1:12,1:6), 12, &
    & Bmatrix(1:6,1:12), 6, 1.d0, elm_Mat(1:12,1:12), 12)

end subroutine gaussStiff_el

end module StiffMat_GenMOD
