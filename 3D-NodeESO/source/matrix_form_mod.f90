module MATRIX_FORM
  use MODEL_MOD
  implicit none
  type list
    integer :: val
    type(list), pointer :: next
  end type list
  type ptr
    type(list), pointer :: p
  end type ptr

  integer :: lc
  integer :: CSRMat_len
  integer, allocatable :: CSR_columns(:)
  integer, allocatable :: CSR_rowIndex(:)

contains

!-------------------------------------------------------------------------------
! subroutine assemCSRMat
! CSRMat_Aに対して、el_ID番目の要素剛性マトリクスを、
! マトリクス構造に合わせて足し合わせる。
! 16/10/01 @ yamazaki
! *-input-*
! el_ID    : 要素番号
! CSRMat_A : skylineマトリクス
! el_Mat   : 要素マトリクス
! *-output-*
! CSRMat_A : CSRマトリクス
!-------------------------------------------------------------------------------
subroutine assemCSRMat(el_ID, CSRMat_A, el_Mat)
  integer, intent(in) :: el_ID
  real(8), intent(in) :: el_Mat(12,12)
  real(8), intent(inout) :: CSRMat_A(:)

  integer :: lo(12)
  integer :: row_head, row_tail, locij

  integer :: i, j

  ! el_ID要素の自由度を羅列
  call locate(el_ID, lo(:), 4, 3)

  do i = 1, 12
    if(lo(i) == 0) cycle
    row_head = CSR_rowIndex(lo(i))
    row_tail = CSR_rowIndex(lo(i)+1)-1
    do j = 1, 12
      ! 拘束節点
      if(lo(j) == 0) cycle
      ! 下三角
      if(lo(j) < lo(i)) cycle
      locij = minloc(CSR_columns(row_head:row_tail), 1, CSR_columns(row_head:row_tail)==lo(j))
      CSRMat_A(row_head+locij-1) = CSRMat_A(row_head+locij-1) + el_Mat(i,j)
    end do
  end do

end subroutine assemCSRMat

!-------------------------------------------------------------------------------
! subroutine Mk_Dof_ID
!	拘束節点自由度を取り除いた真の自由度の番号付け
! sup_id : 支持条件番号
!-------------------------------------------------------------------------------
subroutine Mk_Dof_ID(sup_id)
  integer, intent(in) :: sup_id
  integer :: nt
	integer :: i, j

	icrs(:) = 1
  ! icrsに拘束自由度をマッピング
	do i = 1, 3
		do j = 1, MDL_support(sup_id)%num(i)
			nt = 3 * (MDL_support(sup_id)%nodes(j,i) - 1) + i
			icrs(nt) = 0
		end do
	end do

  ! 拘束自由度以外に自由度番号を割り振り
	lc = 0
	do i = 1, MDL_nodeNum
		do j = 1, 3
			nt = 3 * (i - 1) + j
			if(icrs(nt) /= 0) then
				lc = lc + 1
				icrs(nt) = lc
			end if
		end do
	end do

end subroutine Mk_Dof_ID

!-------------------------------------------------------------------------------
! subroutine MkCSR_Index
! CSR formatを生成
!-------------------------------------------------------------------------------
subroutine MkCSR_Index
  integer :: lo(12)
  type(ptr), allocatable :: root(:)
  integer, allocatable :: len_list(:)
  integer :: row_head, row_tail

  integer :: i, j

  allocate(root(lc), len_list(lc))
  len_list(:) = 1
  do i = 1, lc
    allocate(root(i)%p)
    allocate(root(i)%p%next)
    root(i)%p%val = i
    nullify(root(i)%p%next%next)
  end do

  ! Map CSR format to lists
  do i = 1, MDL_elmNum
    call locate(i, lo(:), 4, 3)
    do j = 1, 12
      if (lo(j)==0) cycle
      call add_list(root(lo(j))%p, pack(lo(:), lo(:)>lo(j)), len_list(lo(j)))
    end do
  end do
  CSRMat_len = sum(len_list(:))

  ! CSR_rowIndex
  CSR_rowIndex(1) = 1
  do i = 2, lc+1
    CSR_rowIndex(i) = CSR_rowIndex(i-1) + len_list(i-1)
  end do

  ! CSR_columns
  allocate(CSR_columns(CSRMat_len))
  do i = 1, lc
    row_head = CSR_rowIndex(i)
    row_tail = CSR_rowIndex(i+1)-1
    call list_to_array(root(i)%p, CSR_columns(row_head:row_tail))
  end do

  do i = 1, lc
    call del_list(root(i)%p)
  end do
  deallocate(root)

  end subroutine MkCSR_Index

  !--- print list
  subroutine write_list(list_head)
    type(list), pointer :: list_head
    type(list), pointer :: work

    work => list_head
    do
      if(associated(work%next)) then
        print *, work%val
        work => work%next
      else
        exit
      end if
    end do
  end subroutine write_list

  !--- add array components to list
  subroutine add_list(list_head, add_array, len)
    type(list), pointer :: list_head
    integer, intent(in) :: add_array(:)
    integer, intent(inout) :: len
    type(list), pointer :: work, temp

    integer :: i
    do i = 1, size(add_array(:))
      work => list_head
      do
        if (add_array(i) <= work%val) exit
        if (associated(work%next%next)) then
          if (add_array(i) < work%next%val) then
            allocate(temp)
            temp%next => work%next
            temp%val = add_array(i)
            work%next => temp
            len = len + 1
            exit
          else
            work => work%next
            cycle
          end if
        else
          allocate(temp)
          temp%next => work%next
          temp%val = add_array(i)
          work%next => temp
          len = len + 1
          exit
        end if
      end do
    end do
  end subroutine add_list

  !--- copy list to array
  subroutine list_to_array(list_head, array)
    type(list), pointer :: list_head
    integer, intent(out) :: array(:)

    type(list), pointer :: work
    integer :: ind
    work => list_head
    ind = 0
    do
      if(associated(work%next)) then
        ind = ind + 1
        array(ind) = work%val
        work => work%next
      else
        exit
      end if
    end do

  end subroutine list_to_array

  !--- deleate list
  subroutine del_list(list_head)
    type(list), pointer :: list_head
    type(list), pointer :: work, next_work

    work => list_head
    do
      if(associated(work%next)) then
        next_work => work%next
        deallocate(work)
        work => next_work
      else
        exit
      end if
    end do
  end subroutine del_list

!-------------------------------------------------------------------------------
! subroutine MakeCSR_RowSearch_Array
! CSRの一行の要素数を把握する。
!-------------------------------------------------------------------------------
subroutine MakeCSR_RowSearch_Array
  use STD_CAL

  integer :: stack_num, i_DOF_index(3), lo(12)
  integer, allocatable :: search_stack(:)
  integer, allocatable :: ind_row(:)

  integer, allocatable :: node_share_element(:,:)
  integer, allocatable :: node_share_elmNum(:)

  integer :: i, j, k

  allocate(ind_row(lc))

  ! max_elments_num_node: 節点が属する要素の数の最大値
  allocate(search_stack(4*max_elments_num_node*3))
  open(10, status='scratch', form='unformatted')

  allocate(node_share_element(MDL_nodeNum, max_elments_num_node), node_share_elmNum(MDL_nodeNum))
  node_share_element(:,:) = 0
  node_share_elmNum(:) = 0

  do i = 1, MDL_elmNum
    do j = 1, 4
      node_share_elmNum(MDL_elm_node(i,j)) = node_share_elmNum(MDL_elm_node(i,j)) + 1
      node_share_element(MDL_elm_node(i,j),node_share_elmNum(MDL_elm_node(i,j))) = i
    end do
  end do

  do i = 1, MDL_nodeNum
    do j = 1, 3
      i_DOF_index(j) = icrs((i-1)*3+j)
    end do
    ! この節点の自由度がすべて真の自由度になかったら探索する意味が無いのでスキップ
    if (all(i_DOF_index(:)==0)) cycle

    ! 各節点が各要素に含まれるかをチェック
    stack_num = 0
    do j = 1, node_share_elmNum(i)

      ! j要素はi節点を含む→j要素の自由度をリストに追加
      if (all(MDL_elm_node(node_share_element(i,j),:) /= i)) cycle
      call locate(node_share_element(i,j),lo(:),4,3)
      do k = 1, 12
        if (lo(k)==0) cycle
        if (any(lo(k)==search_stack(1:stack_num))) cycle

        stack_num = stack_num + 1
        search_stack(stack_num) = lo(k)
      end do
    end do

    do j = 1, 3
      if (i_DOF_index(j)==0) cycle
      ! 上三角のみ採用(行自由度より大きい自由度の幅を確保)
      ind_row(i_DOF_index(j)) = count(search_stack(1:stack_num)>=i_DOF_index(j))

      write(10) iqsort(pack(search_stack(1:stack_num), &
        & search_stack(1:stack_num)>=i_DOF_index(j)))
    end do

  end do

  do i = 2, lc
    ind_row(i) = ind_row(i-1) + ind_row(i)
  end do
  rewind(10)
  allocate(CSR_columns(ind_row(lc)))

  read(10) CSR_columns(1:ind_row(1))
  do i = 2, lc
    read(10) CSR_columns(ind_row(i-1)+1:ind_row(i))
  end do
  close(10)

  CSR_rowIndex(1) = 1
  CSR_rowIndex(2:lc+1) = ind_row(1:lc) + 1
  CSRMat_len = CSR_rowIndex(lc+1) - 1

  deallocate(ind_row)

end subroutine MakeCSR_RowSearch_Array

!-------------------------------------------------------------------------------
! subroutine locate
! 要素剛性マトリクスに対応する自由度をまとめる。>> lo(ns*nf)
!-------------------------------------------------------------------------------
subroutine locate(ne,lo,ns,nf)
	integer, intent(in) :: ne,ns,nf
	integer, intent(out) :: lo(ns*nf)
	integer :: i,ib,n,k
  integer :: i_Node

  ! 総自由度
	do i=1,ns
		ib = nf * (i-1)
		i_Node = MDL_elm_node(ne,i)
		n = nf * (i_Node-1)
		do k=1,nf
			lo(ib+k) = n + k
		end do
	end do

  ! 真の自由度
  do i = 1, ns*nf
    lo(i) = icrs(lo(i))
  end do

end subroutine locate

end module MATRIX_FORM
