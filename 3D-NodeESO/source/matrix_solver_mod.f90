INCLUDE 'mkl_pardiso.f90'
!===============================================================================
! module MATRIX_SOLVER
! sparse_matrix_operation_routines
! ソルバーモジュール
! 16/09/29 @ yamazaki module化・solverをまとめる。
!===============================================================================
module MATRIX_SOLVER
  USE MKL_PARDISO
  implicit none
  ! sparce BLAS用のインデックス配列
  integer, allocatable, dimension(:) :: ind_blas

contains

!----------------------------------------------------------------------
! subroutine pardiso_solver(lc, nnz, CSR_rowIndex, CSR_columns, CSRMat_A, Vec_b, Vec_x)
! CSRマトリクス向けの線形ソルバー
! *-input-*
! lc           : マトリクス次数(真の自由度数)
! nnz          : マトリクス成分数
! CSR_rowIndex : CSR_rowIndex
! CSR_columns  : CSR_columns
! CSRMat_A     : マトリクス非ゼロ成分
! Vec_b        : 右辺
!
! *-output-*
! Vec_x        : 解ベクトル
!---------------------------------------------------------------------
subroutine pardiso_solver(lc, nnz, CSR_rowIndex, CSR_columns, CSRMat_A, Vec_b, Vec_x)
  implicit none

  integer, intent(in) :: lc, nnz
  integer, intent(in) :: CSR_rowIndex(:)
  integer, intent(in) :: CSR_columns(:)
  REAL(8), intent(in) :: CSRMat_A(:)

  REAL(8), intent(inout) :: Vec_b(:)
  REAL(8), intent(out)   :: Vec_x(:)

  integer, parameter :: dp = KIND(1.0D0)
  !.. Internal solver memory pointer
  TYPE(MKL_PARDISO_HANDLE), allocatable :: pt(:)
  !.. All other variables
  integer maxfct, mnum, mtype, phase, nrhs, error, msglvl

  integer :: error1
  integer, allocatable :: iparm( : )

  integer :: i, idum(1)
  REAL(KIND=DP) :: ddum(1)

  !.. Fill all arrays containing matrix data.
  nrhs = 1
  maxfct = 1
  mnum = 1

  !.. Set up PARDISO control parameter
  allocate(iparm(64))

  do i = 1, 64
    iparm(i) = 0
  end do

  iparm(1) = 1 ! no solver default
  iparm(2) = 2 ! fill-in reordering from METIS
  iparm(4) = 0 ! no iterative-direct algorithm
  iparm(5) = 0 ! no user fill-in reducing permutation
  iparm(6) = 0 ! =0 solution on the first lc components of Vec_x
  iparm(8) = 0 ! numbers of iterative refinement steps
  iparm(10) = 0 ! perturb the pivot elements with 1E-13
  iparm(11) = 0 ! use nonsymmetric permutation and scaling MPS(@yamazaki スケーリングしない)
  iparm(13) = 0 ! maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm(13) = 1 in case of inappropriate accuracy
  iparm(14) = 0 ! Output: number of perturbed pivots
  iparm(18) = -1 ! Output: number of nonzeros in the factor LU
  iparm(19) = 0 ! Output: Mflops for LU factorization(@yamazaki アウトプットしない)
  iparm(20) = 0 ! Output: Numbers of CG Iterations
  ! iparm(24) = 1 ! Parallel factorization(@yamazaki テスト的に1にしてみる。many coreの場合有用らしい)

  error  = 0 ! initialize error flag
  msglvl = 0 ! print statistical information ここでソルバーからのメッセージを表示するか決定
  mtype  = 2 ! symmetric, indefinite

  !.. Initialize the internal solver memory pointer. This is only
  ! necessary for the FIRST call of the PARDISO solver.
  allocate(pt(64))
  do i = 1, 64
    pt(i)%DUMMY =  0
  end do

  call pardisoinit(pt, mtype, iparm(:))
  open(210, file='param_pardiso.txt', action='write')
  do i = 1, 64
    write(210,*) i, iparm(i)
  end do
  close(210)

  !.. Reordering and Symbolic Factorization, This step also allocates
  ! all memory that is necessary for the factorization
  phase = 11 ! only reordering and symbolic factorization
  call pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_A, CSR_rowIndex, &
    & CSR_columns, idum, nrhs, iparm, msglvl, ddum, ddum, error)
  ! WRITE(*,*) 'Reordering completed ... '
  if (error /= 0) then
    write(*,*) 'error in static analysis solver at phase', phase, 'ErrCODE:', error
    goto 1000
  end if
  ! WRITE(*,*) 'Number of nonzeros in factors = ',iparm(18)
  ! WRITE(*,*) 'Number of factorization MFLOPS = ',iparm(19)

  !.. Factorization.
  phase = 22 ! only factorization
  call pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_A, CSR_rowIndex, &
    & CSR_columns, idum, nrhs, iparm, msglvl, ddum, ddum, error)
  ! WRITE(*,*) 'Factorization completed ... '
  if (error /= 0) then
    write(*,*) 'error in static analysis solver at phase', phase, 'ErrCODE:', error
    goto 1000
  end if

  !.. Back substitution and iterative refinement
  iparm(8) = 2 ! max numbers of iterative refinement steps
  phase = 33 ! only solving
  call pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_A, CSR_rowIndex, &
    & CSR_columns, idum, nrhs, iparm, msglvl, Vec_b, Vec_x, error)
  ! WRITE(*,*) 'Solve completed ... '
  if (error /= 0) then
    write(*,*) 'error in static analysis solver at phase', phase, 'ErrCODE:', error
    goto 1000
  end if

  1000 continue
  !.. Termination and release of memory
  phase = -1 ! release internal memory
  call pardiso (pt, maxfct, mnum, mtype, phase, lc, ddum, idum, idum, &
    & idum, nrhs, iparm, msglvl, ddum, ddum, error1)

  if (allocated(iparm)) deallocate(iparm)

  if (error1 /= 0) then
    write(*,*) 'error in static analysis solver at phase', phase, 'ErrCODE:', error
    return
  end if

  if (error /= 0) return

end subroutine pardiso_solver

!=========================================================================
! subroutine skysolver
! skylineマトリクス向けの線形ソルバー
!                      coded by hamada 2005.10
! edit : 16/10/01 @ yamazaki
!=========================================================================
subroutine skysolver(skyMat_A, Vec_b, ind, nd)
  real(8), intent(inout) :: skyMat_A(:),Vec_b(:)
  integer, intent(in) :: ind(:)
  integer, intent(in):: nd
  real(8) :: sum
  integer :: k,kwidth,kfirst
  integer :: i,iwidth,ifirst
  integer :: j,jwidth,jfirst
  integer :: ki,il,kl,ij,ji
  integer :: l,low,iback

!----- [L][D][L]^T --------------------
  do k=2,nd
  kwidth = ind(k) - ind(k-1)
  kfirst = k - kwidth + 1
  do i=kfirst,k
  ki = ind(k) - k + i
  if(i /= 1) then
  iwidth = ind(i) - ind(i-1)
  ifirst = i - iwidth + 1
  low = max0(kfirst,ifirst)
  do l=low,i-1
  il = ind(i) - i + l
  kl = ind(k) - k + l
  skyMat_A(ki) = skyMat_A(ki) - skyMat_A(il) * skyMat_A(kl) * skyMat_A(ind(l))
  end do
  end if
  if(skyMat_A(ind(i)) <= 0.0d0) then
        stop "skysolver error マトリクスが不正定です"
  else
  if(k /= i) then
  skyMat_A(ki) = skyMat_A(ki) / skyMat_A(ind(i))
  end if
  end if
  end do
  end do
!----- forward insert --------------------
  do i=2,nd
  iwidth = ind(i) - ind(i-1)
  ifirst = i - iwidth + 1
  do j=ifirst,i-1
  ij = ind(i) - i + j
  Vec_b(i) = Vec_b(i) - skyMat_A(ij) * Vec_b(j)
  end do
  end do
!----- back insert --------------------
  Vec_b(nd) = Vec_b(nd) / skyMat_A(ind(nd))
  do iback=2,nd
  i = nd - iback + 1
  sum = 0.0d0
  do j=i+1,nd
  jwidth = ind(j) - ind(j-1)
  jfirst = j - jwidth + 1
  if(jfirst .le. i) then
  ji = ind(j) - j + i
  sum = sum + skyMat_A(ji) * Vec_b(j)
  end if
  end do
  Vec_b(i) = Vec_b(i) / skyMat_A(ind(i)) - sum
  end do

end subroutine skysolver

!=====================================================================
  subroutine eigen_math_solver(nd, ind, skyMat_A, skyMat_B, &
    & eigen_number, eps, eigen_value, eigen_vector)
  ! skyMat_B : LLt分解する。
!=====================================================================
implicit none
  integer, dimension(:), intent(in) :: ind
  real(8), dimension(:), intent(in) :: skyMat_A, skyMat_B
  integer, intent(in) :: nd, eigen_number
  real(8), intent(in) :: eps
  real(8), dimension(:), intent(out) :: eigen_value
  real(8), dimension(:,:), intent(out) :: eigen_vector

  real(8), dimension(nd) :: alpha, beta, tau, temp_eigen_value
  real(8), dimension(nd, nd) :: temp_eigen_vector
  real(8), allocatable, dimension(:) :: triMat_A, triMat_B

  integer :: i, j
  integer :: find_eigen_number, info

  ! 非参照_引数
  integer :: nsplit
  real(8) :: vl, vu
  real(8), dimension(nd*5) :: work
  integer, dimension(nd*4) :: iwork, iblock, isplit
  integer, dimension(eigen_number) :: ifailv

  integer :: whole_elnum, iRow_top, iRow_low, iRow_width

  ! 上三角行列の大きさを求める
  whole_elnum = 0
  do i = 1, nd
    whole_elnum = whole_elnum + i
  end do

  allocate(triMat_A(whole_elnum), triMat_B(whole_elnum))

  triMat_A = 0.d0
  triMat_B = 0.d0
  ! スカイライン行列を上三角行列に変換
  triMat_A(1) = skyMat_A(1)
  triMat_B(1) = skyMat_B(1)
  ! 初期値
  iRow_low = 1

  do i = 2, nd
    iRow_top = iRow_low + 1
    iRow_low = iRow_low + i
    iRow_width = ind(i) - ind(i-1)
    triMat_A((iRow_low - iRow_width + 1) : iRow_low) = &
     &skymat_A(ind(i-1) + 1: ind(i))
    triMat_B((iRow_low - iRow_width + 1) : iRow_low) = &
     &skymat_B(ind(i-1) + 1: ind(i))
  end do

  ! LLt分解
  call dpptrf('U', nd, triMat_B, info)
  print *, 'info_dpptrf : ', info
  ! 一般問題の標準化
  call dspgst(1, 'U', nd, triMat_A, triMat_B, info)
  print *, 'info_dspgst : ', info

  ! 三重対角化
  call dsptrd('U', nd, triMat_A, alpha, beta, tau, info)
  print *, 'info_dsptrd : ', info

  ! 二分法によって解く
  call dstebz('I', 'E', nd, vl, vu, 1, eigen_number, eps, alpha, beta,&
              & find_eigen_number, nsplit, temp_eigen_value, iblock, isplit, work, iwork, info)
  print *, 'info_dstebz : ', info

  ! 固有ベクトルを求める。
  iblock(:) = 1
  isplit(1) = nd
  ! 三重対角行列での固有ベクトル
  call dstein(nd, alpha, beta(1:nd-1), find_eigen_number, temp_eigen_value,&
            &iblock, isplit, temp_eigen_vector, nd, work, iwork, ifailv, info)
  print *, 'info_dstein : ', info

  ! 標準固有値問題での固有ベクトル
  call dopmtr('L', 'U', 'N', nd, find_eigen_number, triMat_A, tau, temp_eigen_vector(:,:), nd, work, info)
  print *, 'info_dopmtr : ', info

  ! 一般固有値問題での固有ベクトル
  call dtptrs('U', 'N', 'N', nd, find_eigen_number, triMat_B(:), temp_eigen_vector(:,1:find_eigen_number), nd, info)
  print *, 'info_dtptrs : ', info

  eigen_value = temp_eigen_value(1:eigen_number)
  eigen_vector = temp_eigen_vector(:,1:eigen_number)

end subroutine eigen_math_solver

!-------------------------------------------------------------------------------
! subroutine dsdrv5
! ARPACKドライブ5のskylineマトリクス向けルーチン

!\References:
!  1. R.G. Grimes, J.G. Lewis and H.D. Simon, "A Shifted Block Lanczos
!     Algorithm for Solving Sparse Symmetric Generalized Eigenproblems",
!     SIAM J. Matr. Anal. Apps.,  January (1993).

!\Routines called:
!     dsaupd  ARPACK reverse communication interface routine.
!     dseupd  ARPACK routine that returns Ritz values and (optionally)
!             Ritz vectors.
!     dgttrf  LAPACK tridiagonal factorization routine.
!     dgttrs  LAPACK tridiagonal solve routine.
!     daxpy   Level 1 BLAS that computes y <- alpha*x+y.
!     dcopy   Level 1 BLAS that copies one vector to another.
!     dscal   Level 1 BLAS that scales a vector by a scalar.
!     dnrm2   Level 1 BLAS that computes the norm of a vector.
!     av      Matrix vector multiplication routine that computes A*x.
!     mv      Matrix vector multiplication routine that computes M*x.
!----------------------------------------------------------------------
subroutine dsdrv5(n, ind, ind_blas, skyMat_A, skyMat_B, nev, eigenvalue, eigenvector)
  implicit none
  integer, intent(in) :: n, nev
  integer, dimension(:), intent(in) :: ind, ind_blas
  real(8), dimension(:), intent(in) :: skyMat_A, skyMat_B

  real(8), dimension(:), intent(out) :: eigenvalue
  real(8), dimension(:,:), intent(out) :: eigenvector

  ! Define leading dimensions for all arrays.
  integer :: ldv

  ! Local Arrays
  real(8), allocatable, dimension(:) :: skyMat_LDLt

  real(8), allocatable, dimension(:,:) :: v
  real(8), allocatable, dimension(:) :: workl
  real(8), allocatable, dimension(:) :: workd
  real(8), allocatable, dimension(:,:) :: d
  real(8), allocatable, dimension(:) :: resid

  logical, allocatable, dimension(:) :: select
  integer, dimension(11) :: iparam, ipntr
  integer, allocatable, dimension(:) :: ipiv

  ! Local Scalars
  character bmat*1, which*2
  integer :: ido, ncv, lworkl, info, j, ierr, &
           & nconv, maxitr, ishfts, mode
  logical :: rvec
  real(8) :: sigma, tol

  ! real(8) :: dnrm2

  ! iterator
  integer :: count_i
  integer :: i
  real(8) :: delta

  ncv = 30
  ldv = n

  ! Louis Komzsikの方法
  sigma = 0.d0
  delta = 0.d0
  do i = 1, n
    delta = abs(skyMat_B(ind(i)) / skyMat_A(ind(i)))
    if (delta > 1.d4) then
  delta = 1.d4
  end if

  sigma = sigma + delta
  end do
  sigma = -1.d0 / (sigma)

  ! 最大最小の理論
  ! count_i = 2
  ! sigma = skyMat_A(1)**2 / skyMat_B(1)**2
  ! do i = 2, ind(n)
  ! 	if (i == ind(count_i)) then
  ! 		sigma = sigma + skyMat_B(i)**2 / skyMat_A(i)**2
  ! 		count_i = count_i + 1
  ! 	else
  ! 		sigma = sigma + skyMat_B(i)**2 / skyMat_A(i)**2 * 2.d0
  ! 	end if
  ! end do
  ! sigma = -1.d0 * sqrt(sigma / n)

  allocate(skymat_LDLt(size(skyMat_A)))
  allocate(v(n,ncv))
  allocate(workl(ncv*(ncv+8)))
  allocate(workd(3*n))
  allocate(d(ncv,2))
  allocate(resid(n))

  allocate(select(ncv))
  allocate(ipiv(n))

  bmat = 'G'
  which = 'SM'
  d(:,:) = 0.d0

  lworkl = ncv*(ncv+8)

  ! If TOL<=0, machine precision is used.
  tol = 1.d-15

  ! The variable IDO is used for reverse communication,
  !  and is initially set to 0.
  ido = 0

  ! Setting INFO=0 indicates that a random vector is
  ! generated in DSAUPD to start the Arnoldi iteration.
  info = 0

  ishfts = 1
  maxitr = 300
  mode   = 4
  iparam(1) = ishfts
  iparam(3) = maxitr
  iparam(7) = mode

  skymat_LDLt(:) = skyMat_A(:) - sigma * skyMat_B(:)
  call LDLtdecomp(n, ind, skymat_LDLt(:))

!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!       M A I N   L O O P (Reverse communication)
!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
10   continue

  call dsaupd ( ido, bmat, n, which, nev, tol, resid, &
  & ncv, v, n, iparam, ipntr, workd, workl, lworkl, &
  & info )

  if (ido .eq. -1) then

    ! Perform y <--- OP*x = inv[K-SIGMA*KG]*K*x
    call mkl_dskymv('N', n, n, 1.d0, 'sun', skymat_A, ind_blas, &
      & workd(ipntr(1):(ipntr(1)+n-1)), 0.0d0, workd(ipntr(2):(ipntr(2)+n-1)))
    call insert(skymat_LDLt(:), workd(ipntr(2):(ipntr(2)+n-1)), ind, n)

    ! roop back
    go to 10

  else if (ido .eq. 1) then

    call insert(skymat_LDLt(:), workd(ipntr(3):(ipntr(3)+n-1)), ind, n)
    workd(ipntr(2):(ipntr(2)+n-1)) = workd(ipntr(3):(ipntr(3)+n-1))

    ! roop back
    go to 10

  else if (ido .eq. 2) then
    ! Perform  y <--- K*x
    call mkl_dskymv('N', n, n, 1.d0, 'sun', skymat_A, ind_blas, &
      & workd(ipntr(1):(ipntr(1)+n-1)), 0.0d0, workd(ipntr(2):(ipntr(2)+n-1)))

    ! roop back
    go to 10

  end if

  ! DSAUPD error
  if ( info .lt. 0 ) then
    print *, ' '
    print *, ' Error with _saupd, info = ',info
    print *, ' Check the documentation of _saupd '
    print *, ' '
  else
  ! Post-Process using DSEUPD.

    ! Need eigen vector
    rvec = .true.

    call dseupd ( rvec, 'All', select, eigenvalue, eigenvector, ldv, sigma, &
  & bmat, n, which, nev, tol, resid, ncv, v, ldv, &
  & iparam, ipntr, workd, workl, lworkl, ierr )

    ! DSEUPD error
    if (ierr .ne. 0) then
      print *, ' '
      print *, ' Error with _seupd, info = ',ierr
      print *, ' Check the documentation of _seupd '
      print *, ' '
    else
      ! nconv =  iparam(5)
      ! do i = 1, nconv
      !   print *, eigenvalue(i)
      ! end do
    end if

  !        %------------------------------------------%
  !        | Print additional convergence information |
  !        %------------------------------------------%
    if ( info .eq. 1) then
      print *, ' '
      print *, ' Maximum number of iterations reached.'
      print *, ' '
    else if ( info .eq. 3) then
      print *, ' '
      print *, ' No shifts could be applied during implicit', &
      &               ' Arnoldi update, try increasing NCV.'
      print *, ' '
    end if

  end if

9000 continue

end subroutine dsdrv5

!-------------------------------------------------------------------------------
! subroutine dsdrv5_CSR
! ARPACKドライブ5のCSRマトリクス向けルーチン

!\References:
!  1. R.G. Grimes, J.G. Lewis and H.D. Simon, "A Shifted Block Lanczos
!     Algorithm for Solving Sparse Symmetric Generalized Eigenproblems",
!     SIAM J. Matr. Anal. Apps.,  January (1993).

!\Routines called:
!     dsaupd  ARPACK reverse communication interface routine.
!     dseupd  ARPACK routine that returns Ritz values and (optionally)
!             Ritz vectors.
!     dgttrf  LAPACK tridiagonal factorization routine.
!     dgttrs  LAPACK tridiagonal solve routine.
!     daxpy   Level 1 BLAS that computes y <- alpha*x+y.
!     dcopy   Level 1 BLAS that copies one vector to another.
!     dscal   Level 1 BLAS that scales a vector by a scalar.
!     dnrm2   Level 1 BLAS that computes the norm of a vector.
!     av      Matrix vector multiplication routine that computes A*x.
!     mv      Matrix vector multiplication routine that computes M*x.
!----------------------------------------------------------------------
subroutine dsdrv5_CSR(lc, nnz, CSR_rowIndex, CSR_columns, CSRMat_A, CSRMat_B, nev, eigenvalue, eigenvector)
  implicit none

  integer, intent(in) :: lc, nnz, nev
  integer, intent(in) :: CSR_rowIndex(:), CSR_columns(:)
  real(8), intent(in) :: CSRMat_A(:)
  real(8), intent(in) :: CSRMat_B(:)

  real(8), intent(out) :: eigenvalue(:)
  real(8), intent(out) :: eigenvector(:,:)

  ! Define leading dimensions for all arrays.
  integer :: ldv

  ! Local Arrays
  real(8), allocatable, dimension(:) :: CSRMat_TMP

  real(8), allocatable, dimension(:,:) :: v
  real(8), allocatable, dimension(:) :: workl
  real(8), allocatable, dimension(:) :: workd
  real(8), allocatable, dimension(:,:) :: d
  real(8), allocatable, dimension(:) :: resid

  logical, allocatable, dimension(:) :: select
  integer, dimension(11) :: iparam, ipntr
  integer, allocatable, dimension(:) :: ipiv

  ! Local Scalars
  character bmat*1, which*2
  integer :: ido, ncv, lworkl, info, j, ierr, &
           & nconv, maxitr, ishfts, mode
  logical :: rvec
  real(8) :: sigma, tol

  ! real(8) :: dnrm2

  ! iterator
  integer :: count_i
  integer :: i
  real(8) :: delta

!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
! Pardiso_Solver_Variables-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  integer, parameter :: dp = KIND(1.0D0)
  !.. Internal solver memory pointer
  type(MKL_PARDISO_HANDLE), allocatable :: pt(:)
  !.. All other variables
  integer :: maxfct, mnum, mtype, phase, nrhs, error, msglvl

  integer :: error1
  integer, allocatable :: iparm_PAR(:)

  integer :: idum(1)
  real(KIND=DP) ddum(1)
  !.. Fill all arrays containing matrix data.

  nrhs = 1
  maxfct = 1
  mnum = 1

  !.. Set up PARDISO control parameter
  ALLOCATE(iparm_PAR(64))

  DO i = 1, 64
     iparm_PAR(i) = 0
  END DO

  iparm_PAR(1) = 1 ! no solver default
  iparm_PAR(2) = 2 ! fill-in reordering from METIS
  iparm_PAR(4) = 0 ! no iterative-direct algorithm
  iparm_PAR(5) = 0 ! no user fill-in reducing permutation
  iparm_PAR(6) = 0 ! =0 solution on the first lc components of Vec_x
  iparm_PAR(8) = 0 ! numbers of iterative refinement steps
  iparm_PAR(10) = 13 ! perturb the pivot elements with 1E-13
  iparm_PAR(11) = 0 ! use nonsymmetric permutation and scaling MPS(@yamazaki スケーリングしない)
  iparm_PAR(13) = 0 ! maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm_PAR(13) = 1 in case of inappropriate accuracy
  iparm_PAR(14) = 0 ! Output: number of perturbed pivots
  iparm_PAR(18) = -1 ! Output: number of nonzeros in the factor LU
  iparm_PAR(19) = 0 ! Output: Mflops for LU factorization(@yamazaki アウトプットしない)
  iparm_PAR(20) = 0 ! Output: Numbers of CG Iterations
  ! iparm_PAR(24) = 1 ! Parallel factorization(@yamazaki テスト的に1にしてみる。many coreの場合有用らしい)

  error  = 0 ! initialize error flag
  msglvl = 0 ! print statistical information ここでソルバーからのメッセージを表示するか決定
  mtype  = 2 ! symmetric, indefinite

  !.. Initialize the internal solver memory pointer. This is only
  ! necessary for the FIRST call of the PARDISO solver.
  ALLOCATE (pt(64))
  DO i = 1, 64
     pt(i)%DUMMY =  0
  END DO

! Pardiso_Solver_Variables-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  ncv = 30
  ldv = lc

  ! Louis Komzsikの方法
  sigma = 0.d0
  delta = 0.d0
  do i = 1, lc
    delta = abs(CSRMat_B(CSR_rowIndex(i)) / CSRMat_A(CSR_rowIndex(i)))
    if (delta > 1.d4) then
  delta = 1.d4
  end if

  sigma = sigma + delta
  end do
  sigma = -1.d0 / (sigma)

  allocate(CSRMat_TMP(nnz))
  allocate(v(lc,ncv))
  allocate(workl(ncv*(ncv+8)))
  allocate(workd(3*lc))
  allocate(d(ncv,2))
  allocate(resid(lc))

  allocate(select(ncv))
  allocate(ipiv(lc))

  bmat = 'G'
  which = 'SM'
  d(:,:) = 0.d0

  lworkl = ncv*(ncv+8)

  ! If TOL<=0, machine precision is used.
  tol = 1.d-15

  ! The variable IDO is used for reverse communication,
  !  and is initially set to 0.
  ido = 0

  ! Setting INFO=0 indicates that a random vector is
  ! generated in DSAUPD to start the Arnoldi iteration.
  info = 0

  ishfts = 1
  maxitr = 300
  mode   = 4

  iparam(1) = ishfts
  iparam(3) = maxitr
  iparam(7) = mode

!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
! Pardiso-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  CSRMat_TMP(:) = CSRMat_A(:) - sigma * CSRMat_B(:)

  do i = 1, lc
    if (not(CSRMat_TMP(CSR_rowIndex(i)) > 0.d0)) print *, 'minus element buck', i
    if (isnan(CSRMat_TMP(CSR_rowIndex(i)))) print *, 'NaN element buck', i
    if (CSRMat_TMP(CSR_rowIndex(i)) > huge(1.d0)) print *, 'inf element buck', i
    if (CSRMat_TMP(CSR_rowIndex(i)) < -huge(1.d0)) print *, '-inf element buck', i
  end do

  phase = 11 ! only reordering and symbolic factorization
  CALL pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_TMP, CSR_rowIndex, CSR_columns, &
                idum, nrhs, iparm_PAR, msglvl, ddum, ddum, error)
  ! WRITE(*,*) 'Reordering completed ... '
  IF (error /= 0) THEN
    write(*,*) 'error in linear buckling analysis solver at phase', phase, 'ErrCODE:', error
    GOTO 9000
  END IF

  !.. Factorization.
  phase = 22 ! only factorization
  CALL pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_TMP, CSR_rowIndex, CSR_columns, &
                idum, nrhs, iparm_PAR, msglvl, ddum, ddum, error)
  ! WRITE(*,*) 'Factorization completed ... '
  IF (error /= 0) THEN
    write(*,*) 'error in linear buckling analysis solver at phase', phase, 'ErrCODE:', error
    GOTO 9000
  END IF

! Pardiso-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!       M A I N   L O O P (Reverse communication)
!-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
10   continue

  call dsaupd ( ido, bmat, lc, which, nev, tol, resid, &
  & ncv, v, lc, iparam, ipntr, workd, workl, lworkl, &
  & info )

  if (ido .eq. -1) then

    ! Perform y <--- OP*x = inv[K-SIGMA*KG]*K*x
    call mkl_dcsrsymv('U', lc, CSRMat_A, CSR_rowIndex, CSR_columns, &
      & workd(ipntr(1):(ipntr(1)+lc-1)), workd(ipntr(2):(ipntr(2)+lc-1)))
    workd(ipntr(1):(ipntr(1)+lc-1)) = workd(ipntr(2):(ipntr(2)+lc-1))

    !.. Back substitution and iterative refinement
    iparm_PAR(8) = 2 ! max numbers of iterative refinement steps
    phase = 33 ! only solving
    CALL pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_TMP, &
      & CSR_rowIndex, CSR_columns, idum, nrhs, iparm_PAR, msglvl, &
      & workd(ipntr(1):(ipntr(1)+lc-1)), workd(ipntr(2):(ipntr(2)+lc-1)), error)

    IF (error /= 0) THEN
      write(*,*) 'error in linear buckling analysis solver at phase', phase, 'ErrCODE:', error
      GOTO 9000
    END IF

    ! roop back
    go to 10

  else if (ido .eq. 1) then

    !.. Back substitution and iterative refinement
    iparm_PAR(8) = 2 ! max numbers of iterative refinement steps
    phase = 33 ! only solving
    CALL pardiso (pt, maxfct, mnum, mtype, phase, lc, CSRMat_TMP, &
      & CSR_rowIndex, CSR_columns, idum, nrhs, iparm_PAR, msglvl, &
      & workd(ipntr(3):(ipntr(3)+lc-1)), workd(ipntr(2):(ipntr(2)+lc-1)), error)

    IF (error /= 0) THEN
      write(*,*) 'error in linear buckling analysis solver at phase', phase, 'ErrCODE:', error
      GOTO 9000
    END IF

    ! roop back
    go to 10

  else if (ido .eq. 2) then

    ! Perform  y <--- K*x
    call mkl_dcsrsymv('U', lc, CSRMat_A, CSR_rowIndex, CSR_columns, &
      & workd(ipntr(1):(ipntr(1)+lc-1)), workd(ipntr(2):(ipntr(2)+lc-1)))

    ! roop back
    go to 10

  end if

  ! DSAUPD error
  if ( info .lt. 0 ) then
    print *, ' Error with _saupd, info = ',info
    print *, ' Check the documentation of _saupd '
  else
  ! Post-Process using DSEUPD.

    ! Need eigen vector
    rvec = .true.

    call dseupd ( rvec, 'All', select, eigenvalue, eigenvector, ldv, sigma, &
  & bmat, lc, which, nev, tol, resid, ncv, v, ldv, &
  & iparam, ipntr, workd, workl, lworkl, ierr )

    ! DSEUPD error
    if (ierr .ne. 0) then
      print *, ' Error with _seupd, info = ',ierr
      print *, ' Check the documentation of _seupd '
    else
      ! nconv =  iparam(5)
      ! do i = 1, nconv
      !   print *, eigenvalue(i)
      ! end do
    end if

!        %------------------------------------------%
!        | Print additional convergence information |
!        %------------------------------------------%
    if ( info .eq. 1) then
      print *, ' Maximum number of iterations reached.'
    else if ( info .eq. 3) then
      print *, ' No shifts could be applied during implicit', &
      &               ' Arnoldi update, try increasing NCV.'
    end if

  end if

9000 continue

  !.. Termination and release of memory
  phase = -1 ! release internal memory
  call pardiso (pt, maxfct, mnum, mtype, phase, lc, ddum, idum, idum, &
    & idum, nrhs, iparm_PAR, msglvl, ddum, ddum, error1)
  if (allocated(iparm_PAR)) deallocate(iparm_PAR)
  if (allocated(CSRMat_TMP)) deallocate(CSRMat_TMP)


  if (error1 /= 0) THEN
    write(*,*) 'The following ERROR on release stage was detected: ', error1
    return
  end if

  if (error /= 0) return

end subroutine dsdrv5_CSR

subroutine LDLtdecomp(nd,ind,skymat_A)
!===========================================================
!	[mkdiagonal]
!	指定行列(1次元配列)をLDLt分解する
!===========================================================
  integer,intent(in) :: nd
  integer,dimension(:),intent(in) :: ind
  real*8,dimension(:),intent(inout) :: skymat_A

  real*8 :: sum
  integer :: k,kwidth,kfirst
  integer :: i,iwidth,ifirst
  integer :: j,jwidth,jfirst
  integer :: ki,il,kl,ij,ji
  integer :: l,low,iback

  real*8 :: min,ss

  !----- [L][D][L]^T -----
  do k=2,nd
    kwidth = ind(k) - ind(k-1)
    kfirst = k - kwidth + 1
    do i=kfirst,k
      ki = ind(k) - k + i
      if(i /= 1) then
        iwidth = ind(i) - ind(i-1)
        ifirst = i - iwidth + 1
        low = max0(kfirst,ifirst)
        do l=low,i-1
          il = ind(i) - i + l
          kl = ind(k) - k + l
          skymat_A(ki) = skymat_A(ki) - skymat_A(il) * skymat_A(kl) * skymat_A(ind(l))
        end do
      end if

      if(skymat_A(ind(i)) <= 0.0d0) then
        stop "LDLtdecomp error マトリクスが不正定です。"
      else
        if(k /= i) then
          skymat_A(ki) = skymat_A(ki) / skymat_A(ind(i))
        end if
      end if
    end do
  end do

end subroutine LDLtdecomp

subroutine insert(skymat_A,Vec_B,ind,nd)
!===========================================================
!	[insert]
!	前進消去&後退代入
! skymat_A : skymat, Vec_B : vector
!===========================================================
  implicit none

  real(8),dimension(:),intent(inout) :: Vec_B
  real(8),dimension(:),intent(in) :: skymat_A
  integer,dimension(:),intent(in) :: ind
  integer,intent(in):: nd

  real(8) :: sum
  integer :: i,iwidth,ifirst
  integer :: j,jwidth,jfirst
  integer :: ki,il,kl,ij,ji
  integer :: l,low,iback

  !----- forward insert -----
  do i=2,nd
    iwidth = ind(i) - ind(i-1)
    ifirst = i - iwidth + 1
    do j=ifirst,i-1
      ij = ind(i) - i + j
      Vec_B(i) = Vec_B(i) - skymat_A(ij) * Vec_B(j)
    end do
  end do

  !----- back insert -----
  Vec_B(nd) = Vec_B(nd) / skymat_A(ind(nd))
  do iback=2,nd
    i = nd - iback + 1
    sum = 0.0d0
    do j=i+1,nd
      jwidth = ind(j) - ind(j-1)
      jfirst = j - jwidth + 1
      if(jfirst .le. i) then
        ji = ind(j) - j + i
        sum = sum + skymat_A(ji) * Vec_B(j)
      end if
    end do
    Vec_B(i) = Vec_B(i) / skymat_A(ind(i)) - sum
  end do

end subroutine insert

end module MATRIX_SOLVER
