![][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
! make mesh by grid data of BESO_DATA module
! using TETRA_MESH module
! to tetra model of MODEL_MOD
!   BESO_DATA
!      ↓
!      ↓ (grid data & criteria value)
!      ↓
!  TETRA_MESH
!      ↓
!      ↓ (tetra mesh)
!      ↓
!  MODEL_MOD
![][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
module MESH_HANDLE
  use BESO_DATA
  use TETRA_MESH
  use MODEL_MOD
  implicit none

  !*-連続性の確認
  integer, allocatable :: node_continuous(:) ! 連続部分を1、それ以外は、0とする。
  integer, allocatable :: elm_continuous(:) ! 連続部分を1、それ以外は、0とする。

contains

!-----------------------------------------------------------------------------
! subroutine Make_Tetra_Model
! メッシュを生成し、モデルを作成
!-----------------------------------------------------------------------------
subroutine Make_Tetra_Model
	use BESO_DATA
	implicit none
	integer :: new_node, num_tet, num_hex, num_allhex, inf, inf2
	real(8) :: vp00, NNNz

	allocate(xnew(Nd*2), ynew(Nd*2), znew(Nd*2))
	allocate(von(Nd))

	von(:) = Nd_sen(:)
	new_node = Nd
  num_allhex = El
	NNNz = Zn
	vp00 = vp0
  inf = 1
  inf2 = 2

  xnew(1:Nd) = Nd_in(1:Nd,1)
  ynew(1:Nd) = Nd_in(1:Nd,2)
  znew(1:Nd) = Nd_in(1:Nd,3)
  ! メッシュ生成
	call tet_mesh(new_node, num_tet, num_hex, num_allhex, vp00, NNNz, inf2, inf)
 
  ! 生成メッシュよりモデル生成
  allocate(node_continuous(new_node)); node_continuous(:) = 0
  allocate(elm_continuous(num_allhex)); elm_continuous(:) = 0
  call Remove_Isolate(new_node, num_allhex)

  call GenModel_Geometry(new_node, num_allhex)

  ! 生成メッシュのアウトプット
  call tet_mesh_inp(new_node,num_tet,num_hex,num_allhex)

  ! 生成結果よりモデルを作成(不要点削除等)
  ! call mapping_node(new_node, num_allhex)

  ! close TETRA_MESH module arrays
  deallocate(xnew, ynew, znew)
  deallocate(von)
  deallocate(ntet2)

  deallocate(node_continuous, elm_continuous)

end subroutine Make_Tetra_Model

!-------------------------------------------------------------------------------
! subroutine Location
! 4番目節点が、1,2,3番目節点の表にあるか裏にあるか判定
!-------------------------------------------------------------------------------
subroutine Location(targetElm, judge_reverse)
  integer, intent(in) :: targetElm
  integer, intent(out) :: judge_reverse

  real(8), parameter ::err=1.0d-15
  real(8) :: a, b, c, d
  integer :: pt1, pt2, pt3, pt4
  integer :: n
!  -- targetElm; the element include additional node

  pt1 = ntet2(targetElm,1)
  pt2 = ntet2(targetElm,2)
  pt3 = ntet2(targetElm,3)
  pt4 = ntet2(targetElm,4)
  call Trieq( &
  & (/xnew(pt1), ynew(pt1), znew(pt1)/),&
  & (/xnew(pt2), ynew(pt2), znew(pt2)/),&
  & (/xnew(pt3), ynew(pt3), znew(pt3)/),&
  & a, b, c, d)

  if ( a*xnew(pt4)+b*ynew(pt4)+c*znew(pt4)+d.lt.-err ) then
    ! 4点目が裏側
    judge_reverse = -1
  else
    ! 4点目が表側
    judge_reverse = 1
  endif

end subroutine Location

!-------------------------------------------------------------------------------
! subroutine Trieq
! 平面方程式の係数を計算
!-------------------------------------------------------------------------------
subroutine Trieq(Pt_i, Pt_j, Pt_k, a_Val, b_Val, c_Val, d_Val)
  real(8), dimension(3) :: Pt_i, Pt_j, Pt_k
  real(8) :: a_Val, b_Val, c_Val, d_Val

  a_Val = Pt_i(2)*Pt_j(3)+Pt_j(2)*Pt_k(3)+Pt_k(2)*Pt_i(3)-Pt_i(2)*Pt_k(3)-Pt_j(2)*Pt_i(3)-Pt_k(2)*Pt_j(3)
  b_Val = Pt_i(3)*Pt_j(1)+Pt_j(3)*Pt_k(1)+Pt_k(3)*Pt_i(1)-Pt_i(3)*Pt_k(1)-Pt_j(3)*Pt_i(1)-Pt_k(3)*Pt_j(1)
  c_Val = Pt_i(1)*Pt_j(2)+Pt_j(1)*Pt_k(2)+Pt_k(1)*Pt_i(2)-Pt_i(1)*Pt_k(2)-Pt_j(1)*Pt_i(2)-Pt_k(1)*Pt_j(2)
  d_Val = -a_Val*Pt_i(1)-b_Val*Pt_i(2)-c_Val*Pt_i(3)

end subroutine Trieq
!===================================================================
subroutine tet_mesh_inp(new_node,num_tet,num_hex,num_allhex)
!===================================================================
  implicit none
  integer, intent(in) :: new_node,num_tet,num_hex,num_allhex
  integer :: i

  open(10,file='./dataout/testout/'//'dataout.inp', action='write')
  write(10,'("1")')
  write(10,'("data_geom")')

  write(10,'("step1")')

  write(10,'(2I8)') new_node, num_allhex

  do i = 1, new_node
    write(10,fmt='(I8,3(1X,E15.7))') i, xnew(i), ynew(i), znew(i)
  end do
  do i = 1, num_allhex
    write(10,fmt='(2I8, A, 4I8)') i, 1, ' tet', ntet2(i,:)
  end do
  write(10,*) '1  1'
  write(10,*) '1 1'
  write(10,*) 'conti,'
  do i = 1, new_node
    write(10,'(i8,12I8)')  i, node_continuous(i)
  end do

  write(10,*) '1 1'
  write(10,*) 'conti,'
  do i = 1, MDL_elmNum
    write(10, '(I8,7(1X,I8))') i, elm_continuous(i)
  end do
  close(10)

end subroutine tet_mesh_inp

!-----------------------------------------------------
! subroutine mapping_node
! モデルデータを生成
!-----------------------------------------------------
subroutine mapping_node(new_node, num_allhex)
  integer, intent(in) :: new_node, num_allhex
  integer :: counter
  integer, allocatable :: node_map(:)

  integer :: judge_reverse

  integer :: i, j

  allocate(node_map(new_node))
  node_map(:) = 0

  do i = 1, num_allhex
    do j = 1, 4
      node_map(ntet2(i,j)) = node_map(ntet2(i,j)) + 1
    end do
  end do
  max_elments_num_node = maxval(node_map)

  counter = 0
  do i = 1, new_node
    ! モデル節点番号の指定
    if (node_map(i) /= 0) then
      counter = counter + 1
      node_map(i) = counter
    end if
  end do

  ! モデル節点座標の生成
  MDL_nodeNum = counter
  if (allocated(MDL_nodePos)) deallocate(MDL_nodePos)

  ! grid_node_map(i) = t
  !  i                : 設計領域節点番号
  !  t         (t/=0) : モデル節点番号
  !            (t==0) : 非モデル節点

  grid_node_map(1:Nd) = node_map(1:Nd)
  grid_node_model_num = count(grid_node_map(1:Nd)/=0)
  ! comment: ここではnode_mapで不要点を排除している。
  ! 格子点と、メッシュ構成節点の関係性は、ロストしてる。
  allocate(MDL_nodePos(MDL_nodeNum,3))
  MDL_nodePos(:,1) = pack(xnew(1:new_node), node_map(1:new_node)/=0)
  MDL_nodePos(:,2) = pack(ynew(1:new_node), node_map(1:new_node)/=0)
  MDL_nodePos(:,3) = pack(znew(1:new_node), node_map(1:new_node)/=0)

  ! モデル要素-節点関係の生成
  MDL_elmNum = num_allhex

  if (allocated(MDL_elm_node)) deallocate(MDL_elm_node)
  allocate(MDL_elm_node(MDL_elmNum,4))
  do i = 1, MDL_elmNum
    call Location(i, judge_reverse)
    if (judge_reverse==1) then
      MDL_elm_node(i,1) = node_map(ntet2(i,1))
      MDL_elm_node(i,2) = node_map(ntet2(i,2))
      MDL_elm_node(i,3) = node_map(ntet2(i,3))
      MDL_elm_node(i,4) = node_map(ntet2(i,4))
    else
      MDL_elm_node(i,1) = node_map(ntet2(i,1))
      MDL_elm_node(i,2) = node_map(ntet2(i,3))
      MDL_elm_node(i,3) = node_map(ntet2(i,2))
      MDL_elm_node(i,4) = node_map(ntet2(i,4))
    end if
  end do

end subroutine mapping_node

!-------------------------------------------------------------------------------
! subroutine GenModel_Geometry
! モデルジオメトリーを生成
! 支持点と荷重点を接続していない要素や不要節点を削除
! node_continuous: メッシュに含まれない節点と、連続性のない節点をマッピング
!-------------------------------------------------------------------------------
subroutine GenModel_Geometry(new_node, num_allhex)
  integer, intent(in) :: new_node, num_allhex
  ! integer, allocatable :: node_continuous(:)
  ! integer, allocatable :: elm_continuous(:)
  integer :: counter
  integer :: judge_reverse

  integer :: i

  counter = 0
  do i = 1, new_node
    if (node_continuous(i)/=0) then
      counter = counter + 1
      node_continuous(i) = counter
    end if
  end do

  ! 格子節点とモデル節点の関係性を作成
  grid_node_map(1:Nd) = node_continuous(1:Nd)
  grid_node_model_num = count(grid_node_map(1:Nd)/=0)

  ! モデルの節点データを作成
  MDL_nodeNum = counter
  allocate(MDL_nodePos(MDL_nodeNum,3))
  MDL_nodePos(:,1) = pack(xnew(1:new_node), node_continuous(1:new_node)/=0)
  MDL_nodePos(:,2) = pack(ynew(1:new_node), node_continuous(1:new_node)/=0)
  MDL_nodePos(:,3) = pack(znew(1:new_node), node_continuous(1:new_node)/=0)

  ! モデル要素-節点関係の生成
  counter = 0
  MDL_elmNum = count(elm_continuous(:)==1)
  allocate(MDL_elm_node(MDL_elmNum,4))
  do i = 1, num_allhex
    if (elm_continuous(i)/=1) cycle
    counter = counter + 1
    call Location(i, judge_reverse)
    if (judge_reverse==1) then
      MDL_elm_node(counter,1) = node_continuous(ntet2(i,1))
      MDL_elm_node(counter,2) = node_continuous(ntet2(i,2))
      MDL_elm_node(counter,3) = node_continuous(ntet2(i,3))
      MDL_elm_node(counter,4) = node_continuous(ntet2(i,4))
    else
      MDL_elm_node(counter,1) = node_continuous(ntet2(i,1))
      MDL_elm_node(counter,2) = node_continuous(ntet2(i,3))
      MDL_elm_node(counter,3) = node_continuous(ntet2(i,2))
      MDL_elm_node(counter,4) = node_continuous(ntet2(i,4))
    end if
  end do

end subroutine GenModel_Geometry

!-------------------------------------------------------------------------------
! subroutine Remove_Isolate
! 節点コネクティビティの支持節点・荷重節点からの連続性をマッピング
!-------------------------------------------------------------------------------
subroutine Remove_Isolate(new_node, num_allhex)
  integer, intent(in) :: new_node, num_allhex
  integer :: stack_num(2), node_stack(new_node,2)

  integer :: target_node, srch_node
  integer, allocatable :: continuous_nodeMap(:,:), continuous_elmMap(:,:)
  !  1: 連続性あり
  !  0: 未探索
  ! -1: 外部節点

  integer, allocatable :: node_share_element(:,:)
  integer, allocatable :: node_share_elmNum(:)

  integer :: i, j, k

  ! グリッド点 + 生成点のうち、結局モデルに含まれなかった点をマップ
  integer, allocatable :: node_map(:)
  ! 生成メッシュの節点がいくつの要素に属しているかの最大値
  ! その部分がモデルに含まれない場合もあるので、モデルの最大値にはならない
  integer :: max_elm_include
  ! 不使用節点があるため最大スタック数はnode_mapを生成しないとわからない。
  integer :: max_stack

  ! node_mapの生成
  allocate(node_map(new_node)); node_map(:) = 0
  do i = 1, num_allhex
    do j = 1, 4
      node_map(ntet2(i,j)) = node_map(ntet2(i,j)) + 1
    end do
  end do
  max_elm_include = maxval(node_map)
  max_stack = count(node_map(:)/=0)

  ! 節点の所属要素リスト作成
  allocate(node_share_element(new_node, max_elm_include))
  allocate(node_share_elmNum(new_node))
  node_share_element(:,:) = 0
  node_share_elmNum(:) = 0

  do i = 1, num_allhex
    do j = 1, 4
      node_share_elmNum(ntet2(i,j)) = node_share_elmNum(ntet2(i,j)) + 1
      node_share_element(ntet2(i,j),node_share_elmNum(ntet2(i,j))) = i
    end do
  end do

  ! 連続性マップの初期化
  allocate(continuous_nodeMap(new_node,2)); continuous_nodeMap(:,:) = -1
  allocate(continuous_elmMap(num_allhex,2)); continuous_elmMap(:,:) = -1
  ! node_mapから外部節点のマッピング
  do i = 1, new_node
    if (node_map(i) == 0) then
      continuous_nodeMap(i,1) = 0
    end if
  end do
  continuous_nodeMap(:,2) = continuous_nodeMap(:,1)

  ! 初期スタックリストと初期連続性マップ
  stack_num(:) = 0
  node_stack(:,:) = 0
  ! 支持節点で、位相内部の節点にマッピング
  do i = 1, 3
    do j = 1, NFIX_1(i)
      ! 支持節点がモデル外部ならばスキップ
      if (node_map(NFIXP_1(j,i))==0) cycle
      continuous_nodeMap(NFIXP_1(j,i),1) = 1
      stack_num(1) = stack_num(1) + 1
      node_stack(stack_num(1),1) = NFIXP_1(j,i)
    end do
  end do

  ! 荷重節点で、位相内部の節点にマッピング
  do i = 1, 3
    do j = 1, load_data(1)%Num_vec(i)
      ! 荷重節点がモデル外部ならばスキップ
      if (node_map(load_data(1)%ID_vec(j,i))==0) cycle
      continuous_nodeMap(load_data(1)%ID_vec(j,i),2) = 1
      stack_num(2) = stack_num(2) + 1
      node_stack(stack_num(2),2) = load_data(1)%ID_vec(j,i)
    end do
  end do

  ! スタックを消費し切るまで回る
  do i = 1, 2
    srch_roop: do
      target_node = node_stack(stack_num(i),i)
      stack_num(i) = stack_num(i) - 1

      do j = 1, node_share_elmNum(target_node)
        continuous_elmMap(node_share_element(target_node, j),i) = 1
        do k = 1, 4
          srch_node = ntet2(node_share_element(target_node, j),k)
          ! 探索先節点が未探索でない -> スキップ
          if (continuous_nodeMap(srch_node,i) /= -1) cycle

          ! 未探索なのでマッピング
          continuous_nodeMap(srch_node, i) = 1
          stack_num(i) = stack_num(i) + 1
          node_stack(stack_num(i),i) = srch_node
        end do
      end do

      ! 終了条件
      if (stack_num(i) <= 0) exit srch_roop
    end do srch_roop
  end do

  do i = 1, new_node
    if (all(continuous_nodeMap(i,:)==1)) then
      node_continuous(i) = 1
    else
      node_continuous(i) = 0
    end if
  end do

  do i = 1, num_allhex
    if (all(continuous_elmMap(i,:)==1)) then
      elm_continuous(i) = 1
    else
      elm_continuous(i) = 0
    end if
  end do

  max_elments_num_node = maxval(pack(node_share_elmNum(1:new_node), mask=(node_continuous(1:new_node)==1)))

end subroutine Remove_Isolate

end module MESH_HANDLE
