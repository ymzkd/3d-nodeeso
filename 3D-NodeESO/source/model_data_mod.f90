module MODEL_SUPER
  implicit none
  integer :: case_i
  character :: analyCaseStr*2
end module MODEL_SUPER

!===============================================================================
! module MODEL_MOD
! モデルの構成データ・解析データの変数群
!===============================================================================
!-* Variables *-
!	MDL_nodeNum            :節点数
!	MDL_elmNum            :要素数
!	MDL_nodePos(MDL_nodeNum,3)       :節点座標
!	MDL_elm_node(MDL_elmNum,8)    :要素−節点関係
!	nmt            :材料総数
!	mte(MDL_elmNum)       :要素の材料番号
!	nfix(6)        :各方向の拘束節点数
!	nfixp(MDL_nodeNum,6)   :各方向の並び替え後の拘束節点番号(線材ver)
!	eyg(nmt)       :ヤング率
!	poi(nmt)       :ポアソン比
!	nfix(3)        :x,y,z各方向の拘束節点数
!	nfixp(MDL_nodeNum,3)   :x,y,z各方向の並び替え後の拘束節点番号
!	lc             :真の自由度数
!	ind(lc)        :マトリクスの各行の対角項がスカイライン構造のアクティブコラム内で初めから数えて何番目にあたるかを示すインデックス
!                 （各列のアクティブコラムの高さを計算し，累積されている）
!	icrs(MDL_nodeNum*3)    :拘束節点自由度を取り除いた真の自由度の番号
!	disp(MDL_nodeNum*3)    :節点変位ベクトル
! mode_vectors(eigenSolveNum, MDL_nodeNum*3) : 座屈モード
!	ep(MDL_elmNum*6)      :節点ひずみベクトル

module MODEL_MOD
  implicit none

  type node_dispatch
    integer :: num(3), len, thiscase
    integer, allocatable :: nodes(:,:)
    real(8), allocatable :: vals(:,:)
  end type node_dispatch

  !*-モデルジオメトリ*-
  integer :: MDL_nodeNum, MDL_elmNum
  integer :: max_elments_num_node
  integer :: grid_node_model_num

  real(8), allocatable :: MDL_nodePos(:,:)
  integer, allocatable :: MDL_elm_node(:,:)
  real(8), allocatable :: MDL_elm_volume(:)

  !*-材料情報*-
  integer, allocatable :: mte(:)    ! 各要素の材料ID

  !*-荷重情報*-
  type(node_dispatch), allocatable :: MDL_load(:)
  integer :: MDL_loadNum(3)
  integer, allocatable  :: MDL_loadNode(:,:)
  real(8), allocatable  :: MDL_loadVal(:,:)

  !*-支持条件*-
  type(node_dispatch), allocatable :: MDL_support(:)
  integer :: nfix(3)
  integer, allocatable :: nfixp(:,:)

	integer, allocatable :: icrs(:)

  !*-線形解析結果*-
  !*-物理量*-
  !*-外力(総自由度)*-
  real(8), allocatable :: force(:)
  !*-変位(総自由度)*-
  real(8), allocatable :: disp(:)
  !*-ひずみ*-
	real(8), allocatable :: strain(:,:)
  !*-応力*-
  real(8), allocatable :: stress(:,:)
  !*-主応力*-
  real(8), allocatable :: pStress(:,:), pStressVec(:,:,:)
  ! MDLの変位便関数・エネルギーも追加
  ! 構造体ひずみエネルギー
  real(8) :: U_energyCase_i, V_energyCase_i
  !*-mise応力関連*-
  real(8), allocatable :: MDL_vonMises(:)
  real(8) :: vonMax, vonMin, vonMean, vonStd
  !*-変位敏感数関連*-
  real(8), allocatable :: MDL_nodedispSense(:)
  real(8), allocatable :: MDL_dispSense(:)
  real(8) :: DsenMax, DsenMin, DsenMean, DsenStd

  !*-座屈解析*-
  integer :: eigenSolveNum
  real(8), allocatable :: eigen_value(:)
  !*-モード(総自由度)*-
	real(8), allocatable :: mode_vectors(:,:)

  !*-座屈敏感数関連*-
  integer :: MDL_BsenJoinNum
  real(8), allocatable :: MDL_nodebuckSense(:,:)
  real(8), allocatable :: MDL_buckSense_list(:,:)
  real(8), allocatable :: BsenMax(:), BsenMin(:), BsenMean(:), BsenStd(:)

end module MODEL_MOD
