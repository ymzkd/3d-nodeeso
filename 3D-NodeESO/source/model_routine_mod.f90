module model_routines
  use MODEL_MOD
contains

!=====================================================================
                  subroutine strain_energy(F_vec, U_vec)
!                     ひずみエネルギーの計算
!=====================================================================
  use MATRIX_FORM
  implicit none
  real(8), intent(in) :: F_vec(:), U_vec(:)

  integer :: i,j

  !=====外力項=====
  U_energyCase_i = 0.d0
  do i = 1,lc
    U_energyCase_i = U_energyCase_i + F_vec(i) * U_vec(i) * 0.5d0
  end do

  !=====内力項=====
  V_energyCase_i = 0.d0
  do i = 1, MDL_elmNum
    do j = 1,6
      V_energyCase_i = V_energyCase_i + stress(i,j) * strain(i,j) * MDL_elm_volume(i)
    end do
  end do
  V_energyCase_i = V_energyCase_i / 2.d0

end subroutine strain_energy

!-------------------------------------------------------------------------------
! subroutine Principal_Stress
! 主応力求める。
!-------------------------------------------------------------------------------
subroutine Principal_Stress
  real(8) :: stressTensor(3,3)
  real(8) :: work(10)
  integer :: info

  do i = 1, MDL_elmNum
    ! テンソルの下三角に格納
    stressTensor(1,1) = stress(i,1)
    stressTensor(2,2) = stress(i,2)
    stressTensor(3,3) = stress(i,3)

    stressTensor(2,1) = stress(i,4)
    stressTensor(3,1) = stress(i,5)
    stressTensor(3,2) = stress(i,6)

    call dsyev('V', 'L', 3, stressTensor, 3, pStress(i,1:3), work, 10, info)
    if (info/=0) then
      print *, 'error occure at dsyev in Principal_Stress:'
      print *, 'error code : ', info
    end if

    pStressVec(i,1,1:3) = stressTensor(1:3,1)
    pStressVec(i,2,1:3) = stressTensor(1:3,2)
    pStressVec(i,3,1:3) = stressTensor(1:3,3)
  end do

end subroutine Principal_Stress

!=====================================================================
                    subroutine Cal_VonMises
!                Von Mises応力の計算
!=====================================================================
  use STD_CAL
  implicit none
  integer :: i

  !要素のVon Mises応力を求める。
  do i=1,MDL_elmNum
    MDL_vonMises(i)=sqrt(((stress(i,1)-stress(i,2))**2+(stress(i,2)-stress(i,3))**2+(stress(i,3)-stress(i,1))**2+6.0D0*(stress(i,4)**2+stress(i,5)**2+stress(i,6)**2))/2.0D0)
  end do

  ! 統計量の算出
  vonMax  = maxval(pack(MDL_vonMises(:),mte(:)==1))
  vonMin  = minval(pack(MDL_vonMises(:),mte(:)==1))
  vonMean = sum(pack(MDL_vonMises(:),mte(:)==1)) / count(mte(:)==1)
  vonStd  = std(pack(MDL_vonMises(:),mte(:)==1))

end subroutine Cal_VonMises

!-------------------------------------------------------------------------------
! subroutine sus_Buck_sense
! 座屈敏感数を求める。
!-------------------------------------------------------------------------------
subroutine sus_Buck_sense(mix_mode, eigen_vector, gkt_CSR)
  use GeoMat_GenMOD
  use StiffMat_GenMOD
  use STD_CAL
  use MATRIX_FORM
  implicit none
  integer, intent(in) :: mix_mode
  real(8), intent(in) :: eigen_vector(:,:), gkt_CSR(:)

  real(8) :: el_mode_Vec(12,mix_mode)
  real(8) :: weight_bottom
  real(8) :: rayleigh_bottom(mix_mode), rayleigh_top(mix_mode)
  real(8) :: penalty = 1.d0

  integer :: full_ind, lc_ind
  real(8) :: tmp_Vec_A(lc), tmp_Vec_B(12), tmp_Mat_E(12,12)
  integer :: i, j, k, ie

  MDL_buckSense_list(:,:) = 0.d0

  ! weight_bottom = 0.d0
  ! do i = 1, mix_mode
  !   weight_bottom = weight_bottom + 1.d0 / (eigen_value(i)**penalty)
  ! end do

  ! 分母は、要素ごとに違わない
  do i = 1, mix_mode
    call mkl_dcsrsymv('U', lc, gkt_CSR, CSR_rowIndex, CSR_columns, &
      & eigen_vector(:,i), tmp_Vec_A(:))
    rayleigh_bottom(i) = dot_product(tmp_Vec_A(:), eigen_vector(:,i))
  end do

  do ie = 1, MDL_elmNum
    el_mode_Vec(:,:) = 0.d0
    do i = 1, mix_mode
      do j = 1, 4
        do k = 1, 3
          full_ind = (MDL_elm_node(ie,j) - 1) * 3 + k
          lc_ind = icrs(full_ind)
          if(lc_ind == 0) cycle
          el_mode_Vec((j - 1) * 3 + k,i) = eigen_vector(lc_ind,i)
        end do
      end do
    end do

    ! 要素剛性マトリクス    : em
    ! 要素幾何剛性マトリクス : geo_elmMat
    call mkStiff_elMat(ie)
    call mkGeo_elMat(ie)

    do i = 1, mix_mode
      tmp_Mat_E(:,:) = eigen_value(i) * geo_elmMat(:,:) - elm_Mat(:,:)
      call dsymv('U', 12, 1.d0, tmp_Mat_E(1:12,1:12), 12, el_mode_Vec(:,i), 1, &
        & 0.d0, tmp_Vec_B(1:12), 1)
      rayleigh_top(i) = dot_product(el_mode_Vec(:,i), tmp_Vec_B(:))
    end do

    do i = 1, mix_mode
      MDL_buckSense_list(ie,i) = rayleigh_top(i) / rayleigh_bottom(i) / detJ
      ! MDL_buckSense_list(ie,i) = rayleigh_top(i) / rayleigh_bottom(i)
    end do

  end do

  ! 統計量の算出
  do i = 1, MDL_BsenJoinNum
    BsenMax(i)  = maxval(MDL_buckSense_list(:,i))
    BsenMin(i)  = minval(MDL_buckSense_list(:,i))
    BsenMean(i) = sum(MDL_buckSense_list(:,i)) / size(MDL_buckSense_list(:,i))
    BsenStd(i)  = std(MDL_buckSense_list(:,i))
  end do

end subroutine sus_Buck_sense

!-------------------------------------------------------------------------------
! [subroutine mirror]
! 真の自由度を総自由度に転写する。変位や、モードなんか。
!-------------------------------------------------------------------------------
subroutine mirror(icrs, nume_flx_Vec, full_flx_Vec)
  implicit none
  integer, dimension(:), intent(in) :: icrs
  real(8), dimension(:), intent(in) :: nume_flx_Vec
  real(8), dimension(:), intent(out) :: full_flx_Vec

  integer :: i

  full_flx_Vec(:) = 0.d0
  do i = 1, size(icrs(:))
    if (icrs(i) /= 0) then
      full_flx_Vec(i) = nume_flx_Vec(icrs(i))
    end if
  end do
end subroutine mirror

end module model_routines
