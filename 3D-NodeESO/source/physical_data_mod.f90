module PHYSICAL_DATA
  implicit none
  integer :: mtNum = 2       ! 材料総数
  real(8), allocatable :: young_list(:)
  real(8), allocatable :: poisson_list(:)
  real(8), allocatable :: density_list(:)
  real(8) :: unitVolume

  integer :: grav_weight=0                               !自重の有無の情報
  integer :: gravityDir = -3       !重力方向(x:1,y:2,z:3,sign:axial direction)

end module PHYSICAL_DATA
