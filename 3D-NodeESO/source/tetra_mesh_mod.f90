module TETRA_MESH
  use BESO_DATA, only: El_in
  real(8) :: err = 1.d-12
  real(8), allocatable :: xnew(:), ynew(:), znew(:)
  real(8), allocatable :: x_inter(:), y_inter(:), z_inter(:)

  integer, allocatable :: ntet2(:,:)
  integer, allocatable :: map5(:)
  integer, allocatable :: map7(:,:)
  integer, allocatable :: nline_inter(:,:)
  integer, allocatable :: num_gravity(:)

  integer :: num_same_value(8), Msv(6), LD0(6), LL6(6)
  real(8) :: DC0(6)
  real(8), allocatable :: von(:), von_gravity(:)

contains

!============================================================================================================================
  subroutine tet_mesh(new_node,num_tet,num_hex,num_allhex,vp0,NNNz,inf2,inf)
!	→四面体要素分割

!	※引数詳細
!	・new_node0							：総節点数(格子節点＋境界節点＋重心節点) ← 節点の同一化処理後
!	→最終的にnew_nodeに値を格納
!	・num_tet0							：四面体要素を数え上げたもの(微小要素処理前)
!	・num_allhex0						：四面体要素を数え上げたもの(微小要素処理後)　→　m_data_inp4でのみ利用
!	→最終的にnum_allhexに値を格納
!	・num_hex0							：今何番目の六面体要素について考えているか
!	・ncut								：ある六面体の８節点において、基準値以下の点がいくつあるかをカウントしたもの（最大８）
!	・num_interpolation					：境界節点と重心節点の総数
!	・num_interpolation0				：一つの六面体における境界節点の数
!=============================================================================================================================
  implicit none

  real*8,dimension(:),allocatable :: map11		!節点(格子節点・境界節点・重心節点)のが持つ敏感数値の情報
  integer :: i,j,ncut,kv,inf,inf2
  integer :: nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,n1,n2,n3,n4
  integer :: check1,check2,check3,check4,check5,check6,check0
  integer :: new_node,num_tet,num_hex,num_allhex,g_point
  integer :: num_node,num_tet0,num_hex0,num_gravity0,num_interpolation,num_interpolation0,num_g0,new_node0,num_allhex0
  real*8 :: vp0,vol0,g_von,NNNz

  allocate (ntet2(num_allhex*6*5,4))
  allocate (num_gravity(num_allhex))
  allocate (von_gravity(num_allhex))
  allocate (nline_inter(num_allhex*12,3))
  allocate (x_inter(num_allhex*12),y_inter(num_allhex*12),z_inter(num_allhex*12))
  ntet2=0
  num_gravity=0
  von_gravity=0
  nline_inter=0
  x_inter=0.0d0 ; y_inter=0.0d0 ; z_inter=0.0d0

  open(11,file='./dataout/testout/'//'child_hex.txt')

  num_node=0
  num_tet0=0
  num_hex0=0
  num_gravity0=0
  num_interpolation=0

  do j=1,num_allhex
    num_hex0=num_hex0+1
    nn01=El_in(j,1)
    nn02=El_in(j,2)
    nn03=El_in(j,4)
    nn04=El_in(j,3)
    nn05=El_in(j,5)
    nn06=El_in(j,6)
    nn07=El_in(j,8)
    nn08=El_in(j,7)
    write(11,111) nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08
    !  -- count cut points --
    ncut=0
    g_von=0.0d0
    if(von(nn01)<Vp0)ncut=ncut+1
    if(von(nn02)<Vp0)ncut=ncut+1
    if(von(nn03)<Vp0)ncut=ncut+1
    if(von(nn04)<Vp0)ncut=ncut+1
    if(von(nn05)<Vp0)ncut=ncut+1
    if(von(nn06)<Vp0)ncut=ncut+1
    if(von(nn07)<Vp0)ncut=ncut+1
    if(von(nn08)<Vp0)ncut=ncut+1

    if(von(nn01) > Vp0)g_von=g_von+von(nn01)
    if(von(nn02) > Vp0)g_von=g_von+von(nn02)
    if(von(nn03) > Vp0)g_von=g_von+von(nn03)
    if(von(nn04) > Vp0)g_von=g_von+von(nn04)
    if(von(nn05) > Vp0)g_von=g_von+von(nn05)
    if(von(nn06) > Vp0)g_von=g_von+von(nn06)
    if(von(nn07) > Vp0)g_von=g_von+von(nn07)
    if(von(nn08) > Vp0)g_von=g_von+von(nn08)

    ! 等分布荷重時の処理
    ! if(inf2==2)then
    !   if(znew(nn08) == NNNz) ncut=0	!等分布荷重を受ける六面体の時は、無条件で６つの四面体に分割する
    ! end if

    !  case 1  %%%%%
    if(nn01==nn05 .and. nn02==nn06 .and. nn01/=nn02/=nn03/=nn04/=nn07/=nn08)then
      if(ncut==0)then
        !  --"case1:no cut point"--
        !  --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn07
        !  --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn07
        !  --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn04
        ntet2(num_tet0,3)=nn08
        ntet2(num_tet0,4)=nn07
      elseif(ncut==8)then
        !  --"case1:8 cut points"--
      else
        !  --"case1:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        ! call cut_on_face4(nn03,nn04,nn01,nn02,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn03,nn04,nn02,nn01,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)		!修正
        call cut_on_face4(nn03,nn04,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn02,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn02,nn04,nn08,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn07,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 2  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn02==nn06 .and. nn04==nn08 .and. nn01/=nn02/=nn03/=nn04/=nn05/=nn07)then
      if(ncut==0)then
        !  --"case2:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn05
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn02
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn07
        ntet2(num_tet0,3)=nn03
        ntet2(num_tet0,4)=nn04
      elseif(ncut==8)then
        !  --"case2:8 cut points"--
      else
        !  --"case2:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn07,nn05,nn02,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn03,nn04,nn02,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn01,nn02,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn03,nn07,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 3  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn04==nn08 .and. nn03==nn07 .and. nn01/=nn02/=nn03/=nn04/=nn05/=nn06)then
      if(ncut==0)then
        !  --"case3:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn02
        ntet2(num_tet0,3)=nn05
        !				ntet2(num_tet0,4)=nn07
        ntet2(num_tet0,4)=nn03
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn06
        ntet2(num_tet0,3)=nn05
        !				ntet2(num_tet0,4)=nn07
        ntet2(num_tet0,4)=nn03
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn06
        !				ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,3)=nn03
        !				ntet2(num_tet0,4)=nn08
        ntet2(num_tet0,4)=nn04
      elseif(ncut==8)then
        !  --"case3:8 cut points"--
      else
        !  --"case3:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        !				call cut_on_face4(nn01,nn02,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn02,nn04,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        !				call cut_on_face4(nn05,nn06,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn05,nn06,nn04,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        !				call cut_on_face3(nn02,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn02,nn04,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0)
        !				call cut_on_face3(nn01,nn05,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn05,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 4  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn03==nn07 .and. nn01==nn05 .and. nn01/=nn02/=nn03/=nn04/=nn06/=nn08)then
      if(ncut==0)then
        !  --"case4:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn06
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn06
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn04
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn08
        ntet2(num_tet0,4)=nn06
      elseif(ncut==8)then
        !  --"case4:8 cut points"--
      else
        !  --"case4:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn02,nn04,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn02,nn04,nn03,nn01,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn06,nn08,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn01,nn02,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn03,nn08,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 5  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn01==nn02 .and. nn05==nn06 .and. nn01/=nn03/=nn04/=nn05/=nn07/=nn08)then
      if(ncut==0)then
        !  --"case5:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn03
        ntet2(num_tet0,4)=nn04
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn04
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn04
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn08
      elseif(ncut==8)then
        !  --"case5:8 cut points"--
      else
        !  --"case5:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn04,nn08,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn03,nn04,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn05,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn03,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 6  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn05==nn06 .and. nn07==nn08 .and. nn01/=nn02/=nn03/=nn04/=nn05/=nn07)then
      if(ncut==0)then
        !  --"case6:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn05
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn05
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn07
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn05
      elseif(ncut==8)then
        !  --"case6:8 cut points"--
      else
        !  --"case6:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn02,nn04,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn02,nn04,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn01,nn02,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn03,nn07,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 7  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn07==nn08 .and. nn03==nn04 .and. nn01/=nn02/=nn03/=nn05/=nn06/=nn07)then
      if(ncut==0)then
        !  --"case7:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn02
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn03
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn05
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn07
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn05
      elseif(ncut==8)then
        !  --"case7:8 cut points"--
      else
        !  --"case7:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn03,nn02,nn06,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn05,nn06,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn03,nn02,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 8  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn03==nn04 .and. nn01==nn02 .and. nn01/=nn03/=nn05/=nn06/=nn07/=nn08)then
      if(ncut==0)then
        !  --"case8:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn03
        ntet2(num_tet0,4)=nn06
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn05
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn06
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn07
        ntet2(num_tet0,3)=nn08
        ntet2(num_tet0,4)=nn06
      elseif(ncut==8)then
        !  --"case8:8 cut points"--
      else
        !  --"case8:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn03,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn05,nn06,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn01,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn03,nn07,nn08,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 9  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn01==nn03 .and. nn02==nn04 .and. nn01/=nn02/=nn05/=nn06/=nn07/=nn08)then
      if(ncut==0)then
        !  --"case9:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn02
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn07
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn06
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn07
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn08
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn07
      elseif(ncut==8)then
        !  --"case9:8 cut points"--
      else
        !  --"case9:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn02,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn05,nn06,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn02,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn05,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 10  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn02==nn04 .and. nn06==nn08 .and. nn01/=nn02/=nn03/=nn05/=nn06/=nn07)then
      if(ncut==0)then
        !  --"case10:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn02
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn03
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn06
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn07
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn05
      elseif(ncut==8)then
        !  --"case10:8 cut points"--
      else
        !  --"case10:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn06,nn02,nn03,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn05,nn06,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn03,nn02,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 11  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn06==nn08 .and. nn05==nn07 .and. nn01/=nn02/=nn03/=nn04/=nn05/=nn06)then
      if(ncut==0)then
        !  --"case11:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn05
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn05
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn04
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn05
      elseif(ncut==8)then
        !  --"case11:8 cut points"--
      else
        !  --"case11:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn03,nn04,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
      !				call cut_on_face4(nn01,nn02,nn03,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn02,nn04,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn02,nn04,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn05,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
  !  case 12  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif(nn05==nn07 .and. nn01==nn03 .and. nn01/=nn02/=nn04/=nn05/=nn06/=nn08)then
      if(ncut==0)then
        !  --"case12:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn04
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn05
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn04
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn05
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn04
        ntet2(num_tet0,2)=nn08
        ntet2(num_tet0,3)=nn06
        ntet2(num_tet0,4)=nn05
      elseif(ncut==8)then
        !  --"case12:8 cut points"--
      else
        !  --"case12:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(num_hex0)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn02,nn04,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face4(nn01,nn04,nn08,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check0)
        call cut_on_face3(nn05,nn06,nn08,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call cut_on_face3(nn01,nn04,nn02,num_g0,num_tet0,num_interpolation,new_node,vp0)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(num_hex0)=g_von
      endif
    !  case 13 else  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
      if(ncut==0)then
        !  --"case else:no cut point"--
        ! --ntet1
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn01
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn02
        ntet2(num_tet0,4)=nn05
        ! --ntet2
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn06
        ntet2(num_tet0,3)=nn05
        ntet2(num_tet0,4)=nn03
        ! --ntet3
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn05
        ntet2(num_tet0,2)=nn06
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn03
        ! --ntet4
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn02
        ntet2(num_tet0,2)=nn03
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn06
        ! --ntet5
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn03
        ntet2(num_tet0,2)=nn07
        ntet2(num_tet0,3)=nn04
        ntet2(num_tet0,4)=nn06
        ! --ntet6
        num_tet0=num_tet0+1
        ntet2(num_tet0,1)=nn06
        ntet2(num_tet0,2)=nn08
        ntet2(num_tet0,3)=nn07
        ntet2(num_tet0,4)=nn04
      elseif(ncut==8)then
        !  --"case else:8 cut points"--
      else
        !  --"case else:1〜7 cut points"--
        call gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
        num_g0=num_gravity(j)
        call cut_on_face4(nn02,nn04,nn08,nn06,num_g0,num_tet0,num_interpolation,new_node,vp0,check1)
        call cut_on_face4(nn03,nn04,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check2)
        call cut_on_face4(nn05,nn06,nn08,nn07,num_g0,num_tet0,num_interpolation,new_node,vp0,check3)
        call cut_on_face4(nn01,nn02,nn04,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0,check4)
        call cut_on_face4(nn01,nn02,nn06,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check5)
        call cut_on_face4(nn01,nn03,nn07,nn05,num_g0,num_tet0,num_interpolation,new_node,vp0,check6)
        call same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
        !重心節点の応力を求める
        g_von=g_von+(num_interpolation0*Vp0)
        g_von=g_von/(8-ncut+num_interpolation0)
        von_gravity(j)=g_von
      endif
    endif
  enddo
  close(11)

  allocate(map11(new_node+num_interpolation))
  map11=0
  do i=1,new_node
    map11(i)=von(i)
  end do
  do i=new_node+1,new_node+num_interpolation
    map11(i)=Vp0
  end do
  do i=1,num_allhex
    g_point=num_gravity(i)
    g_von=von_gravity(i)
    if(g_point /= 0)then
      map11(new_node+g_point)=g_von
    end if
  end do

  !-----詳細-------------------------------------------------
  if(inf == 1)then
    open(24,file='./dataout/testout/微小要素削除前.txt')
    do i=1,num_tet0
      write(24,*)i,ntet2(i,1),ntet2(i,2),ntet2(i,3),ntet2(i,4)
    end do
    write(24,*)
    do i=1,new_node+num_interpolation
      write(24,*)i,map11(i)
    end do
  end if

  !  -- adding gravity coordinates and interpolate coordinates
  do i=1,num_interpolation
    new_node=new_node+1
    xnew(new_node)=x_inter(i)
    ynew(new_node)=y_inter(i)
    znew(new_node)=z_inter(i)
  enddo

  !  -- deleting the useless tetrahedral element ---------------
  allocate (map7(new_node,2))
  do i=1,new_node
    map7(i,1)=1
    map7(i,2)=0
  enddo
  do i=1,new_node
    if(map7(i,2)==0)then
      do j=1,new_node
        if(i/=j)then
          if( dabs(xnew(i)-xnew(j))<err .and. dabs(ynew(i)-ynew(j))<err .and. dabs(znew(i)-znew(j))<err )then
            map7(j,1)=0
            map7(j,2)=i
            map11(j)=0
          endif
        endif
      enddo
    endif
  enddo

  kv=0
  do i=1,new_node
    if(map7(i,1)/=0)then
      kv=kv+1
      map7(i,1)=kv
      map11(kv)=map11(i)
    endif
  enddo

  map11(kv+1:new_node)=0

  open(11,file='./dataout/testout/'//'map7.txt')
  do i=1,new_node
    write(11,*) (map7(i,j),j=1,2)
  enddo
  close(11)

  !四面体の節点番号を処理
  do i=1,num_tet0
    do j=1,4
      if (map7(ntet2(i,j),2)==0) then
        ntet2(i,j)=map7(ntet2(i,j),1)
      else
        ntet2(i,j)=map7(map7(ntet2(i,j),2),1)
      endif
    enddo
  enddo

  !節点番号と座標関係を処理
  kv=0
  do i=1,new_node
    j=map7(i,1)
    if(j/=0)then
      kv=kv+1
      xnew(j)=xnew(i)
      ynew(j)=ynew(i)
      znew(j)=znew(i)
    end if
  end do
  do i=kv+1,new_node
    xnew(i)=0.0d0
    ynew(i)=0.0d0		!余った部分を0クリア
    znew(i)=0.0d0
  end do

  new_node0=kv			!総節点数

  deallocate (map7)

  allocate (map5(num_tet0))
  map5=0
  do i=1,num_tet0
    n1=ntet2(i,1)
    n2=ntet2(i,2)
    n3=ntet2(i,3)
    n4=ntet2(i,4)
    vol0=dabs(volume(n1,n2,n3,n4))
    if(vol0<err)then
      map5(i)=2
    endif
    if(n1==n2) map5(i)=1
    if(n1==n3) map5(i)=1
    if(n1==n4) map5(i)=1
    if(n2==n3) map5(i)=1
    if(n2==n4) map5(i)=1
    if(n3==n4) map5(i)=1
  enddo

  open(11,file='./dataout/testout/'//'volume.txt')
  do i=1,num_tet0
    write(11,*) dabs(volume(ntet2(i,1),ntet2(i,2),ntet2(i,3),ntet2(i,4)))
  enddo
  close(11)

  open(11,file='./dataout/testout/'//'map5.txt')
  do i=1,num_tet0
    write(11,*) map5(i)
  enddo
  close(11)

  !微小要素の処理
  kv=0
  do i=1,num_tet0
    if(map5(i)==0)then
      kv=kv+1
      ntet2(kv,1)=ntet2(i,1)
      ntet2(kv,2)=ntet2(i,2)
      ntet2(kv,3)=ntet2(i,3)
      ntet2(kv,4)=ntet2(i,4)
    endif
  enddo

!---------------------------------------------------------
  open(24,file='./dataout/testout/ntet2_map11')
  do i=1,kv
    write(24,*)i,ntet2(i,1),ntet2(i,2),ntet2(i,3),ntet2(i,4)
  end do
  do i=1,new_node
    write(24,*)i,map11(i)
  end do
  close(24)
!----------------------------------------------------------

  do i=kv+1,num_tet0
    ntet2(i,:)=0
  enddo
  num_allhex0=kv
  deallocate (map5)

  new_node=new_node0
  num_allhex=num_allhex0

  write(*,*)num_tet0,'  --->   ',kv

  deallocate (num_gravity,von_gravity)
  deallocate (nline_inter)
  deallocate (x_inter,y_inter,z_inter)
  deallocate (map11)

111 format(8i5)

end subroutine tet_mesh

!===================================================================
subroutine gravity_coordinate(nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0,vp0)
!	→六面体の各辺（１２本）に境界節点が存在するか調べる
!		全ての座標値から重心の座標を求める
!===================================================================
  implicit none

  integer :: nn01,nn02,nn03,nn04,nn05,nn06,nn07,nn08,num_hex0,num_interpolation,num_interpolation0
  integer :: kv0,nn00
  real*8 :: vp0,x0,y0,z0

  num_interpolation0=0
  num_same_value=0
  kv0=0
  x0=0
  y0=0
  z0=0
  if(von(nn01)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn01)
    y0=y0+ynew(nn01)
    z0=z0+znew(nn01)
  end if
  if(von(nn02)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn02)
    y0=y0+ynew(nn02)
    z0=z0+znew(nn02)
  end if
  if(von(nn03)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn03)
    y0=y0+ynew(nn03)
    z0=z0+znew(nn03)
  end if
  if(von(nn04)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn04)
    y0=y0+ynew(nn04)
    z0=z0+znew(nn04)
  end if
  if(von(nn05)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn05)
    y0=y0+ynew(nn05)
    z0=z0+znew(nn05)
  end if
  if(von(nn06)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn06)
    y0=y0+ynew(nn06)
    z0=z0+znew(nn06)
  end if
  if(von(nn07)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn07)
    y0=y0+ynew(nn07)
    z0=z0+znew(nn07)
  end if
  if(von(nn08)>=vp0)then
    kv0=kv0+1
    x0=x0+xnew(nn08)
    y0=y0+ynew(nn08)
    z0=z0+znew(nn08)
  endif
! -- interpolate line 1
  if (von(nn01)>=vp0 .and. von(nn02)<vp0) then
    call interpolation(nn01,nn02,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn02)>=vp0 .and. von(nn01)<vp0) then
    call interpolation(nn01,nn02,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 2
  if (von(nn02)>=vp0 .and. von(nn04)<vp0) then
    call interpolation(nn02,nn04,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn04)>=vp0 .and. von(nn02)<vp0) then
    call interpolation(nn02,nn04,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 3
  if (von(nn03)>=vp0 .and. von(nn04)<vp0) then
    call interpolation(nn03,nn04,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn04)>=vp0 .and. von(nn03)<vp0) then
    call interpolation(nn03,nn04,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 4
  if (von(nn01)>=vp0 .and. von(nn03)<vp0) then
    call interpolation(nn01,nn03,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn03)>=vp0 .and. von(nn01)<vp0) then
    call interpolation(nn01,nn03,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
! -- interpolate line 5
  if (von(nn05)>=vp0 .and. von(nn06)<vp0) then
    call interpolation(nn05,nn06,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn06)>=vp0 .and. von(nn05)<vp0) then
    call interpolation(nn05,nn06,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 6
  if (von(nn06)>=vp0 .and. von(nn08)<vp0) then
    call interpolation(nn06,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn08)>=vp0 .and. von(nn06)<vp0) then
    call interpolation(nn06,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 7
  if (von(nn07)>=vp0 .and. von(nn08)<vp0) then
    call interpolation(nn07,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn08)>=vp0 .and. von(nn07)<vp0) then
    call interpolation(nn07,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 8
  if (von(nn05)>=vp0 .and. von(nn07)<vp0) then
    call interpolation(nn05,nn07,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn07)>=vp0 .and. von(nn05)<vp0) then
    call interpolation(nn05,nn07,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 9
  if (von(nn01)>=vp0 .and. von(nn05)<vp0) then
    call interpolation(nn01,nn05,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn05)>=vp0 .and. von(nn01)<vp0) then
    call interpolation(nn01,nn05,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 10
  if (von(nn02)>=vp0 .and. von(nn06)<vp0) then
    call interpolation(nn02,nn06,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn06)>=vp0 .and. von(nn02)<vp0) then
    call interpolation(nn02,nn06,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 11
  if (von(nn04)>=vp0 .and. von(nn08)<vp0) then
    call interpolation(nn04,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn08)>=vp0 .and. von(nn04)<vp0) then
    call interpolation(nn04,nn08,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if
  ! -- interpolate line 12
  if (von(nn03)>=vp0 .and. von(nn07)<vp0) then
    call interpolation(nn03,nn07,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  elseif (von(nn07)>=vp0 .and. von(nn03)<vp0) then
    call interpolation(nn03,nn07,nn00,num_interpolation,vp0)
    kv0=kv0+1
    num_interpolation0=num_interpolation0+1
    num_same_value(num_interpolation0)=nn00
    x0=x0+x_inter(nn00)
    y0=y0+y_inter(nn00)
    z0=z0+z_inter(nn00)
  end if

  num_interpolation=num_interpolation+1
  num_gravity(num_hex0)=num_interpolation
  x_inter(num_interpolation)=x0/kv0
  y_inter(num_interpolation)=y0/kv0
  z_inter(num_interpolation)=z0/kv0
!print*,'How many nodes was made in this hex without gravity node. =>',num_interpolation0
!print*,'kv0',kv0
!print*,'num_interpolation',num_interpolation,x0/kv0,y0/kv0,z0/kv0
!print*,'through gravity_coordinate'
end subroutine gravity_coordinate

!===================================================================
  subroutine cut_on_face4(nn01,nn02,nn03,nn04,num_g0,num_tet0,num_interpolation,new_node,vp0,check)
!	→　四角形平面と重心節点から四面体を生成
!===================================================================
  implicit none

  integer :: nn01,nn02,nn03,nn04,num_g0,num_tet0,num_interpolation,new_node
  integer :: kk00,nn12,nn14,nn23,nn34,check
  real*8 :: vp0,dist1,dist2

  check=0
!%%%%%%%%%%%%%% tetrahedron(use the gravity point) %%%%%%%%%%%%%%%%%%%%%%%%
! == No.0
  if( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 .and. von(nn04)>=vp0 )then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
! == No.1
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 101
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 101
  endif
  enddo
  101	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 102
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 102
  endif
  enddo
  102	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn03
  ntet2(num_tet0,2)=nn14+new_node
  ntet2(num_tet0,3)=nn12+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
! == No.2
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 103
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 103
  endif
  enddo
  103	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 104
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 104
  endif
  enddo
  104	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn23+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
! == No.3
  elseif( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 105
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 105
  endif
  enddo
  105	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 106
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 106
  endif
  enddo
  106	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn23+new_node
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn01
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
! == No.4
  elseif( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 107
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 107
  endif
  enddo
  107	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 108
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 108
  endif
  enddo
  108	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == No.5
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)<vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 109
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 109
  endif
  enddo
  109	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 110
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 110
  endif
  enddo
  110	continue
  dist1=sqrt( (x_inter(nn12)-xnew(nn04))**2 + (y_inter(nn12)-ynew(nn04))**2 + (z_inter(nn12)-znew(nn04))**2 )
  dist2=sqrt( (xnew(nn01)-x_inter(nn34))**2 + (ynew(nn01)-y_inter(nn34))**2 + (znew(nn01)-z_inter(nn34))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.6（なかなか起こらない）
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 .and. von(nn04)<vp0 )then
  check=6
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 111
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 111
  endif
  enddo
  111	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 112
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 112
  endif
  enddo
  112	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 113
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 113
  endif
  enddo
  113	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 114
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 114
  endif
  enddo
  114	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn03
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node

  dist1=dsqrt( (x_inter(nn14)-x_inter(nn23))**2 + (y_inter(nn14)-y_inter(nn23))**2 + (z_inter(nn14)-z_inter(nn23))**2 )
  dist2=dsqrt( (x_inter(nn12)-x_inter(nn34))**2 + (y_inter(nn12)-y_inter(nn34))**2 + (z_inter(nn12)-z_inter(nn34))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn34+new_node
  ntet2(num_tet0,2)=nn14+new_node
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn23+new_node
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn12+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

! == No.7
  elseif( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 115
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 115
  endif
  enddo
  115	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 116
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 116
  endif
  enddo
  116	continue
  dist1=dsqrt( (x_inter(nn14)-xnew(nn02))**2 + (y_inter(nn14)-ynew(nn02))**2 + (z_inter(nn14)-znew(nn02))**2 )
  dist2=dsqrt( (xnew(nn01)-x_inter(nn23))**2 + (ynew(nn01)-y_inter(nn23))**2 + (znew(nn01)-z_inter(nn23))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn01
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.8
  elseif( von(nn01)<vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 117
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 117
  endif
  enddo
  117	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 118
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 118
  endif
  enddo
  118	continue
  dist1=sqrt( (xnew(nn04)-x_inter(nn23))**2 + (ynew(nn04)-y_inter(nn23))**2 + (znew(nn04)-z_inter(nn23))**2 )
  dist2=sqrt( (xnew(nn03)-x_inter(nn14))**2 + (ynew(nn03)-y_inter(nn14))**2 + (znew(nn03)-z_inter(nn14))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn23+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn04
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.9（なかなか起こらない）
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 .and. von(nn04)>=vp0 )then
  check=9
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 119
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 119
  endif
  enddo
  119	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 120
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 120
  endif
  enddo
  120	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 121
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 121
  endif
  enddo
  121	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 122
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 122
  endif
  enddo
  122	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn12+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn04
  ntet2(num_tet0,2)=nn14+new_node
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node

  dist1=dsqrt( (x_inter(nn14)-x_inter(nn23))**2 + (y_inter(nn14)-y_inter(nn23))**2 + (z_inter(nn14)-z_inter(nn23))**2 )
  dist2=dsqrt( (x_inter(nn12)-x_inter(nn34))**2 + (y_inter(nn12)-y_inter(nn34))**2 + (z_inter(nn12)-z_inter(nn34))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn34+new_node
  ntet2(num_tet0,2)=nn14+new_node
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn14+new_node
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn23+new_node
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn12+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

! == No.10
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 123
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 123
  endif
  enddo
  123	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 124
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 124
  endif
  enddo
  124	continue
  dist1=sqrt( (xnew(nn02)-x_inter(nn34))**2 + (ynew(nn02)-y_inter(nn34))**2 + (znew(nn02)-z_inter(nn34))**2 )
  dist2=sqrt( (xnew(nn03)-x_inter(nn12))**2 + (ynew(nn03)-y_inter(nn12))**2 + (znew(nn03)-z_inter(nn12))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.11
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)<vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 125
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 125
  endif
  enddo
  125	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 126
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 126
  endif
  enddo
  126	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn14+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == No.12
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 127
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 127
  endif
  enddo
  127	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 128
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 128
  endif
  enddo
  128	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn12+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == No.13
  elseif( von(nn01)<vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 .and. von(nn04)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 129
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 129
  endif
  enddo
  129	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 130
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 130
  endif
  enddo
  130	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn03
  ntet2(num_tet0,2)=nn34+new_node
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == No.14
  elseif( von(nn01)<vp0 .and. von(nn02)<vp0 .and. von(nn03)<vp0 .and. von(nn04)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 131
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn34=nline_inter(num_interpolation-kk00+1,3)
  goto 131
  endif
  enddo
  131	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn04)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 132
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn04 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn14=nline_inter(num_interpolation-kk00+1,3)
  goto 132
  endif
  enddo
  132	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn04
  ntet2(num_tet0,2)=nn14+new_node
  ntet2(num_tet0,3)=nn34+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

end subroutine cut_on_face4

!===================================================================
subroutine cut_on_face3(nn01,nn02,nn03,num_g0,num_tet0,num_interpolation,new_node,vp0)
!	→三角形平面と重心節点から四面体を生成
!===================================================================
  implicit none

  integer :: nn01,nn02,nn03,num_g0,num_tet0,num_interpolation,new_node
  integer :: kk00,nn12,nn31,nn23
  real*8 :: vp0,dist1,dist2

!%%%%%%%%%%%%%% tetrahedron(use the gravity point) %%%%%%%%%%%%%%%%%%%%%%%%
! == No.0
  if( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 )then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
! == No.1
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 101
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 101
  endif
  enddo
  101	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 102
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 102
  endif
  enddo
  102	continue
  dist1=dsqrt( (x_inter(nn31)-xnew(nn02))**2 + (y_inter(nn31)-ynew(nn02))**2 + (z_inter(nn31)-znew(nn02))**2 )
  dist2=dsqrt( (xnew(nn03)-x_inter(nn12))**2 + (ynew(nn03)-y_inter(nn12))**2 + (znew(nn03)-z_inter(nn12))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn02
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn03
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.2
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 103
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 103
  endif
  enddo
  103	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 104
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 104
  endif
  enddo
  104	continue
  dist1=dsqrt( (x_inter(nn12)-xnew(nn03))**2 + (y_inter(nn12)-ynew(nn03))**2 + (z_inter(nn12)-znew(nn03))**2 )
  dist2=dsqrt( (xnew(nn01)-x_inter(nn23))**2 + (ynew(nn01)-y_inter(nn23))**2 + (znew(nn01)-z_inter(nn23))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.3
  elseif( von(nn01)>=vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 105
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 105
  endif
  enddo
  105	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 106
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 106
  endif
  enddo
  106	continue
  dist1=dsqrt( (x_inter(nn31)-xnew(nn02))**2 + (y_inter(nn31)-ynew(nn02))**2 + (z_inter(nn31)-znew(nn02))**2 )
  dist2=dsqrt( (xnew(nn01)-x_inter(nn23))**2 + (ynew(nn01)-y_inter(nn23))**2 + (znew(nn01)-z_inter(nn23))**2 )
  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn31+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif
! == No.4
  elseif( von(nn01)<vp0 .and. von(nn02)<vp0 .and. von(nn03)>=vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 107
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 107
  endif
  enddo
  107	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 108
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 108
  endif
  enddo
  108	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn31+new_node
  ntet2(num_tet0,2)=nn23+new_node
  ntet2(num_tet0,3)=nn03
  ntet2(num_tet0,4)=num_g0+new_node
! == No.5
  elseif( von(nn01)>=vp0 .and. von(nn02)<vp0 .and. von(nn03)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 109
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 109
  endif
  enddo
  109	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 110
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn31=nline_inter(num_interpolation-kk00+1,3)
  goto 110
  endif
  enddo
  110	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn01
  ntet2(num_tet0,2)=nn12+new_node
  ntet2(num_tet0,3)=nn31+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == No.6
  elseif( von(nn01)<vp0 .and. von(nn02)>=vp0 .and. von(nn03)<vp0 )then
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn01 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 111
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn01)then
  nn12=nline_inter(num_interpolation-kk00+1,3)
  goto 111
  endif
  enddo
  111	continue
  do kk00=1,num_interpolation
  if(nline_inter(num_interpolation-kk00+1,1)==nn02 .and. nline_inter(num_interpolation-kk00+1,2)==nn03)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 112
  elseif(nline_inter(num_interpolation-kk00+1,1)==nn03 .and. nline_inter(num_interpolation-kk00+1,2)==nn02)then
  nn23=nline_inter(num_interpolation-kk00+1,3)
  goto 112
  endif
  enddo
  112	continue
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=nn12+new_node
  ntet2(num_tet0,2)=nn02
  ntet2(num_tet0,3)=nn23+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

end subroutine cut_on_face3

!===================================================================
subroutine interpolation(nn01,nn02,nn00,num_interpolation,vp0)
!===================================================================
  implicit none

  integer :: nn01,nn02,nn00,num_interpolation
  integer :: kk00
  real*8 :: vp0,x01,y01,z01

!  --
  do kk00=1,num_interpolation
  if(nline_inter(kk00,1)==nn01 .and. nline_inter(kk00,2)==nn02)then
  nn00=nline_inter(kk00,3)
  goto 100
  elseif(nline_inter(kk00,1)==nn02 .and. nline_inter(kk00,2)==nn01)then
  nn00=nline_inter(kk00,3)
  goto 100
  endif
  enddo
!  --
  num_interpolation=num_interpolation+1
  x01= xnew(nn01)-( (xnew(nn01)-xnew(nn02)) / (von(nn01)-von(nn02)) * (von(nn01)-vp0) )
  y01= ynew(nn01)-( (ynew(nn01)-ynew(nn02)) / (von(nn01)-von(nn02)) * (von(nn01)-vp0) )
  z01= znew(nn01)-( (znew(nn01)-znew(nn02)) / (von(nn01)-von(nn02)) * (von(nn01)-vp0) )
  x_inter(num_interpolation)=x01
  y_inter(num_interpolation)=y01
  z_inter(num_interpolation)=z01
  nline_inter(num_interpolation,1)=nn01
  nline_inter(num_interpolation,2)=nn02
  nline_inter(num_interpolation,3)=num_interpolation
  nn00=num_interpolation
100 continue

end subroutine interpolation

!===================================================================
subroutine same_value_level(check1,check2,check3,check4,check5,check6,num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,vp0)
!	→等値面と重心節点から四面体を生成
!===================================================================
  implicit none

  integer :: num_g0,num_tet0,num_interpolation,num_interpolation0,new_node,kpm
  integer :: check1,check2,check3,check4,check5,check6
  integer :: n01,n02,n03,n04,n05,n06,n07,n08
  real*8 :: vp0,dist1,dist2

! == num_interpolation0=3
  if(num_interpolation0==3)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=num_same_value(1)+new_node
  ntet2(num_tet0,2)=num_same_value(2)+new_node
  ntet2(num_tet0,3)=num_same_value(3)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == num_interpolation0=4
  elseif(num_interpolation0==4)then
  msv(1)=num_same_value(1)
  msv(2)=num_same_value(2)
  msv(3)=num_same_value(3)
  msv(4)=num_same_value(4)
  kpm=4
  call m_SUB777(kpm,num_g0)
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(2)+new_node
  ntet2(num_tet0,3)=msv(3)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(3)+new_node
  ntet2(num_tet0,3)=msv(4)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == num_interpolation0=5
  elseif(num_interpolation0==5)then
  msv(1)=num_same_value(1)
  msv(2)=num_same_value(2)
  msv(3)=num_same_value(3)
  msv(4)=num_same_value(4)
  msv(5)=num_same_value(5)
  kpm=5
  call m_SUB777(kpm,num_g0)
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(2)+new_node
  ntet2(num_tet0,3)=msv(3)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(3)+new_node
  ntet2(num_tet0,3)=msv(4)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(4)+new_node
  ntet2(num_tet0,3)=msv(5)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == num_interpolation0=6
  elseif(num_interpolation0==6)then
  msv(1)=num_same_value(1)
  msv(2)=num_same_value(2)
  msv(3)=num_same_value(3)
  msv(4)=num_same_value(4)
  msv(5)=num_same_value(5)
  msv(6)=num_same_value(6)
  kpm=6
  call m_SUB777(kpm,num_g0)
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(2)+new_node
  ntet2(num_tet0,3)=msv(3)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(3)+new_node
  ntet2(num_tet0,3)=msv(4)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(4)+new_node
  ntet2(num_tet0,3)=msv(5)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=msv(1)+new_node
  ntet2(num_tet0,2)=msv(5)+new_node
  ntet2(num_tet0,3)=msv(6)+new_node
  ntet2(num_tet0,4)=num_g0+new_node
! == num_interpolation0=8
  elseif(num_interpolation0==8)then
  if(check1==6.and.check6==6)then	!cut_on_face4 の No.6のパターン(面１と面６に発生)
  n01=num_same_value(1)
  n02=num_same_value(2)
  n07=num_same_value(7)
  n08=num_same_value(8)

  dist1=dsqrt( (x_inter(n01)-x_inter(n08))**2 + (y_inter(n01)-y_inter(n08))**2 + (z_inter(n01)-z_inter(n08))**2 )
  dist2=dsqrt( (x_inter(n02)-x_inter(n07))**2 + (y_inter(n02)-y_inter(n07))**2 + (z_inter(n02)-z_inter(n07))**2 )

  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n02+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n02+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  n03=num_same_value(3)
  n04=num_same_value(4)
  n05=num_same_value(5)
  n06=num_same_value(6)

  dist1=dsqrt( (x_inter(n03)-x_inter(n05))**2 + (y_inter(n03)-y_inter(n05))**2 + (z_inter(n03)-z_inter(n05))**2 )
  dist2=dsqrt( (x_inter(n04)-x_inter(n06))**2 + (y_inter(n04)-y_inter(n06))**2 + (z_inter(n04)-z_inter(n06))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n06+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n04+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n06+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n06+new_node
  ntet2(num_tet0,3)=n03+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  elseif(check2==6.and.check5==6)then	!cut_on_face4 の No.6のパターン(面２と面５に発生)
  n01=num_same_value(1)
  n02=num_same_value(2)
  n06=num_same_value(6)
  n07=num_same_value(7)

  dist1=dsqrt( (x_inter(n01)-x_inter(n07))**2 + (y_inter(n01)-y_inter(n07))**2 + (z_inter(n01)-z_inter(n07))**2 )
  dist2=dsqrt( (x_inter(n02)-x_inter(n06))**2 + (y_inter(n02)-y_inter(n06))**2 + (z_inter(n02)-z_inter(n06))**2 )

  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n02+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n06+new_node
  ntet2(num_tet0,3)=n07+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
    ! --ntet1
    num_tet0=num_tet0+1
    ntet2(num_tet0,1)=n01+new_node
    ntet2(num_tet0,2)=n06+new_node
    ntet2(num_tet0,3)=n02+new_node
    ntet2(num_tet0,4)=num_g0+new_node
    ! --ntet2
    num_tet0=num_tet0+1
    ntet2(num_tet0,1)=n02+new_node
    ntet2(num_tet0,2)=n06+new_node
    ntet2(num_tet0,3)=n07+new_node
    ntet2(num_tet0,4)=num_g0+new_node
  endif

  n03=num_same_value(3)
  n04=num_same_value(4)
  n05=num_same_value(5)
  n08=num_same_value(8)

  dist1=dsqrt( (x_inter(n03)-x_inter(n08))**2 + (y_inter(n03)-y_inter(n08))**2 + (z_inter(n03)-z_inter(n08))**2 )
  dist2=dsqrt( (x_inter(n04)-x_inter(n05))**2 + (y_inter(n04)-y_inter(n05))**2 + (z_inter(n04)-z_inter(n05))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n04+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n03+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  elseif(check1==9.and.check6==9)then	!cut_on_face4 の No.9のパターン(面１と面６に発生)
  n01=num_same_value(1)
  n02=num_same_value(2)
  n05=num_same_value(5)
  n06=num_same_value(6)

  dist1=dsqrt( (x_inter(n01)-x_inter(n05))**2 + (y_inter(n01)-y_inter(n05))**2 + (z_inter(n01)-z_inter(n05))**2 )
  dist2=dsqrt( (x_inter(n02)-x_inter(n06))**2 + (y_inter(n02)-y_inter(n06))**2 + (z_inter(n02)-z_inter(n06))**2 )

  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n02+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n06+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n02+new_node
  ntet2(num_tet0,2)=n01+new_node
  ntet2(num_tet0,3)=n06+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n02+new_node
  ntet2(num_tet0,2)=n06+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  n03=num_same_value(3)
  n04=num_same_value(4)
  n07=num_same_value(7)
  n08=num_same_value(8)

  dist1=dsqrt( (x_inter(n03)-x_inter(n08))**2 + (y_inter(n03)-y_inter(n08))**2 + (z_inter(n03)-z_inter(n08))**2 )
  dist2=dsqrt( (x_inter(n04)-x_inter(n07))**2 + (y_inter(n04)-y_inter(n07))**2 + (z_inter(n04)-z_inter(n07))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n07+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n04+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n07+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n03+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  elseif(check2==9.and.check5==9)then	!cut_on_face4 の No.9のパターン(面２と面５に発生)
  n01=num_same_value(1)
  n02=num_same_value(2)
  n05=num_same_value(5)
  n08=num_same_value(8)

  dist1=dsqrt( (x_inter(n01)-x_inter(n08))**2 + (y_inter(n01)-y_inter(n08))**2 + (z_inter(n01)-z_inter(n08))**2 )
  dist2=dsqrt( (x_inter(n02)-x_inter(n05))**2 + (y_inter(n02)-y_inter(n05))**2 + (z_inter(n02)-z_inter(n05))**2 )

  if(dist1<=dist2)then
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n08+new_node
  ntet2(num_tet0,3)=n02+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n01+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet1
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n02+new_node
  ntet2(num_tet0,2)=n01+new_node
  ntet2(num_tet0,3)=n05+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet2
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n02+new_node
  ntet2(num_tet0,2)=n05+new_node
  ntet2(num_tet0,3)=n08+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif

  n03=num_same_value(3)
  n04=num_same_value(4)
  n06=num_same_value(6)
  n07=num_same_value(7)

  dist1=dsqrt( (x_inter(n03)-x_inter(n07))**2 + (y_inter(n03)-y_inter(n07))**2 + (z_inter(n03)-z_inter(n07))**2 )
  dist2=dsqrt( (x_inter(n04)-x_inter(n06))**2 + (y_inter(n04)-y_inter(n06))**2 + (z_inter(n04)-z_inter(n06))**2 )

  if(dist1<=dist2)then
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n06+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n03+new_node
  ntet2(num_tet0,2)=n04+new_node
  ntet2(num_tet0,3)=n07+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  else
  ! --ntet3
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n07+new_node
  ntet2(num_tet0,3)=n06+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  ! --ntet4
  num_tet0=num_tet0+1
  ntet2(num_tet0,1)=n04+new_node
  ntet2(num_tet0,2)=n06+new_node
  ntet2(num_tet0,3)=n03+new_node
  ntet2(num_tet0,4)=num_g0+new_node
  endif
  end if
  endif

end subroutine same_value_level

!======================================================================================
  real(8) function volume(ia,ib,ic,ip)
! →四面体の体積計算(パラメータ座標)
!======================================================================================
  implicit none

  integer :: ia,ib,ic,ip
  real*8 :: xa,ya,za,xb,yb,zb,xc,yc,zc,xp,yp,zp,va,vb,vc,wa,wb,wc

  xa = xnew(ia)
  ya = ynew(ia)
  za = znew(ia)
  xb = xnew(ib)
  yb = ynew(ib)
  zb = znew(ib)
  xc = xnew(ic)
  yc = ynew(ic)
  zc = znew(ic)
  xp = xnew(ip)
  yp = ynew(ip)
  zp = znew(ip)

  va = xb*yc*zp+xa*ya*zp+xb*ya*za+xa*yc*za-(xb*yc*za+xa*ya*za+xb*ya*zp+xa*yc*zp)
  vb = yb*zc*xp+ya*za*xp+yb*za*xa+ya*zc*xa-(yb*zc*xa+ya*za*xa+yb*za*xp+ya*zc*xp)
  vc = zb*xc*yp+za*xa*yp+zb*xa*ya+za*xc*ya-(zb*xc*ya+za*xa*ya+zb*xa*yp+za*xc*yp)

  wa = xb*zc*ya+xa*za*ya+xb*za*yp+xa*zc*yp-(xb*zc*yp+xa*za*yp+xb*za*ya+xa*zc*ya)
  wb = yb*xc*za+ya*xa*za+yb*xa*zp+ya*xc*zp-(yb*xc*zp+ya*xa*zp+yb*xa*za+ya*xc*za)
  wc = zb*yc*xa+za*ya*xa+zb*ya*xp+za*yc*xp-(zb*yc*xp+za*ya*xp+zb*ya*xa+za*yc*xa)

  volume = (va+vb+vc+wa+wb+wc)/6

end function volume

!===================================================================
subroutine m_SUB777(kpm,num_g0)
!	→m_SUB777:六面体内の等高面の節点番号逆時針方向に並べる。
!		VPXYZ(lr,k)=(X,Y,Z) lr=1,2,3
!		DC0＝１，2 点と各点を結んだ三角形の角度，最終は順番に並べる。
!		LD0→順番に並べた後と順番に並べる前の番号の関係。
!		LL6→LV6順番のための中間ベクトル
!		LV6 →等高面の節点順番に対応する等高面の節点番号
!===================================================================
  implicit none

  integer :: kpm,num_g0,ks0
  integer :: i,l1,l2,lx
  real*8 :: dc

!  --
  DC0=0.0d0
  LD0=0
  LL6=0
  call m_SUB666(kpm,num_g0)
  if(kpm>6) kpm=6   !====2004.5.18
  Ks0=0
  do 60 i=1,kpm-2
  L1=LD0(i)
  DC=DC0(i)
  LL6(i)=Msv(i)
  if(DC<0) Ks0=Ks0+1
60	continue
  if(Ks0/=0) Then
  L1=LD0(1)
  L2=Msv(2)
  Msv(2)=Msv(L1+1)
  Msv(L1+1)=L2
  call m_SUB666(kpm,num_g0)
  if(kpm>6) kpm=6  !====2004.5.18
  endif
  LL6(1)=Msv(1)
  LL6(2)=Msv(2)
  do 70 i=1,kpm-2
  L2=LD0(i)
  Lx=LL6(i+2)
  LL6(i+2)=Msv(L2+2)
  Msv(L2+2)=Lx
70	continue
  do 80 i=1,kpm
  Msv(i)=LL6(i)
80	continue

end subroutine m_SUB777

!===================================================================
  subroutine m_SUB666(kpm,num_g0)
!	→SUB666:六面体内の各面の節点番号逆時針方向に並べる準備。
!		VPXYZ(lr,k)=(X,Y,Z) lr=1,2,3
!		DC0＝１，2 点と各点を結んだ三角形の角度，最終は順番に並べる。
!		LD0→順番に並べた後と順番に並べる前の番号の関係。
!		Msv →各面の節点順番に対応する等高面の節点番号
!===================================================================
  implicit none

  real*8,dimension(:,:) :: ai(3,5),Vxyz(3,6)
  integer :: kpm,num_g0
  integer :: i,j,j1,j2,j3,k,ki,k1,kc0,jmin
  real*8 :: x1,y1,z1,xp,yp,zp,xi,yi,zi,ap01,ap02,ap03,a1,b1,c1,xxx,dc,VS0,zx1,zx2,zx3,zx,zii,zjj,zkk,dmin,d0,d1

!  --
  ai=0.0d0
  Vxyz=0.0d0
  DC0=0.0d0
  LD0=0
!  --
  k1=Msv(1)
  x1=x_inter(k1)
  y1=y_inter(k1)
  z1=z_inter(k1)
  xp=x_inter(num_g0)
  yp=y_inter(num_g0)
  zp=z_inter(num_g0)
  ap01=x1-xp
  ap02=y1-yp
  ap03=z1-zp
  Vxyz(1,1)=ap01
  Vxyz(2,1)=ap02
  Vxyz(3,1)=ap03
  if(kpm>6) kpm=6  !==2004.5.18
!  --
  do 10 i=1,Kpm-1
  ki=Msv(i+1)
  xi=x_inter(ki)
  yi=y_inter(ki)
  zi=z_inter(ki)
  ai(1,i)=xi-x1
  ai(2,i)=yi-y1
  ai(3,i)=zi-z1
  Vxyz(1,i+1)=xi-xp
  Vxyz(2,i+1)=yi-yp
  Vxyz(3,i+1)=zi-zp
10	continue
  a1=dsqrt( (ai(1,1))**2 + (ai(2,1))**2 + (ai(3,1))**2 )
  do 20 k=1,kpm-2
  b1=dsqrt((ai(1,k+1))**2+(ai(2,k+1))**2+(ai(3,k+1))**2)
  c1=dsqrt((ai(1,1)-ai(1,k+1))**2+(ai(2,1)-ai(2,k+1))**2+(ai(3,1)-ai(3,k+1))**2)
  if(a1*b1==0.0d0) goto 20   !==2004.5.18
  xxx=(a1**2+b1**2-c1**2)/(2.0d0*a1*b1)
  if(xxx>=1.0d0) then
  DC=0.0d0
  elseif(xxx<=-1.0d0) then
  DC=180.0d0
  else
  DC=dacosd(xxx)
  endif
  kc0=k+2
  VS0= m_Xmatorix3(Vxyz,1,2,kc0)/6.0d0
  Zx1=ai(2,1)*ai(3,2)-ai(3,1)*ai(2,2)
  Zx2=ai(3,1)*ai(1,2)-ai(1,1)*ai(3,2)
  Zx3=ai(1,1)*ai(2,2)-ai(2,1)*ai(1,2)
  Zx=dsqrt(Zx1**2+Zx2**2+Zx3**2)
  if(Zx==0.0d0) goto 20   !==2004.5.18
  Zii=Zx1/Zx
  Zjj=Zx2/Zx
  Zkk=Zx3/Zx
  VS0=ZX1*Zii+Zx2*Zjj+Zx3*Zkk
  DC0(k)=dsign(DC,VS0)
20	continue
!  --
  do 35 i=1,6
  LD0(i)=i
35	continue
!  --
  do 40 i=1,kpm-2
  j3=i
  J1=LD0(i)
  Jmin=i
  Dmin=DC0(i)
  do 50 j=i+1,kpm-2
  D1=DC0(j)
  If(D1<Dmin)then
  J1=LD0(j)
  j3=j
  Jmin=j
  Dmin=D1
  endif
50	continue
  D0=DC0(i)
  DC0(i)=DC0(jmin)
  DC0(jmin)=D0
  j2=LD0(i)
  LD0(i)=j1
  LD0(j3)=j2
40	continue

end subroutine m_SUB666


!======================================================================================
real*8 function m_Xmatorix3 (Vxyz,iz,jz,kz)
!	→matorix3 :3x3マトリクス展開値を求める。
!		Vxyz(lr,k)=(X,Y,Z座標) lr=1,2,3,(四面体節点番号）k=1,2,3,4
!======================================================================================
  implicit none

  real*8,dimension(:,:) :: Vxyz(3,6)
  integer :: iz,jz,kz
  real*8 :: z1,z2,z3,z4,z5,z6


  Z1=Vxyz(1,iz)*Vxyz(2,jz)*Vxyz(3,kz)
  Z2=Vxyz(2,iz)*Vxyz(3,jz)*Vxyz(1,kz)
  Z3=Vxyz(3,iz)*Vxyz(1,jz)*Vxyz(2,kz)

  Z4=Vxyz(3,iz)*Vxyz(2,jz)*Vxyz(1,kz)
  Z5=Vxyz(2,iz)*Vxyz(1,jz)*Vxyz(3,kz)
  Z6=Vxyz(1,iz)*Vxyz(3,jz)*Vxyz(2,kz)

  m_Xmatorix3=z1+z2+z3-z4-z5-z6

end function m_Xmatorix3

end module TETRA_MESH
